using System;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using SEB.Entities;
using SEB.Mapping;

namespace SEB
{
	public class BookMakerContext : DbContext
	{
		static BookMakerContext()
		{ 
			Database.SetInitializer<BookMakerContext>(null);
		}

		public DbSet<Currency> Currencies { get; set; }
		public DbSet<Driver> Drivers { get; set; }
		public DbSet<ELMAH_Error> ELMAH_Error { get; set; }
		public DbSet<Equipment> Equipments { get; set; }
		public DbSet<EquipmentCode> EquipmentCodes { get; set; }
		public DbSet<Fleet> Fleets { get; set; }
		public DbSet<Hour> Hours { get; set; }
		public DbSet<Import> Imports { get; set; }
		public DbSet<Location> Locations { get; set; }
		public DbSet<Login> Logins { get; set; }
		public DbSet<Mail> Mails { get; set; }
		public DbSet<MailAttachment> MailAttachments { get; set; }
		public DbSet<Person> People { get; set; }
		public DbSet<Region> Regions { get; set; }
		public DbSet<Request> Requests { get; set; }
		public DbSet<Reservation> Reservations { get; set; }
		public DbSet<ReservationHistory> ReservationHistories { get; set; }
		public DbSet<ReservationRequest> ReservationRequests { get; set; }
		public DbSet<State> States { get; set; }
		public DbSet<TravelAgent> TravelAgents { get; set; }
		public DbSet<User> Users { get; set; }
		public DbSet<WSCache> WSCaches { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
		{
            modelBuilder.Conventions.Remove<IncludeMetadataConvention>();
			modelBuilder.Configurations.Add(new CurrencyMap());
			modelBuilder.Configurations.Add(new DriverMap());
			modelBuilder.Configurations.Add(new ELMAH_ErrorMap());
			modelBuilder.Configurations.Add(new EquipmentMap());
			modelBuilder.Configurations.Add(new EquipmentCodeMap());
			modelBuilder.Configurations.Add(new FleetMap());
			modelBuilder.Configurations.Add(new HourMap());
			modelBuilder.Configurations.Add(new ImportMap());
			modelBuilder.Configurations.Add(new LocationMap());
			modelBuilder.Configurations.Add(new LoginMap());
			modelBuilder.Configurations.Add(new MailMap());
			modelBuilder.Configurations.Add(new MailAttachmentMap());
			modelBuilder.Configurations.Add(new PersonMap());
			modelBuilder.Configurations.Add(new RegionMap());
			modelBuilder.Configurations.Add(new RequestMap());
			modelBuilder.Configurations.Add(new ReservationMap());
			modelBuilder.Configurations.Add(new ReservationHistoryMap());
			modelBuilder.Configurations.Add(new ReservationRequestMap());
			modelBuilder.Configurations.Add(new StateMap());
			modelBuilder.Configurations.Add(new TravelAgentMap());
			modelBuilder.Configurations.Add(new UserMap());
			modelBuilder.Configurations.Add(new WSCacheMap());
		}
	}
}

