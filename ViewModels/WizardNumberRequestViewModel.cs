﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SEB.ViewModels
{
    public class WizardNumberRequestViewModel
    {
        
        public string EmployeeId { get; set; }
        public Nullable<double> DepartmentID { get; set; }
        public Nullable<double> CompanyID { get; set; }
        public Nullable<double> OfficePhoneNumber { get; set; }
        public Nullable<double> MobilePhoneNumber { get; set; }
        public string ProjectNo { get; set; }
        public string DriversLicenseNumber { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string AddressLine3 { get; set; }
        public string PostalCode { get; set; }
        public Nullable<System.DateTime> DateOfBirth { get; set; }
        public Nullable<System.DateTime> EndDate { get; set; }
        public string WizardNumber { get; set; }
    }
}