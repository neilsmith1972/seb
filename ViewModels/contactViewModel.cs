﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace SEB.ViewModels
{
    public class contactViewModel
    {
        [Required]
        public string FirstName { get; set; }

        [Required]
        public string LastName { get; set; }

        [Required]
        public string EmailAddress { get; set; }

        public string PhoneNumber { get; set; }

        [Required]
        public string Enquiry { get; set; }

        public int Company { get; set; }
        public int EnquiryType { get; set; }
    }
}