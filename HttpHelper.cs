﻿//---------------------------------------------------------------------
// <copyright file="HttpHelper.cs" company="Adam Elsworth">
//      Copyright (c) Adam Elsworth. All Rights Reserved.
//      Information Contained Herein is Proprietary and Confidential.
// </copyright>
//---------------------------------------------------------------------
namespace SolutionTemplate.ExternalServices
{
    #region "Using Directive(s)"

    using System;
    using System.Collections.Generic;
    using System.Collections.Specialized;
    using System.IO;
    using System.Linq;
    using System.Net;
    using System.Text;
    using System.Web;

    #endregion

    /// <summary>
    /// 
    /// </summary>
    public static class HttpHelper
    {
        #region "Constant Fields"

        #endregion

        #region "Fields"

        #endregion

        #region "Constructors"

        #endregion

        #region "Finalizers (Destructors)"

        #endregion

        #region "Delegates"

        #endregion

        #region "Events"

        #endregion

        #region "Enums"

        #endregion

        #region "Interfaces"

        #endregion

        #region "Properties"

        #endregion

        #region "Indexers"

        #endregion

        #region "Methods"

        public static string HttpGet(string requestUri)
        {
            string res;
            var req = WebRequest.Create(requestUri);
            var resp = req.GetResponse();
            using (var sr = new StreamReader(resp.GetResponseStream()))
            {
                res = sr.ReadToEnd().Trim();
            }
            return res;
        }
        /// <summary>
        ///  Does a Http Post
        /// </summary>
        /// <param name="requestUri">Uri</param>
        /// <param name="parameters">parameteres in the form val1=test&val2=test2</param>
        /// <param name="cache">Loging credentials if any</param>
        /// <returns></returns>
        public static string HttpPost(string requestUri, string parameters, CredentialCache cache = null)
        {
            try
            {
                string res = String.Empty;
                WebRequest req = WebRequest.Create(requestUri);
                //Add these, as we're doing a POST
                if (cache != null)
                    req.Credentials = cache;
                req.ContentType = "application/x-www-form-urlencoded";
                req.Method = "POST";
                //We need to count how many bytes we're sending. Post'ed Faked Forms should be name=value&
                byte[] bytes = Encoding.ASCII.GetBytes(parameters);
                req.ContentLength = bytes.Length;

                var os = req.GetRequestStream();
                os.Write(bytes, 0, bytes.Length); //Push it out there
                os.Close();

                var resp = req.GetResponse();
                if (resp == null) return null;

                using (var sr = new StreamReader(resp.GetResponseStream()))
                {
                    res = sr.ReadToEnd().Trim();
                }
                return res;
            }
            catch (Exception ex)
            {
                throw new HttpException("A connection could not be established, is the proxy server or the web service down?", ex);
            }
        }
        /// <summary>
        /// Does a Http Post
        /// </summary>
        /// <param name="requestUri">
        /// Uri
        /// </param>
        /// <param name="parameters">
        /// </param>
        /// <param name="cache">
        /// Loging credentials if any
        /// </param>
        /// <returns>
        /// </returns>
        public static string HttpPost(string requestUri, NameValueCollection parameters, CredentialCache cache = null)
        {
            var resultParameters = new StringBuilder();
            foreach (string key in parameters)
            {
                resultParameters.AppendFormat("{0}={1}&",
                    HttpUtility.UrlEncode(key),
                    HttpUtility.UrlEncode(parameters[key]));
            }
            resultParameters.Length -= 1;

            return HttpPost(requestUri, resultParameters.ToString(), cache);
        }

        public static string HttpPost(string requestUri, IDictionary<string, object> parameters, CredentialCache cache = null)
        {
            var resultParameters = new StringBuilder();
            foreach (var item in parameters)
            {
                resultParameters.AppendFormat("{0}={1}&",
                    HttpUtility.UrlEncode(item.Key),
                    HttpUtility.UrlEncode(item.Value.ToString()));
            }
            resultParameters.Length -= 1;

            return HttpPost(requestUri, resultParameters.ToString(), cache);
        }

        #endregion

        #region "Structs"

        #endregion

        #region "Classes"

        #endregion
    }
}
