﻿//---------------------------------------------------------------------
// <copyright file="MoneyDto.cs" company="Adam Elsworth">
//      Copyright (c) Adam Elsworth. All Rights Reserved.
//      Information Contained Herein is Proprietary and Confidential.
// </copyright>
//---------------------------------------------------------------------
namespace SolutionTemplate.ExternalServices.Dto
{
    using System.Runtime.Serialization;

    #region "Using Directive(s)"    

    #endregion

    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    public class MoneyDto
    {
        #region "Constant Fields"

        #endregion

        #region "Fields"

        #endregion

        #region "Constructors"

        public MoneyDto()
        {            
        }

        public MoneyDto(string currencyCode, decimal amount)
        {
            this.CurrencyCode = currencyCode;
            this.Amount = amount;
        }

        #endregion

        #region "Finalizers (Destructors)"

        #endregion

        #region "Delegates"

        #endregion

        #region "Events"

        #endregion

        #region "Enums"

        #endregion

        #region "Interfaces"

        #endregion

        #region "Properties"

        /// <summary>
        /// Gets or sets the monetary amount.
        /// </summary>
        [DataMember]
        public decimal Amount { get; set; }

        /// <summary>
        /// Gets or sets the monetary currency.
        /// </summary>
        [DataMember]
        public string CurrencyCode { get; set; }

        // public int CurrencyId { get; set; }
        [IgnoreDataMember]
        public string UserFriendlyString { get; set; }

        #endregion

        #region "Indexers"

        #endregion

        #region "Methods"

        public override string ToString()
        {
            return this.UserFriendlyString;
        }

        #endregion

        #region "Structs"

        #endregion

        #region "Classes"

        #endregion
    }
}
