﻿//---------------------------------------------------------------------
// <copyright file="RefundPaymentDto.cs" company="Adam Elsworth">
//      Copyright (c) Adam Elsworth. All Rights Reserved.
//      Information Contained Herein is Proprietary and Confidential.
// </copyright>
//---------------------------------------------------------------------
namespace SolutionTemplate.ExternalServices.Dto
{
    #region "Using Directive(s)"

    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    #endregion

    /// <summary>
    /// 
    /// </summary>
    public class MakePaymentDto
    {
        #region "Constant Fields"

        #endregion

        #region "Fields"

        #endregion

        #region "Constructors"

        #endregion

        #region "Finalizers (Destructors)"

        #endregion

        #region "Delegates"

        #endregion

        #region "Events"

        #endregion

        #region "Enums"

        #endregion

        #region "Interfaces"

        #endregion

        #region "Properties"

        public decimal Amount { get; set; }

        public string OrderId { get; set; }

        public string Currency { get; set; }

        public string RentalAgreementNumber { get; set; }

        public long OriginalPaymentId { get; set; }

        public string CardNumber { get; set; }

        public string CardHolderName { get; set; }

        public string ExpiryDate { get; set; }

        public string CVC { get; set; }

        #endregion

        #region "Indexers"

        #endregion

        #region "Methods"

        #endregion

        #region "Structs"

        #endregion

        #region "Classes"

        #endregion
    }
}
