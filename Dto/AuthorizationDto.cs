﻿//---------------------------------------------------------------------
// <copyright file="AuthorizationDto.cs" company="Adam Elsworth">
//      Copyright (c) Adam Elsworth. All Rights Reserved.
//      Information Contained Herein is Proprietary and Confidential.
// </copyright>
//---------------------------------------------------------------------
namespace SolutionTemplate.ExternalServices.Dto
{
    #region "Using Directive(s)"

    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    #endregion

    /// <summary>
    /// 
    /// </summary>
    public class AuthorizationDto
    {
        #region "Constant Fields"

        #endregion

        #region "Fields"

        #endregion

        #region "Constructors"

        #endregion

        #region "Finalizers (Destructors)"

        #endregion

        #region "Delegates"

        #endregion

        #region "Events"

        #endregion

        #region "Enums"

        #endregion

        #region "Interfaces"

        #endregion

        #region "Properties"

        public decimal Amount { get; set; }

        public string OrderId { get; set; }

        public string Currency { get; set; }

        public string RentalAgreementNumber { get; set; }

        public string CustomerName { get; set; }

        public string CustomerEmail{ get; set; }

        public string RedirectUrlId { get; set; }

        public bool InstantCapture { get; set; }

        public bool IsMobileView { get; set; }

        #endregion

        #region "Indexers"

        #endregion

        #region "Methods"

        #endregion

        #region "Structs"

        #endregion

        #region "Classes"

        #endregion
    }
}
