﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MvcPayment.Dto
{
    using System.Runtime.Serialization;

    public enum CardAuthorizationStatus
    {
        /// <summary>
        /// The authorization is waiting action from the customer
        /// </summary>
        Pending = 0,

        /// <summary>
        /// The user has authorized the card using the payment gateway
        /// </summary>
        Authorized = 1,

        /// <summary>
        /// The authorization has been used to capture one or more payments
        /// </summary>
        Captured = 2,

        /// <summary>
        /// The authorization has been cancelled, this can only happen if no payments have been captured for the authorization.
        /// </summary>
        Cancelled = 4,

        Declined = 8,

        Error = 16
    }

    public class CardAuthorizationDto 
    {
        #region "Constant Fields"

        #endregion

        #region "Fields"

        #endregion

        #region "Constructors"

        #endregion

        #region "Finalizers (Destructors)"

        #endregion

        #region "Delegates"

        #endregion

        #region "Events"

        #endregion

        #region "Enums"

        #endregion

        #region "Interfaces"

        #endregion

        #region "Properties"

        [DataMember]
        public string CardNumber { get; set; }

        [DataMember]
        public decimal AuthorizedAmount { get; set; }

        [DataMember]
        public string AuthorizedCurrency { get; set; }

        [DataMember]
        public bool IsCaptured { get; set; }

        [DataMember]
        public string Status { get; set; }

        [IgnoreDataMember]
        public string PaymentGatewayAuthorizationId { get; set; }

        [DataMember]
        public string Brand { get; set; }

        [DataMember(IsRequired = false)]
        public DateTime? TransactionDate { get; set; }

        [DataMember]
        public string OrderId { get; set; }

        [DataMember]
        public string PaymentMethod { get; set; }

        [DataMember]
        public string ErrorDescription { get; set; }

        [DataMember]
        public string PaymentGatewayStatus { get; set; }

        /// <summary>
        /// Gets a value indicating if the card auth should be instantly captured.
        /// </summary>
        [DataMember]
        public bool InstantCapture { get; set; }

        #endregion

        #region "Indexers"

        #endregion

        #region "Methods"

        #endregion

        #region "Structs"

        #endregion

        #region "Classes"

        #endregion
    }
}