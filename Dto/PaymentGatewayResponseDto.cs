﻿using System;

namespace MvcPayment.Dto
{
    public class PaymentGatewayResponseDto
    {
        public string CardNumber { get; set; }

        public string Brand { get; set; }

        public DateTime TransactionDate { get; set; }

        public string PaymentGatewayStatus { get; set; }

        public string PaymentMethod { get; set; }

        public string ErrorDescription { get; set; }

        public string PayId { get; set; }

        public string PaymentGatewayAuthorizationId { get; set; }
    }
}