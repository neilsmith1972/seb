﻿using System.Data;
using SEB.Entities;
using Xeed.Runtime.Serialization;

namespace SEB.Controllers
{
    using System;
    using System.Collections.Specialized;
    using System.Linq;
    using System.Web.Mvc;

    using MvcPayment.Dto;

    using SolutionTemplate.ExternalServices;
    using SolutionTemplate.ExternalServices.Impl;

    /*
       ========== OGONE TEST CREDIT CARDS ==========
       Are there test credit cards that I can use?

       A few credit card numbers have been defined in our test environment for testing purposes.
       As these are only meant for testing, please refrain from using them in our production environment:
       Visa Brand : 4111 1111 1111 1111
       Visa 3D Brand : 4000 0000 0000 0002
       American Express Brand : 3741 1111 1111 111
       MasterCard Brand : 5399 9999 9999 9999
       Diners Brand : 3625 5695 5800 17
       Bancontact/Mister Cash Brand : 67030000000000003

       Visa Purchasing Brand : 4484 1200 0000 0029
       American Express Purchasing Brand : 3742 9101 9071 995
       Any expiry date in a near future may be used and any 3 digit CVC code. (4 digits for American Express and no CVC code for purchasing cards)
       */


    public class CardAuthorizationController : Controller
    {
        //private ILog log = LogManager.GetLogger(typeof(CardAuthorizationController));

        SEB.SEBContext db = new SEBContext();

        // NOTE: Use the 'OGonePaymentGateway' implementation for the 'IPaymentGateway' interface.
        public CardAuthorizationController(/* IPaymentGateway paymentGateway -> Uncomment this if you are using a DI framework. */)            
        {            
            /*
            if (paymentGateway == null)
            {
                throw new ArgumentNullException("paymentGateway");
            }
            */

            this.PaymentGateway = new OGonePaymentGateway(); // paymentGateway;
        }

        public IPaymentGateway PaymentGateway { get; private set; }

        /// <summary>
        /// This is the controller action that must be called to begin the payment process.
        /// <para />
        /// This action generates parameters that will be posted to the payment gateway.
        /// </summary>
        /// <param name="id">Normally, this will be a 'booking' id that can be used to load booking details from the database.</param>
        /// <returns>A view that redirects users to the payment gateway.</returns>
        //[HttpGet]
        //public ActionResult PerformAuthorization(object id) // TODO: Change the id parameter Type.
        //{            
        //    /**************** This is only for demo puposes. Look at the code below for basic implementation. *******************************/
        //    var payAuthDto = new AuthorizationDto()
        //    {
        //        Amount = 100,
        //        Currency = "NOK",
        //        OrderId = "7",
        //        CustomerName = "Jim Bo", // TODO: Get the customer name from the database
        //        CustomerEmail = "jim@email.com",
        //        RedirectUrlId = Guid.NewGuid().ToString(),
        //        InstantCapture = false,
        //        IsMobileView = false
        //    };
        //    var redirectUrl = "OnlyUsedForRedirect";

        //    /*
        //    throw new NotImplementedException("Load data that contains payment info from database here, using the 'Id' parameter.");
        //    CardAuthorizationDto cardAuth = null; // TODO: You may want to extend this object to include all data required for payments

        //    var payAuthDto = new AuthorizationDto()
        //                         {
        //                             Amount = cardAuth.AuthorizedAmount,
        //                             Currency = cardAuth.AuthorizedCurrency,
        //                             OrderId = cardAuth.OrderId,
        //                             CustomerName = customer.FullName, // TODO: Get the customer name from the database
        //                             CustomerEmail = customer.Email,
        //                             RedirectUrlId = id,
        //                             InstantCapture = cardAuth.InstantCapture,
        //                             IsMobileView = false
        //                         };

        //    var redirectUrl = this.Request.QueryString["carRequestId"];
        //    */

        //    var formParameters = this.PaymentGateway.GetFormParametersForPostingPaymentAuthorization(payAuthDto, redirectUrl);
        //    return this.View(formParameters);
        //}

        [HttpGet]
        public ActionResult DeclineAuthorization()
        {
            var id = Guid.Parse(this.Request.QueryString["id"]);

            // Get the request parameters and update the db
            var responseParams = this.GetRequestParameters();
            this.UpdateCardAuth(id, responseParams, CardAuthorizationStatus.Declined);

            this.TempData.Add("AuthMessage", "The card was declined, please try again.");

            // Redirect to Try Again
            return this.RedirectToAction("BookingSummary", "Booking", new { id = id });
        }

        [HttpGet]
        public ActionResult ExceptionAuthorization()
        {
            var id = Guid.Parse(this.Request.QueryString["id"]);

            // Get the request parameters and update the db
            var responseParams = this.GetRequestParameters();
            this.UpdateCardAuth(id, responseParams, CardAuthorizationStatus.Error);

            this.TempData.Add("AuthMessage", "Something went wrong. Please try again.");

            // Redirect to Try Again
            return this.RedirectToAction("BookingSummary", "Booking", new { id = id });
        }

        [HttpGet]
        public ActionResult CancelAuthorization()
        {
            //var id = Guid.Parse(this.Request.QueryString["id"]);

            //// Get the request parameters and update the db
            //var responseParams = this.GetRequestParameters();
            //this.UpdateCardAuth(id, responseParams, CardAuthorizationStatus.Cancelled); // TODO: You may or may not want to mark the auth as cancelled.

            //var redirectOverride = this.Request.QueryString["carRequestId"]; // TODO: This is a custom parameter used only for redirection. You probably want to delete this.
            //if (!string.IsNullOrWhiteSpace(redirectOverride))
            //{
            //    // Need to get the base URL From Config           
            //    var url = ConfigurationManager.AppSettings["rodBaseUrl"]
            //              + "Request/Details?carRequestId=" + redirectOverride;
            //    //log.Debug("Accept: 5");
            //    return this.Redirect(url);
            //}

            //return this.View();

            return RedirectToAction("bookingsummary", "booking");

        }

        [HttpGet]
        public ActionResult Thankyou(Guid id)
        {            
            if (this.TempData.ContainsKey("AuthMessage"))
            {
                this.ViewData.Add("AuthMessage", this.TempData["AuthMessage"]);    
            }

            return RedirectToAction("confirmbooking", "booking");
        }

        [HttpGet]
        public ActionResult AcceptAuthorization()
        {
            try
            {
                //log.Debug("Accept: 1");
                //var id = Guid.Parse(this.Request.QueryString["id"]);
                //log.Debug("Accept: 2");

                // Get the request parameters and update the db
                var responseParams = this.GetRequestParameters();
                //this.UpdateCardAuth(id, responseParams, CardAuthorizationStatus.Authorized);

                var reservation = Session["reservation"] as Service.DCReservation;
                if (reservation.DriverRemarks == responseParams["carRequestId"])
                {
                    db.Payments.Add(new Payment
                                        {
                                            Amount = decimal.Parse(responseParams["Amount"]),
                                            OGonePaymentId = responseParams["OrderId"],
                                            PaymentDate = DateTime.Now,
                                            PaymentStatus = "SUCCESS",
                                            ReservationUniqueId = responseParams["carRequestId"],
                                            Comments = responseParams["Currency"],
                                            UserReference = responseParams["PAYID"]
                                        });

                    db.SaveChanges();

                    var resid = Guid.Parse(responseParams["carRequestId"]);
                    var s_Reservation = db.Reservations.FirstOrDefault(r=>r.Reservation_ID == resid);
                    s_Reservation.Reservation_Updated = DateTime.Now;
                    s_Reservation.Reservation_Status = "PAYMENT MADE - RESERVATION NOT CONFIRMED";
                    db.Entry(s_Reservation).State = EntityState.Modified;
                    db.SaveChanges();

                }

                //log.Debug("Accept: 3");
                //if (this.TempData.ContainsKey("AuthMessage"))
                //{
                //    this.TempData.Remove("AuthMessage");
                //}

                //var values = this.CreatePaymentGatewayResponseDto(responseParams);
                //this.TempData.Add("AuthMessage", "Your receipt number is " + values.PaymentGatewayAuthorizationId);
                
                ////log.Debug("Accept: 4");
                //var redirectOverride = this.Request.QueryString["carRequestId"]; // TODO: This is a custom parameter used only for redirection. You probably want to delete this.
                //if (!string.IsNullOrWhiteSpace(redirectOverride))
                //{
                //    // Need to get the base URL From Config           
                //    var url = ConfigurationManager.AppSettings["rodBaseUrl"]
                //              + "Request/CheckOutToCustomer?carRequestId=" + redirectOverride;
                //    //log.Debug("Accept: 5");
                //    return this.Redirect(url);
                //}

                //log.Debug("Accept: 6");
                
                // Redirect to Try Again                
                return RedirectToAction("confirmbooking", "booking");
            }
            catch (Exception ex)
            {
                 //log.Error("Error accepting card authorization.", ex);
                throw;
            }
        }

        private NameValueCollection GetRequestParameters()
        {
            var responseParams = this.Request.QueryString;
            string shaOutHash = responseParams["SHASIGN"];
            if (!OGonePaymentGateway.VerifyShaOutHash(responseParams, shaOutHash))
            {
                throw new InvalidOperationException("Invalid SHA OUT HASH. Request may have been tampered with.");
            }
            return responseParams;
        }


        private void UpdateCardAuth(Guid id, NameValueCollection responseParams, CardAuthorizationStatus status)
        {
            SEB.SEBContext db = new SEBContext();
            var paymentGatewayRespone = this.CreatePaymentGatewayResponseDto(responseParams);
        }

        /// <summary>
        /// Converts the payment gateway response to a strongly-typed object.
        /// </summary>
        /// <param name="response">The payment gateway response.</param>
        /// <returns>A strongly-typed instance of payment gateway parameters.</returns>
        private PaymentGatewayResponseDto CreatePaymentGatewayResponseDto(NameValueCollection response)
        {
            /**** EXAMPLE RESPONSE *****
            PM=CreditCard&
            ACCEPTANCE=test123&
            STATUS=5&
            CARDNO=XXXXXXXXXXXX1111&
            ED=0214&CN=Card+Test&
            TRXDATE=04%2f04%2f13&
            PAYID=20411985&
            NCERROR=0&
            BRAND=VISA&IP=93.124.227.4&
            SHASIGN=2D079C3D76F6C2818AB0EC58818393FE13F9A098
            */
            var dto = new PaymentGatewayResponseDto();
            dto.CardNumber = AssignResponseParameter(response, "CARDNO");
            dto.PaymentGatewayAuthorizationId = AssignResponseParameter(response, "PAYID");
            dto.Brand = AssignResponseParameter(response, "BRAND");
            try
            {
                dto.TransactionDate = DateTime.Parse(AssignResponseParameter(response, "TRXDATE"));
            }
            catch (Exception)
            {
             // BURY IT
                dto.TransactionDate = DateTime.Now;
            }
            
            dto.PaymentGatewayStatus = AssignResponseParameter(response, "STATUS");
            dto.PaymentMethod = AssignResponseParameter(response, "PM");
            dto.PayId = AssignResponseParameter(response, "PAYID");
            dto.ErrorDescription = AssignResponseError(response);
            return dto;
        }

        private static string AssignResponseParameter(NameValueCollection response, string parameterName)
        {
            if (response[parameterName] == null)
            {
                return string.Empty;
            }

            return response[parameterName];
        }

        private static string AssignResponseError(NameValueCollection response)
        {
            return string.Format("Status: {0}. Acquirer Response: {1}. Error Code: {2}.", response["STATUS"], response["ACCEPTANCE"], response["NCERROR"]);
        }        
    }    
}
