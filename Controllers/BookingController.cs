﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Entity.Validation;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using Resources;
using SEB.Service;
using SEB.Entities;
using System.IO;
using SolutionTemplate.ExternalServices;
using SolutionTemplate.ExternalServices.Dto;
using SolutionTemplate.ExternalServices.Impl;
using Xeed.Diagnostics.Errors;
using Xeed.Runtime.Serialization;
using System.Xml.Serialization;
using System.Data.SqlClient;
using System.Reflection;
using AutoMapper;


namespace SEB.Controllers
{
    public class BookingController : BaseController
    {
        private BookMakerContext bm = new BookMakerContext();
        private SEBContext db = new SEBContext();

        private string LogFileLocation = System.Configuration.ConfigurationManager.AppSettings["LogFileLocation"];

        [Authorize]
        public ActionResult Public()
        {
            return View();
        }


        //
        // GET: /Booking/
        
        public ActionResult BookingSearch()
        {
            if (Session == null || Session["chosenLanguage"] == null)
            {
                return RedirectToAction("bookingdetails", "booking");
            }

            DCReservation bookings = new DCReservation();
            //////ViewBag.LastName = db.Users.Find(User.Identity.Name).LastName;
            ViewBag.Message = string.Empty;

            var confirmed = new List<Reservation>();
            var cancelled = new List<Reservation>();

            if (User.Identity.IsAuthenticated)
            {

                confirmed = db.Reservations.Where(c => c.Reservation_UserID == User.Identity.Name && (c.Reservation_Status == "WIZARD CONFIRMED" || c.Reservation_Status == "UPDATED") && c.Reservation_DCID != null).OrderByDescending(o => o.Reservation_Updated).ToList();
                cancelled = db.Reservations.Where(c => c.Reservation_UserID == User.Identity.Name && c.Reservation_Status == "CANCELLED" && c.Reservation_DCID != null && (db.Reservations.Where(b => b.Reservation_Number_Cancelled == c.Reservation_DCID).Any() == false)).OrderByDescending(o => o.Reservation_Updated).ToList();

                foreach (var reservation in confirmed)
                {
                    reservation.Reservation_PickupLocation = FindLocation(reservation.Reservation_PickupLocation).Location_Name; //                    bm.Locations.First(l => l.Location_Code == reservation.Reservation_PickupLocation).Location_Name;
                    reservation.Reservation_DropoffLocation = FindLocation(reservation.Reservation_DropoffLocation).Location_Name; // bm.Locations.First(l => l.Location_Code == reservation.Reservation_DropoffLocation).Location_Name;
                }

                foreach (var reservation in cancelled)
                {
                    reservation.Reservation_PickupLocation = FindLocation(reservation.Reservation_PickupLocation).Location_Name;//                    bm.Locations.First(l => l.Location_Code == reservation.Reservation_PickupLocation).Location_Name;
                    reservation.Reservation_DropoffLocation = FindLocation(reservation.Reservation_DropoffLocation).Location_Name;//                    bm.Locations.First(l => l.Location_Code == reservation.Reservation_DropoffLocation).Location_Name;
                }

            }
            else
            {
                ViewBag.Anonymous = true;
            }
            ViewBag.Confirmed = confirmed;
            ViewBag.Cancelled = cancelled;


            return View(bookings);
        }

       
        [HttpPost]
        public ActionResult BookingSearch(FormCollection frm, string button)
        {
            if (Session == null || Session["chosenLanguage"] == null)
            {
                return RedirectToAction("bookingdetails", "booking");
            }

            var usr = db.Users.Where(u => u.Email == User.Identity.Name).FirstOrDefault();
            var lastName = string.Empty; //usr.LastName;

            lastName = lastName.Replace("Å", "A");
            lastName = lastName.Replace("Ä", "A");
            lastName = lastName.Replace("Æ", "A");
            lastName = lastName.Replace("Ø", "O");
            lastName = lastName.Replace("Ö", "O");

            lastName = lastName.Replace("ö", "o");
            lastName = lastName.Replace("ä", "a");
            lastName = lastName.Replace("å", "a");
            lastName = lastName.Replace("ø", "o");
            lastName = lastName.Replace("æ", "a");

            var r = new DCReservation();
            var dcid = frm["txtResNo"];
            var inv = int.Parse(string.IsNullOrEmpty(frm["txtInvoiceNumber"]) ? "0" : frm["txtInvoiceNumber"]);
            var firstname = frm["txtFirstName"];
            var lastname = frm["txtLastName"];
            var licence = frm["txtLicenseNumber"];

            if (!User.Identity.IsAuthenticated && (string.IsNullOrEmpty(dcid.Trim()) || string.IsNullOrEmpty(firstname.Trim()) || string.IsNullOrEmpty(lastname.Trim()) || string.IsNullOrEmpty(licence.Trim())))
            {
                //return RedirectToAction("bookingsearch", "booking");
                ViewBag.Message = Resources.Copy.Info_NoReservations;
                return View();
            }

            var sw_reservation = new Reservation();
            var query = db.Reservations.ToList();

            if (!string.IsNullOrEmpty(dcid))
            {
                var dcidLower = dcid.ToLower();
                query = query.Where(q => q.Reservation_DCID.ToLower() == dcidLower).ToList();
            }
            if (inv > 0)
            {
                query = query.Where(q => q.InvoiceNumber == inv).ToList();
            }

            if (!string.IsNullOrEmpty(firstname))
            {
                var firstnameLower = firstname.ToLower();
                query = query.Where(q => q.Reservation_DriverFirstName.ToLower() == firstnameLower).ToList();
            }
            if (!string.IsNullOrEmpty(lastname))
            {
                var lasttnameLower = lastname.ToLower();
                query = query.Where(q => q.Reservation_DriverLastName.ToLower() == lasttnameLower).ToList();
            }
            if (!string.IsNullOrEmpty(licence))
            {
                var licenseLower = licence.ToLower();
                query = query.Where(q => q.Reservation_Driver.ToLower() == licenseLower).ToList();
            }

            sw_reservation = query.FirstOrDefault();

            if (sw_reservation != null)
            {
                var serializer = new XmlSerializer(r.GetType());
                var deserialized = serializer.Deserialize(new StringReader(sw_reservation.Reservation_FormState));
                r = (DCReservation) deserialized;
                ViewBag.Status = sw_reservation.Reservation_Status;
                dcid = sw_reservation.Reservation_DCID;
            }
            else
            {
                ViewBag.Message = Resources.Copy.Info_NoReservations;
                return View();
            }

            //if (r != null && r.DriverSurname != null)
            //{
            //    r = RebuildExtras(r, sw_reservation.Reservation_ID);
            //}
            if (r.Extras == null)
            {
                r.Extras = new DCExtra[0];
            }



            var Pickup = FindLocation(r.PickupLocation); // bm.Locations.First(p => p.Location_Code == r.PickupLocation);
            var Return = FindLocation(r.ReturnLocation);// bm.Locations.First(p => p.Location_Code == r.ReturnLocation);

            ViewBag.Pickup = GetOpeningHoursForLocation(Pickup);
            ViewBag.Return = GetOpeningHoursForLocation(Return);

            var temp = db.Reservations.FirstOrDefault(i => i.InvoiceReady == 1 && i.Reservation_DCID == r.ID);
            if (temp != null) ViewBag.InvoiceNumber = temp.InvoiceNumber;

            TimeSpan ts = r.ReturnDateTime - r.PickupDateTime;
            ViewBag.Days = ts.Days;
            if (ts.Hours > 0) ViewBag.Days += 1;

            ViewBag.VoucherValue = sw_reservation.VoucherValue != null ? (Decimal)sw_reservation.VoucherValue : 0M;
            ViewBag.Total = sw_reservation.Reservation_Amount != null ? (Decimal) sw_reservation.Reservation_Amount : 0M; // r.Amount;
            r.Amount = sw_reservation.Reservation_Amount != null ? (Decimal)sw_reservation.Reservation_Amount : 0M;

            foreach (var ex in r.Extras)
            {
                ViewBag.Total += ex.Total;
            }



            return View(r);
        }

        [Authorize]
        public ActionResult ViewBooking(string id)
        {
            var reservation = new DCReservation();
            var sw_reservation = db.Reservations.FirstOrDefault(b => b.Reservation_DCID == id);
            DCReservation r = new DCReservation();
            

            if(sw_reservation != null)
            {
                var serializer = new XmlSerializer(reservation.GetType());
                var deserialized = serializer.Deserialize(new StringReader(sw_reservation.Reservation_FormState));
                reservation = (DCReservation)deserialized;
                ViewBag.Status = sw_reservation.Reservation_Status;
            }
            else
            {
                ViewBag.Message = Resources.Copy.Info_NoReservations;
            }

            //r = RebuildExtras(reservation, sw_reservation.Reservation_ID);

            if(r.Extras == null)
            {
                r.Extras = new DCExtra[0];
            }


            var Pickup = FindLocation(r.PickupLocation); // bm.Locations.First(p => p.Location_Code == r.PickupLocation);
            var Return = FindLocation(r.ReturnLocation); // bm.Locations.First(p => p.Location_Code == r.ReturnLocation);

            ViewBag.Pickup = GetOpeningHoursForLocation(Pickup);
            ViewBag.Return = GetOpeningHoursForLocation(Return);

            var temp = db.Reservations.FirstOrDefault(i => i.InvoiceReady == 1 && i.Reservation_DCID == r.ID);
            if (temp != null) ViewBag.InvoiceNumber = temp.InvoiceNumber;

            TimeSpan ts = r.ReturnDateTime - r.PickupDateTime;
            ViewBag.Days = ts.Days;
            if (ts.Hours > 0) ViewBag.Days += 1;

            return View("BookingSearch", r);

        }


       
        [HttpPost]
        public ActionResult editBooking(FormCollection frm)
        {

            var dcid = frm["ResNumber"];

            var res = db.Reservations.FirstOrDefault(r => r.Reservation_DCID == dcid);
            if (res != null)
            {
                TextReader reader = new StringReader(res.Reservation_FormState);
                var s = new XmlSerializer(typeof(DCReservation));
                var reservation = (DCReservation) s.Deserialize(reader);
                Session["reservation"] = reservation;
                Session["editmode"] = "true";
                if (!string.IsNullOrEmpty(reservation.DeliveryAddress1)) Session["IsDelivery"] = "true";
                if (!string.IsNullOrEmpty(reservation.CollectionAddress1)) Session["IsCollection"] = "true";
                if (reservation.PickupLocation != reservation.ReturnLocation) Session["SameReturn"] = "false";
            }

                return RedirectToAction("bookingdetails", "Booking");

        }

        private string CancelReservation (string resId, string surname)
        {

            var company = db.Reservations.FirstOrDefault(r => r.Reservation_DCID == resId).RentalCompany;

            if (company == "AVIS")
            {
                using (var client = new ServiceClient())
                {
                    var request = new ReservationCancelRequest()
                    {
                        ReservationID = resId,
                        LastName = surname
                    };

                    if (System.Configuration.ConfigurationManager.AppSettings["log"] == "true")
                        Log("reservation_cancel_request", "reservation_cancel_request", request);

                    ReservationCancelResponse response = new ReservationCancelResponse();


                    try
                    {
                        //logger.Info("Cancelling reservation number:" + resId);

                        response = ConfigurationManager.AppSettings["MockWebService"] == "true" ? RebuildReservationCancelResponse() : client.ReservationCancel(request);
                    }
                    catch (Exception ex)
                    {
                        logger.Error("Error when cancelling reservation number:" + resId, ex);

                        return "error";
                    }

                    client.Close();

                    if (response.Error != null)
                    {
                        logger.Info("Cancellation response error:" + response.Error.Message);

                        return "error";
                    }
                    if (response.Reservation != null && response.Reservation.Errors != null && response.Reservation.Errors.Any() && response.Reservation.Status.ToLower() != "cancelled")
                    {
                        logger.Info("Cancel reservation issue: " + response.Reservation.Errors[0].Value);

                        return response.Reservation.Errors[0].Value;
                    }

                }
            }
            else
            {
                using (var client = new SEB.Maestro.ServiceClient())
                {
                    var request = new SEB.Maestro.ReservationCancelRequest()
                    {
                        ReservationID = resId,
                        LastName = surname
                    };

                    if (System.Configuration.ConfigurationManager.AppSettings["log"] == "true")
                        Log("reservation_cancel_request", "reservation_cancel_request", request);

                    var response = new SEB.Maestro.ReservationCancelResponse();


                    try
                    {
                        //logger.Info("Cancelling reservation number:" + resId);

                        response = client.ReservationCancel(request);
                    }
                    catch (Exception ex)
                    {
                        logger.Error("Error when cancelling reservation number:" + resId, ex);

                        return "error";
                    }

                    client.Close();

                    if (response.Error != null)
                    {
                        logger.Info("Cancellation response error:" + response.Error.Message);

                        return "error";
                    }
                    if (response.Reservation != null && response.Reservation.Errors != null && response.Reservation.Errors.Any() && response.Reservation.Status.ToLower() != "cancelled")
                    {
                        logger.Info("Cancel reservation issue: " + response.Reservation.Errors[0].Value);

                        return response.Reservation.Errors[0].Value;
                    }

                }
            }

            var res = db.Reservations.FirstOrDefault(re => re.Reservation_DCID == resId);
            
                //CommonHelper.SendMail("Avbokningsbekräftelse",
                //                      "Din bokning, med bokningsnummer: " + resId +
                //                      " har avbokats. Vänligen se bokningshistoriken för mer information.",
                //                      driveremail, "",
                //                      ConfigurationManager.AppSettings["ReservationConfirmBCC"]);

                var rs = new DCReservation();
                var serializer = new XmlSerializer(rs.GetType());
                var deserialized = serializer.Deserialize(new StringReader(res.Reservation_FormState));
                rs = (DCReservation)deserialized;

            if (!string.IsNullOrEmpty(rs.DriverEmail))
            {

                string html;
                var Pickup = FindLocation(rs.PickupLocation); // bm.Locations.First(p => p.Location_Code == r.PickupLocation);
                var Return = FindLocation(rs.ReturnLocation);// bm.Locations.First(p => p.Location_Code == r.ReturnLocation);

                ViewBag.Pickup = GetOpeningHoursForLocation(Pickup);
                ViewBag.Return = GetOpeningHoursForLocation(Return);

                if (res.RentalCompany == "AVIS")
                {
                    html = RenderRazorViewToString("BookingCancelled", rs);
                }
                else
                {
                    html = RenderRazorViewToString("BookingCancelledBudget", rs);
                }

                CommonHelper.SendMail(Resources.Copy.CancellationEmailSubject, html, rs.DriverEmail, "", "");
            }
            return "Cancelled";
        }

       
        [HttpPost]
        public ActionResult CancelBooking(FormCollection frm, string id)
        {
            var status = CancelReservation(frm["ResNumber"], frm["Surname"]);
            var dcid = frm["ResNumber"];
            if (status == "Cancelled")
            {
                var res = db.Reservations.First(s => s.Reservation_DCID == dcid);
                res.Reservation_Status = "CANCELLED";
                db.SaveChanges();

                Session["reservation"] = null;

                return RedirectToAction("BookingCancelled", "Booking");
            }
            TempData["error"] = @Resources.Copy.CancelError;
            return RedirectToAction("BookingSearch", "Booking");

        }

        [Authorize]
        public ActionResult CancelListBooking(string id)
        {
            var status = CancelReservation(id, db.Reservations.First(u => u.Reservation_DCID == id).Reservation_DriverLastName);

            if (status == "Cancelled")
            {
                var res = db.Reservations.First(s => s.Reservation_DCID == id);
                res.Reservation_Status = "CANCELLED";
                db.SaveChanges();

                Session["reservation"] = null;
                return RedirectToAction("BookingCancelled", "Booking");
            }
            TempData["error"] = @Resources.Copy.CancelError;
            return RedirectToAction("BookingSearch", "Booking");

        }

       
        public ActionResult BookingCancelled()
        {
            return View("SiteBookingCancelled");
        }

        

        public void setCardMakeCookie(string cardMake)
        {
            HttpCookie cardCookie = Request.Cookies["_cardMake"];
            if (cardCookie != null)
            {
                cardCookie.Value = cardMake;
            }
            else
            {
                HttpCookie cookie = new HttpCookie("_cardMake", cardMake);
                Response.Cookies.Add(cookie);
            }
        }

        public ActionResult BookingDetails()
        {
            if (Session["chosenCardmake"] == null && Request.Url.ToString().ToLower().Contains("bookingdetails/eurocard"))
            {
                Session["chosenCardmake"] = "EC"; 
                setCardMakeCookie(Session["chosenCardmake"].ToString());
            }
            if (Session["chosenCardmake"] == null && Request.Url.ToString().ToLower().Contains("bookingdetails/dinersclub"))
            {
                Session["chosenCardmake"] = "DC"; 
                setCardMakeCookie(Session["chosenCardmake"].ToString());
            }

            var fromEdit = true;

            if (Request["ctx"] == "new")
            //if (Session["editmode"] != "true")
            {
                Session["reservation"] = null;
                Session["IsDelivery"] = null;
                Session["IsCollection"] = null;
                fromEdit = false;
                Session["SameReturn"] = null;
            
           
            Session["Project_Full"] = null;
            Session["ProjectNumber"] = null;
            Session["Company_Name"] = null;
            Session["editmode"] = null;
            
            }

            Session["RateCode"] = null;
            Session["AWDNumber"] = null;

            ViewBag.fromEdit = fromEdit.ToString().ToLower();

            if (Request["item"] != null)
            {
                var p = helpers.CMSHelper.GetCMSItemParameters(int.Parse(Request["item"]));
                try { ViewBag.minDays = p["mindays"] == null ? 0 : int.Parse(p["mindays"]); } catch { ViewBag.minDays = 0; }
                try { ViewBag.maxDays = p["maxdays"] == null ? 30 : int.Parse(p["maxdays"]); } catch { ViewBag.maxDays = 30; }
                try { Session["AWDNumber"] = p["awdnumber"] == null ? null : p["awdnumber"]; } catch { Session["AWDNumber"] = null;}
                try { Session["RateCode"] = p["ratecode"] == null ? null : p["ratecode"]; } catch { Session["RateCode"] = null; }
                ViewBag.DisplayTimeConstraints = true;
            }
            else
            {
                ViewBag.minDays = 0;
                ViewBag.maxDays = 30;
            }

            var reservation = (DCReservation)Session["reservation"] ?? new DCReservation();

            

            if (reservation.PickupLocationCountry == null)
            {
                reservation.PickupLocationCountry = SetCountryCode();
                reservation.PickupLocationCountryName = SetCountryName();
                reservation.PickupDateTime = new DateTime(DateTime.Now.AddDays(2).Year, DateTime.Now.AddDays(2).Month,
                                                          DateTime.Now.AddDays(2).Day, 9, 0, 0);
            }
            if (reservation.ReturnLocationCountry == null)
            {
                reservation.ReturnLocationCountry = SetCountryCode();
                reservation.ReturnLocationCountryName = SetCountryName();
                reservation.ReturnDateTime = new DateTime(DateTime.Now.AddDays(3).Year, DateTime.Now.AddDays(3).Month,
                                                          DateTime.Now.AddDays(3).Day, 9, 0, 0);
            }

            Session["reservation"] = reservation;

            var cnty = bm.Regions.Where(r=>r.Language_ID == Culture).AsEnumerable()
                            .OrderBy(o => o.Country_Name);

            var allCountries = cnty.OrderBy(o => o.Country_Name)
                .Select(x => new { CountryName = x.Country_Name, CountryCode = x.Country_Code })
                .Distinct();

            var ResidenceCountries = new List<SelectListItem>();
            foreach (var x in allCountries)
            {
                var li = new SelectListItem { Text = x.CountryName, Value = x.CountryCode };

                //if (User.Identity.IsAuthenticated && x.CountryCode == )
                //{
                //    li.Selected = true;
                //}
                ResidenceCountries.Add(li);
            }
            ViewData["ResidenceCountries"] = ResidenceCountries;

            //var n = bm.Regions.AsEnumerable()
            //    .Where(x => x.Country_Name == "Norway" || x.Country_Name == "Sweden" || x.Country_Name == "Denmark")
            //    .OrderBy(o => o.Country_Name)
            //    .Select(x => new {CountryName = x.Country_Name, CountryCode = x.Country_Code})
            //    .Distinct();

            var n = cnty.OrderBy(o => o.Country_Name)
                .Select(x => new {CountryName = x.Country_Name, CountryCode = x.Country_Code})
                .Distinct();

            var PickUpCountries = new List<SelectListItem>();
            string CountryCode = string.Empty;
            foreach (var x in n)
            {
                var li = new SelectListItem {Text = x.CountryName, Value = x.CountryCode};
                if (li.Text.StartsWith(reservation.PickupLocationCountryName))
                {
                    li.Selected = true;
                    CountryCode = li.Value;
                }
                PickUpCountries.Add(li);
            }
            ViewData["PickUpCountries"] = PickUpCountries;

            var ReturnCountries = new List<SelectListItem>();
            foreach (var x in n)
            {
                var li = new SelectListItem {Text = x.CountryName, Value = x.CountryCode};
                if (li.Text.Contains(reservation.ReturnLocationCountryName))
                {
                    li.Selected = true;
                    CountryCode = li.Value;
                }
                ReturnCountries.Add(li);
            }
            ViewData["ReturnCountries"] = ReturnCountries;


            if (reservation.PickupLocation == null)
            {
                //////var unit = db.Users.FirstOrDefault(u => u.UserName == User.Identity.Name).DefaultStation;
                var location = bm.Locations.FirstOrDefault(l => l.Location_Code == reservation.PickupLocation && l.Location_Language_ID == this.Culture);
                if (location != null)
                {
                    reservation.PickupLocation = location.Location_Code;
                    reservation.PickupLocationName = location.Location_Name;
                }
            }

            if (reservation.ReturnLocation == null)
            {
                //////var unit = db.Units.FirstOrDefault(u => u.LoginId == User.Identity.Name).DefaultStation;
                var location = bm.Locations.FirstOrDefault(l => l.Location_Code == reservation.ReturnLocation && l.Location_Language_ID == this.Culture);
                if (location != null)
                {
                    reservation.ReturnLocation = location.Location_Code;
                    reservation.ReturnLocationName = location.Location_Name;
                }
            }



            List<Entities.Location> locations = bm.Locations.Where(l => l.Region_Code.StartsWith(CountryCode) && l.Location_Language_ID == this.Culture).OrderBy(o => o.Location_Name).ToList();
            List<SelectListItem> PickUpLocations = new List<SelectListItem>();
            foreach (Entities.Location l in locations)
            {
                SelectListItem li = new SelectListItem { Text = l.Location_Name, Value = l.Location_Code };
                if (li.Value == reservation.PickupLocation) { li.Selected = true; }
                PickUpLocations.Add(li);
            }

            List<SelectListItem> ReturnLocations = new List<SelectListItem>();
            foreach (Entities.Location l in locations)
            {
                SelectListItem li = new SelectListItem { Text = l.Location_Name, Value = l.Location_Code };
                if (li.Value == reservation.ReturnLocation) { li.Selected = true; }
                ReturnLocations.Add(li);
            }
            ViewData["PickUpLocations"] = PickUpLocations;
            ViewData["ReturnLocations"] = ReturnLocations;

            //ViewBag.Projects = new SelectList(db.Projects.OrderBy(o => o.ProjectName), "ProjectNo", "ProjectName");

            List<SelectListItem> PickUphours = new List<SelectListItem>();
            for (int h = 1; h < 25; h++)
            {
                SelectListItem li = new SelectListItem {Text = pad(h.ToString()), Value = h.ToString()};
                if (reservation.PickupDateTime.Hour == h)
                {
                    li.Selected = true;
                }
                PickUphours.Add(li);
                //PickUphours.Add(new SelectListItem() { Text = pad(h.ToString()), Value = h.ToString(), Selected = reservation.PickupDateTime.Hour == h});
            }
            ViewData["PickUpHour"] = PickUphours;

            List<SelectListItem> PickUpmins = new List<SelectListItem>();
            for (int h = 0; h < 51; h += 10)
            {
                PickUpmins.Add(new SelectListItem()
                                   {
                                       Text = pad(h.ToString()),
                                       Value = h.ToString(),
                                       Selected = reservation.PickupDateTime.Minute == h ? true : false
                                   });
            }
            ViewData["PickUpMin"] = PickUpmins;

            List<SelectListItem> Returnhours = new List<SelectListItem>();
            for (int h = 1; h < 25; h++)
            {
                Returnhours.Add(new SelectListItem()
                                    {
                                        Text = pad(h.ToString()),
                                        Value = h.ToString(),
                                        Selected = reservation.ReturnDateTime.Hour == h ? true : false
                                    });
            }
            ViewData["Returnhour"] = Returnhours;

            List<SelectListItem> Returnmins = new List<SelectListItem>();
            for (int h = 0; h < 51; h += 10)
            {
                Returnmins.Add(new SelectListItem()
                                   {
                                       Text = pad(h.ToString()),
                                       Value = h.ToString(),
                                       Selected = reservation.ReturnDateTime.Minute == h ? true : false
                                   });
            }
            ViewData["Returnmin"] = Returnmins;

            if (Request["ctx"] == "NR")
            {
                ViewBag.Message = "<script>alert('" + HttpUtility.HtmlDecode(Resources.Copy.Info_NoCars) + "');</script>";
            }
            if (Request["ctx"] == "returnLocationClosed")
            {
                ViewBag.Message = "<script>alert('" + HttpUtility.HtmlDecode(Resources.Copy.Info_ReturnLocationClosed) + "');</script>";
            }

            return View(reservation);
        }

        private string SetCountryCode()
        {

            return Session["chosenLanguage"] == null ? "NO" : Session["chosenLanguage"].ToString();
            //switch (c)
            //{
            //    case "sv-SE":
            //        return "SE";
            //    case "da-DK":
            //        return "DK";
            //    case "nb-NO":
            //        return "NO";
            //    default:
            //        return "SE";

            //}
        }

        private DCReservation SetCultureAccountDetails(DCReservation reservation)
        {
            string c = Session["culture"].ToString();
            var site = db.Sites.FirstOrDefault(s => s.FullCultureCode == c);

            reservation.PaymentCID = site.PaymentCID;
            reservation.WizardNumber = site.WizardNumber;
            reservation.IATANumber = site.IATANumber;
            reservation.AWDNumber = site.AWDNumber;

            return reservation;
        }

        private string SetCountryName()
        {

            string c = Session["culture"] == null ? "nb-NO" : Session["culture"].ToString();
            switch (c)
            {
                case "sv-SE":
                    return "Sverige";
                case "da-DK":
                    return "Danmark";
                case "nb-NO":
                    return "Norge";
                case "fi-FI":
                    return "Finland";
                default:
                    return "Norway";

            }
        }

        private string pad(string val)
        {
            if (val.Length == 1)
            {
                return "0" + val;
            }
            return val;
        }

        SEB.Maestro.RateListResponse responseBudget;
        private void GetBudgetRates(SEB.Maestro.RateListRequest request)
        {
            try
            {
                using (var client = new SEB.Maestro.ServiceClient())
                {
                    responseBudget = client.RateList(request);
                    client.Close();
                }
            }
            catch (Exception ex)
            {
                logger.Error("Error retrieving Budget rates", ex);
            }
        }

        [HttpPost]
        public ActionResult ChooseACar(FormCollection frm)
        {
            if (Session == null || Session["chosenLanguage"] == null)
            {
                return RedirectToAction("bookingdetails", "booking");
            }

            var reservation = Session["reservation"] as Service.DCReservation;

            if (reservation == null) reservation = new DCReservation();

            //if (frm["chkNoFlight"] == "on")
            //{
            //    reservation.ArrivalFlightNumber = "WI";
            //    reservation.ArrivalFlightAirline = null;
            //    reservation.ArrivalFlightCode = "24";
            //}
            //else
            //{
            //    var codePart = string.Empty;
            //    var letters = "abcdefghijklmnopqrstuvwxyz";
            //    for (var p = 0; p < frm["FlightNumber"].Trim().Length; p++)
            //    {
            //        if (letters.Contains(frm["FlightNumber"].Trim().ToLower().Substring(p, 1)))
            //        {
            //            codePart = codePart + frm["FlightNumber"].Trim().ToLower().Substring(p, 1);
            //        }
            //        else
            //        {
            //            break;
            //        }
            //    }

            //    var airline = db.Airlines.FirstOrDefault(a => a.AirlineCode == codePart);
            //    if (airline != null)
            //    {
            //        reservation.ArrivalFlightNumber = frm["FlightNumber"];
            //        reservation.ArrivalFlightAirline = codePart.ToUpper();
            //        reservation.ArrivalFlightCode = "14";
            //    }
            //    else
            //    {
            //        reservation.ArrivalFlightNumber = frm["FlightNumber"];
            //        reservation.ArrivalFlightAirline = codePart.ToUpper();
            //        reservation.ArrivalFlightCode = "26";
            //    }
            //}

            reservation.PickupLocationCountry = frm["PickUpCountry"];
            reservation.PickupLocationCountryName =
                bm.Regions.Where(r => r.Country_Code == reservation.PickupLocationCountry && r.Language_ID == Culture).First().Country_Name;
            ViewBag.PickupRegionCode =
                bm.Regions.Where(r => r.Country_Code == reservation.PickupLocationCountry && r.Language_ID == Culture).First().Region_Code;

            if (frm["chkSameReturn"] == "on")
            {
                reservation.ReturnLocationCountry = frm["PickUpCountry"];
                reservation.ReturnLocationCountryName =
                    bm.Regions.Where(r => r.Country_Code == reservation.PickupLocationCountry && r.Language_ID == Culture).First().Country_Name;
            }
            else
            {
                reservation.ReturnLocationCountry = frm["ReturnCountry"];
                reservation.ReturnLocationCountryName =
                    bm.Regions.Where(r => r.Country_Code == reservation.ReturnLocationCountry && r.Language_ID == Culture).First().Country_Name;
            }

            DateTime dateOut = DateTime.Parse(frm["RentalStartDate"]);
            DateTime dateIn = DateTime.Parse(frm["RentalEndDate"]);

            dateOut = dateOut.AddHours(double.Parse(frm["PickupDateTime.Hour"]));
            dateOut = dateOut.AddMinutes(double.Parse(frm["PickupDateTime.Minute"]));
            dateIn = dateIn.AddHours(double.Parse(frm["ReturnDateTime.Hour"]));
            dateIn = dateIn.AddMinutes(double.Parse(frm["ReturnDateTime.Minute"]));

            reservation.PickupLocationName = frm["PickUpLocations"];
            reservation.ReturnLocationName = frm["ReturnLocations"];

            reservation.CountryCode = frm["PickUpCountry"];

            var reg = bm.Locations.Where(l => l.Location_Name == reservation.PickupLocationName && l.Location_Language_ID == this.Culture).FirstOrDefault().Region_Code;
            ViewBag.RegionCode = reg;

            if (bm.Locations.Where(l => l.Location_Name == reservation.PickupLocationName && l.Location_Language_ID == this.Culture).FirstOrDefault() == null)
            {
                reservation.PickupLocation = frm["pickUpMnemonic"];
                reservation.PickupLocationName = bm.Locations.Where(x => x.Region_Code.StartsWith(reservation.CountryCode) && (x.Location_Code == reservation.PickupLocation) && x.Location_Language_ID == this.Culture).First().Location_Name;
            }
            else
            {
                reservation.PickupLocation =
                    bm.Locations.Where(l => l.Location_Name == reservation.PickupLocationName && l.Location_Language_ID == this.Culture).First().Location_Code;
            }


            var pickupDelay = 4;
            Session["IsDelivery"] = "false";
            Session["IsCollection"] = "false";
            if(!string.IsNullOrEmpty(frm["chkDeliver"]) && frm["chkDeliver"] == "on")
            {
                pickupDelay = 8;
                reservation.DeliveryAddress1 = frm["DeliveryAddress1"];
                reservation.DeliveryAddress2 = frm["DeliveryAddress2"];
                reservation.DeliveryPostCode = frm["DeliveryPostCode"];
                reservation.DeliveryInstructions1 = frm["DeliveryInstructions1"];
                Session["IsDelivery"] = "true";
            }

            if(!string.IsNullOrEmpty(frm["chkCollect"]) && frm["chkCollect"] == "on")
            {
                reservation.CollectionAddress1 = frm["CollectionAddress1"];
                reservation.CollectionAddress2 = frm["CollectionAddress2"];
                reservation.CollectionPostCode = frm["CollectionPostCode"];
                reservation.CollectionInstructions1 = frm["CollectionInstructions1"];
                Session["IsCollection"] = "true";
            }

            var msg = IsLocationOpenAndWithinTimeContraints(reservation.PickupLocation, dateOut, pickupDelay);
            bool cont = false;
            if (msg.ToUpper() != "OK")
            {
                ViewBag.Message = "<script type='text/javascript'>alert('" + HttpUtility.HtmlDecode(msg) + "'); window.location.href = ('" + CommonHelper.GetHostUrl() + "/booking/bookingdetails" + "');</script>";
                cont = true;
            }

            msg = IsLocationOpenAndWithinTimeContraints(reservation.PickupLocation, dateOut, pickupDelay, true);
            if (msg.ToUpper() != "OK")
            {
                try
                {
                    if (!bm.Locations.First(l => l.Location_Name == reservation.ReturnLocationName && l.Location_Language_ID == this.Culture).Location_AfterHoursReturn)
                    {
                        return RedirectToAction("bookingdetails", "booking", new { @ctx = "returnLocationClosed" });
                    }
                }
                catch
                {
                    return RedirectToAction("bookingdetails", "booking", new { @ctx = "returnLocationClosed" });

                }
            }


            reservation.PickupDateTime = dateOut;
            if (frm["chkSameReturn"] == "on")
            {
                reservation.ReturnLocation = reservation.PickupLocation;
                reservation.ReturnLocationCountry = frm["PickUpCountry"];
                reservation.ReturnLocationName = bm.Locations.Where(x => x.Region_Code.StartsWith(reservation.ReturnLocationCountry) && (x.Location_Code == reservation.ReturnLocation) && x.Location_Language_ID == this.Culture).First().Location_Name;
            }
            else
            {
                if (bm.Locations.Where(l => l.Location_Name == reservation.ReturnLocationName && l.Location_Language_ID == this.Culture).FirstOrDefault() == null)
                {
                    reservation.ReturnLocation = frm["returnMnemonic"];
                    reservation.ReturnLocationName = bm.Locations.Where(x => x.Region_Code.StartsWith(reservation.ReturnLocationCountry) && (x.Location_Code == reservation.ReturnLocation) && x.Location_Language_ID == this.Culture).First().Location_Name;

                }
                else
                {
                    reservation.ReturnLocation =
                        bm.Locations.First(l => l.Location_Name == reservation.ReturnLocationName && l.Location_Language_ID == this.Culture).Location_Code;
                }
                reservation.ReturnLocationCountry = frm["ReturnCountry"];
            }


            reservation.ReturnDateTime = dateIn;
            Session["reservation"] = reservation;

            //var countryCode = frm["CountryOfResidence"];
            //Session["CountryOfResidence"] = bm.Regions.FirstOrDefault(r => r.Country_Code == countryCode) != null ? bm.Regions.FirstOrDefault(r => r.Country_Code == countryCode).Country_Name : null;
            //if(User.Identity.IsAuthenticated)
            //{
            //    var u = db.Users.FirstOrDefault(f=>f.Email == User.Identity.Name).Country;
            //    var c = db.Countries.FirstOrDefault(xx => xx.id == u).CountryText;
            //    countryCode = bm.Regions.FirstOrDefault(r => r.Country_Name == c).Country_Code;
            //    Session["CountryOfResidence"] = c;
            //}

            var chosenLanguage = Session["chosenLanguage"].ToString();
            var cardType = frm["cardType"];
            var awdNumber = Session["AWDNumber"] == null ? db.AwdNumbers.First(a => a.CardType == cardType && a.Region == chosenLanguage).Number : Session["AWDNumber"].ToString();
            reservation.CountryCode = chosenLanguage;
            reservation.DriverCountry = chosenLanguage;
            reservation.AWDNumber = Session["AWDNumber"] == null ? awdNumber : Session["AWDNumber"].ToString();
            Session["cardType"] = cardType;
            //Session["AWDNumber"] = awdNumber;

            var company = string.Empty;
            if (reservation.ID != null)
            {
                company = db.Reservations.First(r => r.Reservation_DCID == reservation.ID).RentalCompany;
            }

            try
            {
                using (var client = new ServiceClient())
                {
                    var request = new RateListRequest
                                      {
                                          Filter = new DCRateFilter
                                                       {
                                                           CountryCode = chosenLanguage,
                                                           //CurrencyCode = "NOK", //currencyCode,
                                                           PickupLocationCode = reservation.PickupLocation,
                                                           PickupDateTime = reservation.PickupDateTime,
                                                           ReturnLocationCode = reservation.ReturnLocation,
                                                           ReturnDateTime = reservation.ReturnDateTime,
                                                           AirConditionPref = PreferLevelType.Preferred,
                                                           AirCondition = true,
                                                           TransmissionPref = PreferLevelType.Preferred,
                                                           Transmission = VehicleTransmissionType.Automatic,
                                                           ClassPref = PreferLevelType.Preferred,
                                                           ClassSize = DCVehicleSize.Economy,
                                                           TypePref = PreferLevelType.Preferred,
                                                           TypeCategory = DCVehicleCategory.Car,
                                                           AWDNumber = awdNumber  //Session["AWDNumber"].ToString()
                                                           
                                                       }
                                      };
                    

                    RateListResponse response = null;
                    //SEB.Maestro.RateListResponse budgetresponse = new SEB.Maestro.RateListResponse();

                    var budgetAWD = db.AwdNumbers.FirstOrDefault(a => a.Number == awdNumber).BudgetNumber;
                    if (string.IsNullOrEmpty(budgetAWD)) budgetAWD = awdNumber;
                    var pickup_budgetlocationmap = db.BudgetLocationMapping.FirstOrDefault(b => b.AvisLocation == reservation.PickupLocation);
                    var return_budgetlocationmap = db.BudgetLocationMapping.FirstOrDefault(b => b.AvisLocation == reservation.ReturnLocation);

                    if (System.Configuration.ConfigurationManager.AppSettings["MockWebService"] == "true")
                    {
                        response = RebuildRateListResponse();
                    }
                    else
                    {
                        var budgetrequest = new SEB.Maestro.RateListRequest
                        {
                            Filter = new SEB.Maestro.DCRateFilter
                            {
                                CurrencyCode = "NOK",
                                DriverCountry = chosenLanguage,
                                CountryCode = chosenLanguage,
                                PickupLocationCode = reservation.PickupLocation,
                                PickupDateTime = reservation.PickupDateTime,
                                ReturnLocationCode = reservation.ReturnLocation,
                                ReturnDateTime = reservation.ReturnDateTime,
                                AirConditionPref = SEB.Maestro.PreferLevelType.Preferred,
                                AirCondition = true,
                                TransmissionPref = SEB.Maestro.PreferLevelType.Preferred,
                                Transmission = SEB.Maestro.VehicleTransmissionType.Automatic,
                                ClassPref = SEB.Maestro.PreferLevelType.Preferred,
                                ClassSize = SEB.Maestro.DCVehicleSize.Economy,
                                TypePref = SEB.Maestro.PreferLevelType.Preferred,
                                TypeCategory = SEB.Maestro.DCVehicleCategory.Car,
                                AWDNumber = budgetAWD
                                //CountryCode = chosenLanguage,
                                //PickupLocationCode = pickup_budgetlocationmap.BudgetLocation,
                                //PickupDateTime = reservation.PickupDateTime,
                                //ReturnLocationCode = return_budgetlocationmap.BudgetLocation,
                                //ReturnDateTime = reservation.ReturnDateTime,
                                //AirConditionPref = SEB.Maestro.PreferLevelType.Preferred,
                                //AirCondition = true,
                                //TransmissionPref = SEB.Maestro.PreferLevelType.Preferred,
                                //Transmission = SEB.Maestro.VehicleTransmissionType.Automatic,
                                //ClassPref = SEB.Maestro.PreferLevelType.Preferred,
                                //ClassSize = SEB.Maestro.DCVehicleSize.Economy,
                                //TypePref = SEB.Maestro.PreferLevelType.Preferred,
                                //TypeCategory = SEB.Maestro.DCVehicleCategory.Car,
                                //AWDNumber = awdNumber
                            }
                        };


                        Thread th1 = null;
                       // var budgetClient = new SEB.Maestro.ServiceClient();
                        if ((reservation.ID == null || (!string.IsNullOrEmpty(company) && company == "BUDGET")) &&
                           ((pickup_budgetlocationmap != null && !string.IsNullOrEmpty(pickup_budgetlocationmap.BudgetLocation)) && (return_budgetlocationmap != null && !string.IsNullOrEmpty(return_budgetlocationmap.BudgetLocation))))
                        {
                            budgetrequest.Filter.PickupLocationCode = pickup_budgetlocationmap.BudgetLocation;
                            budgetrequest.Filter.ReturnLocationCode = return_budgetlocationmap.BudgetLocation;
                            th1 = new Thread(() => GetBudgetRates(budgetrequest));
                            th1.Start();
                        }
                        if(reservation.ID == null || (!string.IsNullOrEmpty(company) && company == "AVIS")) response = client.RateList(request);
                        if(th1 != null) th1.Join();
                    }

                    client.Close();

                    var avisOK = true;
                    var budgetOK = true;
                    var returnContext = string.Empty;

                    if (response == null)
                    {
                        avisOK = false;
                        returnContext = "NR";
                    }

                    if (response != null && response.Rates == null && !cont)
                    {
                        //return RedirectToAction("BookingDetails", "Booking", new {@ctx = "NR"});
                        avisOK = false;
                        returnContext = "NR";
                    }

                    if (response != null && response.Rates.Any() && response.Rates[0].CarGroup == null && !cont)
                    {
                        if (response.Rates[0].Errors[0].Text.Contains("32261"))
                        {
                            //return RedirectToAction("bookingdetails", "booking", new { @ctx = "returnLocationClosed" });
                            avisOK = false;
                            returnContext = "returnLocationClosed";

                        }
                        //return RedirectToAction("BookingDetails", "Booking", new {@ctx = "NR"});
                        avisOK = false;
                        returnContext = "NR";
                    }

                    ViewBag.Fleet = bm.Fleets.Where(f => f.Language_ID == Culture).ToList();

                    var AllRates = new List<SEBRate>();


                    if (response != null && response.Rates != null && response.Rates.Any())
                    {
                        foreach (var rte in response.Rates)
                        {
                            var sr = new SEBRate();

                            Type type = rte.GetType();
                            PropertyInfo[] properties = type.GetProperties();

                            foreach (PropertyInfo property in properties)
                            {
                                property.SetValue(sr, property.GetValue(rte, null), null);
                            }
                            sr.Company = "AVIS";
                            AllRates.Add(sr);
                        }
                    }

                    if (responseBudget != null && responseBudget.Rates != null && responseBudget.Rates.Any() && responseBudget.Rates[0].Errors.Count() == 0)
                    {
                        foreach (var rte in responseBudget.Rates)
                        {
                            var sr = new SEBRate();
                            try
                            {
                                sr.Company = "BUDGET";
                                sr.AirCondition = rte.AirCondition;
                                sr.AntiLockBreaks = rte.AntiLockBreaks;
                                sr.Automatic = rte.Automatic;
                                sr.AWDCode = rte.AWDCode;
                                sr.CarGroup = rte.CarGroup;
                                sr.Category = rte.Category;
                                sr.Charges = MapCharge(rte.Charges);
                                sr.Code = rte.Code;
                                sr.CurrencyCode = rte.CurrencyCode;
                                sr.DecimalPlaces = rte.DecimalPlaces;
                                sr.Description = rte.Description;
                                sr.DisplayImage = rte.DisplayImage;
                                sr.DistanceQuantity = rte.DistanceQuantity;
                                sr.DistanceRentalUnit = rte.DistanceRentalUnit;
                                sr.DistanceUnit = rte.DistanceUnit;
                                sr.DistanceUnlimited = rte.DistanceUnlimited;
                                sr.DoorCount = rte.DoorCount;
                                sr.DriverAirBag = rte.DriverAirBag;
                                sr.DriveType = rte.DriveType;
                                sr.ElectricWindow = rte.ElectricWindow;
                                sr.EstimatedTotal = rte.EstimatedTotal;
                                sr.EstimatedTotalStandard = rte.EstimatedTotalStandard;
                                sr.FourWheelDrive = rte.FourWheelDrive;
                                sr.FuelType = rte.FuelType;
                                sr.ID = rte.ID;
                                sr.LargeImage = rte.LargeImage;
                                sr.LuggageCount = rte.LuggageCount;
                                sr.ModelName = rte.ModelName;
                                sr.ModelYear = rte.ModelYear;
                                sr.Name = rte.Name;
                                sr.PassengerAirbag = rte.PassengerAirbag;
                                sr.PowerSteering = rte.PowerSteering;
                                sr.RadioCassette = rte.RadioCassette;
                                sr.RadioOnly = rte.RadioOnly;
                                sr.RateCategory = rte.RateCategory;
                                sr.RateCategoryStandard = rte.RateCategoryStandard;
                                sr.RateQualifier = rte.RateQualifier;
                                sr.RateQualifierStandard = rte.RateQualifierStandard;
                                sr.RateTotal = rte.RateTotal;
                                sr.RateTotalStandard = rte.RateTotalStandard;
                                sr.RearSeatbelts = rte.RearSeatbelts;
                                sr.SeatCount = rte.SeatCount;
                                sr.Size = rte.Size;
                                sr.SizeCode = rte.SizeCode;
                                sr.SizeIndex = rte.SizeIndex;
                                sr.SmallImage = rte.SmallImage;
                                sr.Status = rte.Status;
                                sr.SunRoof = rte.SunRoof;
                                sr.TransmissionType = rte.TransmissionType;
                            }
                            catch (Exception ex)
                            {

                            }

                            //Type type = rte.GetType();
                            //PropertyInfo[] properties = type.GetProperties();

                            //foreach (PropertyInfo property in properties)
                            //{
                            //    //property.SetValue(sr, CommonHelper.GetPropertyValue(rte, property.Name), null);
                            //    CommonHelper.setInstanceProperty(sr, property.Name, CommonHelper.GetPropertyValue(rte, property.Name));
                            //}

                            AllRates.Add(sr);
                        }
                    }
                    else
                    {
                        budgetOK = false;
                        returnContext = "NR";
                    }
                    Session["Rates"] = AllRates;

                    if (budgetOK == false && avisOK == false)
                    {
                        return RedirectToAction("bookingdetails", "booking", new { @ctx = returnContext });
                    }

                   //if(budgetresponse != null && budgetresponse.Rates != null && budgetresponse.Rates.Any()) ViewBag.BudgetRates = budgetresponse.Rates.Where(x => x.Status.ToLower() == "available").OrderBy(r => r.EstimatedTotal).ToList();

                    return View(AllRates.Where(x=>x.Status.ToLower()=="available").OrderBy(r => r.CarGroup).ThenBy(r=>r.EstimatedTotal).ToList());

                }
            }
            catch (Exception e)
            {
                //Log("rates", "", "error:  " + e.Message + Environment.NewLine + e.StackTrace);

            }


            return View(new List<SEBRate>());
        }

        public DCCharge[] MapCharge(Maestro.DCCharge[] maestroCharges)
        {
            DCCharge[] charge = new DCCharge[maestroCharges.Count()];
            var ctr = 0;
            foreach (var mc in maestroCharges)
            {
                charge[ctr] = new DCCharge { Name = mc.Name, Amount = mc.Amount, Description = mc.Description };
                ctr += 1;
            }
            return charge;

        }


        public ActionResult ChooseExtras(string carGroup, DCVehicleSize sizeCode, DCVehicleCategory category,
                                         string rateQualifier, decimal total, string carCurrencyCode, string carGroupLetter, string budget, string status, string company)
        {
            if (Session == null || Session["chosenLanguage"] == null)
            {
                return RedirectToAction("bookingdetails", "booking");
            }

            var reservation = Session["reservation"] as Service.DCReservation;
            if (reservation == null) return RedirectToAction("BookingDetails", "Booking");

            var car = new car();
            car.carGroup = carGroup;
            car.sizeCode = sizeCode;
            car.category = category;
            car.rateQualifier = rateQualifier;
            car.total = total;
            car.carCurrencyCode = carCurrencyCode;
            car.status = status;
            car.company = company;
            Session["car"] = car;

            reservation.RequestType = status;
            ViewBag.Fleet = bm.Fleets.Where(f => f.Language_ID == Culture).ToList();
            var reg = bm.Locations.Where(l => l.Location_Name == reservation.PickupLocationName && l.Location_Language_ID == this.Culture).FirstOrDefault().Region_Code;
            ViewBag.RegionCode = reg;
            Entities.Fleet Car = bm.Fleets.FirstOrDefault(f => f.Fleet_CarGroup == carGroupLetter && f.Language_ID == Culture && f.Region_Code == reg);

            if (Car == null && reg.Length > 3)
            {
                reg = reg.Substring(0, 2) + "0";
                Car = bm.Fleets.FirstOrDefault(x => x.Fleet_CarGroup == carGroupLetter && x.Region_Code == reg && x.Language_ID == Culture);
            }

            if (car != null)
            {
                reservation.VehicleMakeModelName = Car.Fleet_ModelName;
            }

            reservation.VehicleGroupValue = carGroup;
            reservation.VehicleSize = sizeCode;
            reservation.VehicleCategory = category;
            reservation.RateQualifier = rateQualifier;
            reservation.Amount = decimal.Parse(decimal.Round(total,2).ToString());
            reservation.RateCategory = DCRateCategory.All;
            reservation.CurrencyCode = carCurrencyCode;
            reservation.VehicleGroupType = "SIPP";
            Session["carGroupLetter"] = carGroupLetter;

            Session["reservation"] = reservation;

            ViewBag.equipmentCodes = bm.EquipmentCodes.ToList();

            var _ex = new EquipmentListResponse();
            using (ServiceClient s = new ServiceClient())
            {
                var extrasRequest = new EquipmentListRequest
                {
                    PickupDateTime = (DateTime)reservation.PickupDateTime,
                    PickupLocationCode = reservation.PickupLocation,
                    DropoffDateTime = (DateTime)reservation.ReturnDateTime,
                    DropoffLocationCode = reservation.ReturnLocation,
                    AwdNumber = reservation.AWDNumber,
                    CarGroup = car.carGroup,
                    LanguageID = Session["chosenLanguage"].ToString()
                };
                _ex = s.EquipmentList(extrasRequest);
            }

            var _exList = new List<DCCharge>();
            if (_ex != null && _ex.Charges != null)
            {
                _exList = _ex.Charges.ToList();
            }

            //int max = 0;
            //List<Entities.Equipment> extras;

            //try
            //{
            //    max = (int)bm.Equipments.Where(e => e.Location_Code == reservation.PickupLocation && e.Equipment_Description != "" && e.Language_ID == Culture).OrderByDescending(
            //            o => o.Import_ID).FirstOrDefault().Import_ID;
            //}
            //catch (Exception ex)
            //{
            //}

            //if(max > 0)
            //{
            //    extras =
            //        bm.Equipments.Where(e => e.Location_Code == reservation.PickupLocation && e.Equipment_Description != "" && e.Import_ID == max && e.Language_ID == Culture).Distinct().OrderBy(
            //            o => o.Equipment_AdditionalInfo).ToList();
            //}
            //else
            //{
            //    extras =
            //        bm.Equipments.Where(e => e.Location_Code == reservation.PickupLocation && e.Equipment_Description != "" && e.Language_ID == Culture).Distinct().OrderBy(
            //            o => o.Equipment_AdditionalInfo).ToList();
            //}
            
            ViewBag.CurrencyCode = carCurrencyCode;
            ViewBag.Reservation = reservation;

            try
            {
                var Rates = (List<SEBRate>)Session["Rates"];
                var companyName = Request["company"];
                ViewBag.Inclusions = Rates.First(r => r.Code == reservation.VehicleGroupValue && r.Company == companyName);
            }
            catch (Exception ex)
            {
            }
            
            ViewBag.PickupRegionCode =
                bm.Regions.Where(r => r.Country_Code == reservation.PickupLocationCountry && r.Language_ID == Culture).First().Region_Code;

            return View(_exList.Where(z => z.Type == "equipment" && (z.Code != null || z.Number != null)).Distinct().ToList());
            //return View(extras.Where(e => e.Equipment_Description.ToLower().Contains("child seat") == false && e.Equipment_Description.ToLower().Contains("player") == false && e.Equipment_Description.ToLower().Contains("rack") == false).ToList());
        }

        [HttpPost]
        public ActionResult ChooseExtras(FormCollection frm)
        {
            if (Session == null || Session["chosenLanguage"] == null)
            {
                return RedirectToAction("bookingdetails", "booking");
            }

            var reservation = Session["reservation"] as Service.DCReservation;
            if (reservation == null) return RedirectToAction("BookingDetails", "Booking");
            TimeSpan ts = reservation.ReturnDateTime - reservation.PickupDateTime;

            var _ex = new EquipmentListResponse();
            using (ServiceClient s = new ServiceClient())
            {
                var extrasRequest = new EquipmentListRequest
                {
                    PickupDateTime = (DateTime)reservation.PickupDateTime,
                    PickupLocationCode = reservation.PickupLocation,
                    DropoffDateTime = (DateTime)reservation.ReturnDateTime,
                    DropoffLocationCode = reservation.ReturnLocation,
                    AwdNumber = reservation.AWDNumber,
                    LanguageID = Session["chosenLanguage"].ToString()
                };
                _ex = s.EquipmentList(extrasRequest);
            }

            var _exList = new List<DCCharge>();
            if (_ex != null && _ex.Charges != null)
            {
                _exList = _ex.Charges.ToList().Where(z => z.Type == "equipment" && (z.Code != null || z.Number != null)).Distinct().ToList();
            }

            int days = ts.Days;
            if (ts.Hours > 0) days += 1;
            List<Entities.Equipment> extras =
                bm.Equipments.Where(e => e.Location_Code == reservation.PickupLocation).ToList();
            var dcExtras = new DCExtra[] {};
            int ctr = -1;
            foreach (var extra in _exList)
            {
                if (frm[extra.ID] != null)
                {
                    Service.DCExtra e = null;
                    if (extra.Code.Contains("CSS") || extra.Code == "ADD" || extra.Code.Contains("BSS"))
                    {
                        if (int.Parse(frm[extra.ID]) > 0)
                        {
                            //var equip =
                            //    bm.EquipmentCodes.Where(eq => eq.Equipment_Description == extra.Equipment_Description).
                            //        FirstOrDefault();
                            e = new DCExtra();
                            e.Code = extra.Code != null ? extra.Code : string.Empty;
                            e.Quantity = int.Parse(frm[extra.ID]);
                            e.Number = extra.Number != null ? extra.Number.ToString() : string.Empty;
                            e.Type = "";
                            e.Name = extra.Text;
                            e.Text = extra.ExtraInfo;
                            e.UnitName = "";
                            e.Availability = extra.Availability;
                            e.IncludedInRate = false;
                            e.Amount = extra.AmountTotal;

                            if (extra.UnitName.ToLower() == "day" || extra.UnitName.ToLower() == "dygn" || extra.UnitName.ToLower() == "dag")
                            {
                                e.Total = extra.AmountTotal*e.Quantity*days;

                            }
                            else
                            {
                                e.Total = extra.AmountTotal * e.Quantity;
                            }

                            if (e.Total > (extra.AmountMax*e.Quantity))
                            {
                                e.Total = extra.AmountMax*e.Quantity;
                            }
                            ctr += 1;
                        }

                    }
                    else
                    {
                        if (frm[extra.ID] == "on")
                        {
                            //var equip =
                            //    bm.EquipmentCodes.Where(eq => eq.Equipment_Description == extra.Equipment_Description).
                            //        FirstOrDefault();
                            
                            e = new DCExtra();

                            //e = Mapper.Map<DCExtra>(extra);

                            e.Code = extra.Code != null ? extra.Code.ToString() : string.Empty;
                            e.Quantity = 1;
                            e.Number = extra.Number != null ? extra.Number.ToString() : string.Empty; ;
                            e.Type = "";
                            e.Name = extra.Text;
                            e.Text = extra.ExtraInfo;
                            e.UnitName = "";
                            e.Availability = extra.Availability;
                            e.IncludedInRate = false;
                            e.Amount = extra.AmountTotal;
                            if (extra.UnitName.ToLower() == "day" || extra.UnitName.ToLower() == "dygn" || extra.UnitName.ToLower() == "dag")
                            {
                                e.Total = extra.AmountTotal*days;
                            }
                            else
                            {
                                e.Total = extra.AmountTotal;
                            }


                            if (e.Total > extra.AmountMax)
                            {
                                e.Total = extra.AmountMax;
                            }
                            ctr += 1;
                        }
                    }

                    if (e != null)
                    {
                        Array.Resize(ref dcExtras, ctr + 1);
                        dcExtras[ctr] = e;
                    }

                }
            }
            reservation.Extras = dcExtras;
            Session["reservation"] = reservation;

            return RedirectToAction("BookingSummary", "Booking");
        }


        public JsonResult GetLocations(string CountryCode)
        {

            var query = (from c in bm.Locations
                         where c.Region_Code.StartsWith(CountryCode) && c.Location_Language_ID == this.Culture
                         orderby c.Location_Name
                         select new { LocationId = c.Location_Code, LocationName = c.Location_Name});

            var x = Json(query.ToList());

            return Json(query.ToList());

        }

        //////public JsonResult GetQuickPickDetails(string id)
        //////{
        //////    var i = int.Parse(id);

        //////    var query = (from c in db.Units
        //////                 where c.ID == i
        //////                 select new { Line1 = c.Address1, Line2 = c.Address2 }).ToList();

        //////    return Json(query);

        //////}

        //////public JsonResult IsProjectNumberValid(string ProjectValue)
        //////{
        //////    var ProjNum = ProjectValue.Split('-').First().Trim();
        //////    Project p = db.Projects.Find(ProjNum);
        //////    if (p != null)
        //////    {
        //////        return Json("true");
        //////    }
        //////    else
        //////    {
        //////        return Json("false");
        //////    }
        //////}

        //////public JsonResult GetCompanyName(string ProjectValue)
        //////{
        //////    var ProjNum = ProjectValue.Split('-').First().Trim();
        //////    Project p = db.Projects.Find(ProjNum);
        //////    if (p != null)
        //////    {
        //////        var x = db.Companies.AsEnumerable().Where(z => z.CompanyID.ToString() == p.CompanyId).FirstOrDefault();
        //////        if (x != null)
        //////        {
        //////            return Json(x.CompanyName);
        //////        }
        //////    }
        //////    return Json(string.Empty);
        //////}

        private Location FindLocation(string locationCode)
        {
            if (bm.Locations.Where(p => p.Location_Code == locationCode && p.Location_Language_ID == this.Culture).FirstOrDefault() != null)
            {
                return bm.Locations.Where(p => p.Location_Code == locationCode && p.Location_Language_ID == this.Culture).FirstOrDefault();
            }
            var budgetLocation = db.BudgetLocationMapping.Where(b => b.BudgetLocation == locationCode).FirstOrDefault().AvisLocation;
            return bm.Locations.Where(p => p.Location_Code == budgetLocation && p.Location_Language_ID == this.Culture).FirstOrDefault();
        }


        public ActionResult BookingSummary()
        {
            if (Session == null || Session["chosenLanguage"] == null)
            {
                return RedirectToAction("bookingdetails", "booking");
            }

            //try
            //{
                if (Request["ctx"] != null && Request["ctx"].ToLower() == "bookingerror")
                {
                    var errMsg = string.Empty;
                    if (Session["ReservationError"] != null)
                    {
                        errMsg = CommonHelper.GetErrorTranslation(Session["ReservationError"].ToString());
                    }
                    if (string.IsNullOrEmpty(errMsg)) errMsg = Resources.Copy.BookingError;
                    ViewBag.Message = "<script>alert('" + errMsg + "');</script>";
                    Session["ReservationError"] = null;
                }
                if (Request["ctx"] != null && Request["ctx"].ToLower() == "paymenterror")
                {
                    ViewBag.Message = "<script>alert('" + Resources.Copy.PaymentError + "');</script>";
                }

                
                var reservation = Session["reservation"] as Service.DCReservation;
                if (reservation == null) return RedirectToAction("BookingDetails", "Booking");
                var currentUser = db.Users.FirstOrDefault(u => u.Email == User.Identity.Name);

                if (User.Identity.IsAuthenticated)
                {
                    ViewBag.user = currentUser;
                }
                else
                {
                    var u = new User();

                    if (reservation.ID != null)
                    {
                        var s_Reservation = db.Reservations.FirstOrDefault(r => r.Reservation_DCID == reservation.ID);

                        u.FirstName = reservation.DriverGivenName;
                        u.LastName = reservation.DriverSurname;
                        u.DateOfBirth = (DateTime)reservation.DriverBirthDate;
                        u.Address1 = s_Reservation.Driver_Address1;
                        u.Address2 = s_Reservation.Driver_Address2;
                        u.City = s_Reservation.Driver_City;
                        u.State = s_Reservation.Driver_State;
                        u.PostCode = reservation.DriverPostCode;
                        u.DriversLicense = s_Reservation.Driver_Licence;
                        u.Email = reservation.DriverEmail;

                    }
                    else
                    {
                        u.FirstName = reservation.DriverGivenName;
                        u.LastName = reservation.DriverSurname;
                        u.DateOfBirth = reservation.DriverBirthDate == null ? new DateTime(1,1,1) : (DateTime)reservation.DriverBirthDate;
                        u.Address1 = reservation.DriverStreet;
                        u.Address2 = reservation.DriverStreet2;
                        u.City = reservation.DriverPostName;
                        u.PostCode = reservation.DriverPostCode;
                        u.DriversLicense = reservation.DriversLicence == null ? string.Empty : reservation.DriversLicence.Substring(4);
                        u.Email = reservation.DriverEmail;
                    }
                    ViewBag.user = u;
                }

                TimeSpan ts = reservation.ReturnDateTime - reservation.PickupDateTime;
                ViewBag.Days = ts.Days;
                if (ts.Hours > 0) ViewBag.Days += 1;

                ViewBag.Total = reservation.Amount;
                foreach (var ex in reservation.Extras)
                {
                    ViewBag.Total += ex.Total;
                }

                var Pickup = FindLocation(reservation.PickupLocation);
                var Return = FindLocation(reservation.ReturnLocation);

                ViewBag.Pickup = GetOpeningHoursForLocation(Pickup);
                ViewBag.Return = GetOpeningHoursForLocation(Return);
                try
                {
                    SEBRate[] Rates = (SEBRate[])Session["Rates"];
                    ViewBag.Inclusions = Rates.First(r => r.Code == reservation.VehicleGroupValue);
                    reservation.Charges = Rates.First(r => r.Code == reservation.VehicleGroupValue).Charges;
                }
                catch (Exception ex)
                {
                }


                Session["reservation"] = reservation;
                //Session["refundAmount"] = refundAmount;
                ViewBag.Step = "step3";

                var cnty = bm.Regions.Where(r => r.Language_ID == Culture).AsEnumerable()
                            .OrderBy(o => o.Country_Name);

                var c = cnty.OrderBy(o => o.Country_Name)
                    .Select(x => new { CountryName = x.Country_Name, CountryCode = x.Country_Code })
                    .Distinct();

                //var c = db.Countries.OrderBy(o => o.SortOrder).ThenBy(o => o.CountryText).ToList();
                var l = new List<SelectListItem>();
                foreach (var cty in c)
                {
                    var li = new SelectListItem { Text = cty.CountryName, Value = cty.CountryCode.ToString() };
                    l.Add(new SelectListItem { Text = cty.CountryName, Value = cty.CountryCode.ToString(), Selected = li.Text.StartsWith(reservation.PickupLocationCountryName) });
                }

                ViewBag.Countries = l;
                ViewData["ResidenceCountries"] = l;

                return View(reservation);
            //}
            //catch (Exception ex)
            //{
            //   logger.Error("Error", ex);
            //    throw ex;
            //}
        }

        //[HttpPost]
        //public ActionResult BookingSummary(FormCollection frm)
        //{
        //    var tempEmail = frm["DriverEmail"];
        //    if (!User.Identity.IsAuthenticated && db.Users.FirstOrDefault(u=>u.Email == tempEmail) != null)
        //    {
        //        RedirectToAction("bookingsumary", "booking", new { @ctx="duplicateUser" });
        //    }

        //    var reservation = (DCReservation)Session["reservation"];
            
        //    try{
        //        var usr = db.Users.FirstOrDefault(u => u.Email == User.Identity.Name);

        //        decimal paymentAmount = reservation.Amount;
        //        try
        //        {
        //            var s_Reservation = new Reservation();

        //            //var countryCode = reservation.DriverCountry; //int.Parse(frm["Country"]);
        //            //var countryText = db.Countries.FirstOrDefault(c => c.id == countryCode).CountryText;

        //            //var country2CharCode = bm.Regions.FirstOrDefault(r => r.Country_Name == countryText).Country_Code;

        //            reservation.DriverGivenName = frm["DriverGivenName"];
        //            reservation.DriverSurname = frm["DriverSurname"];
        //            reservation.DriverEmail = frm["DriverEmail"];
        //            reservation.DriverPhone = frm["DriverPhone"];
        //            reservation.DriverStreet = frm["DriverStreet"];
        //            reservation.DriverPostName = frm["City"];
        //            reservation.DriverPostCode = frm["DriverPostCode"];
        //            reservation.DriverCountry = frm["CountryOfResidence"]; //bm.Regions.FirstOrDefault(r => r.Country_Name == countryText).Country_Code;
        //            reservation.DriverBirthDate = DateTime.Parse(frm["DriverBirthDate"]);
        //            reservation.DriversLicense = frm["DriversLicense"];

        //            //if (reservation.ID != null && reservation.ID.Trim().Length > 0)
        //            //{
        //            //    s_Reservation = db.Reservations.FirstOrDefault(r => r.Reservation_DCID == reservation.ID);
        //            //    paymentAmount = reservation.Amount;
        //            //    s_Reservation.Reservation_Status = "UPDATE TEMP - PRE-PAYMENT";
        //            //    s_Reservation.Reservation_Amount = paymentAmount;
        //            //    s_Reservation.Reservation_DriverFirstName = reservation.DriverGivenName;
        //            //    s_Reservation.Reservation_DriverLastName = reservation.DriverSurname;
        //            //    s_Reservation.Reservation_PickupDate = reservation.PickupDateTime;
        //            //    s_Reservation.Reservation_PickupLocation = reservation.PickupLocation;
        //            //    s_Reservation.Reservation_DropoffDate = reservation.ReturnDateTime;
        //            //    s_Reservation.Reservation_DropoffLocation = reservation.ReturnLocation;
        //            //    s_Reservation.Reservation_Updated = DateTime.Now;
        //            //    s_Reservation.Reservation_UserID = User.Identity.Name;
        //            //    s_Reservation.Reservation_FormState = SerializationUtility.SaveXml(reservation);
        //            //    s_Reservation.Reservation_UserCode = frm["DriversLicense"];
        //            //    s_Reservation.Driver_Licence = frm["DriversLicense"];
        //            //    s_Reservation.Driver_Address1 = frm["DriverStreet"];
        //            //    s_Reservation.Driver_Address2 = frm["DriverStreet2"];
        //            //    s_Reservation.Driver_City = frm["City"];
        //            //    s_Reservation.Reservation_Country = frm["CountryOfResidence"];
        //            //    s_Reservation.Driver_State = frm["State"];
        //            //    s_Reservation.Driver_Email = frm["DriverEmail"];


        //            //    db.Entry(s_Reservation).State = EntityState.Modified;
        //            //    db.SaveChanges();
        //            //}
        //            //else
        //            //{
        //            //    s_Reservation.Reservation_Status = "TEMP - PREPAYMENT";
        //            //    s_Reservation.Reservation_Amount = reservation.Amount;
        //            //    s_Reservation.Reservation_Created = DateTime.Now;
        //            //    s_Reservation.Reservation_DCID = Guid.NewGuid().ToString();
        //            //    s_Reservation.Reservation_DriverFirstName = reservation.DriverGivenName;
        //            //    s_Reservation.Reservation_DriverLastName = reservation.DriverSurname;
        //            //    s_Reservation.Reservation_PickupDate = reservation.PickupDateTime;
        //            //    s_Reservation.Reservation_PickupLocation = reservation.PickupLocation;
        //            //    s_Reservation.Reservation_DropoffDate = reservation.ReturnDateTime;
        //            //    s_Reservation.Reservation_DropoffLocation = reservation.ReturnLocation;
        //            //    s_Reservation.Reservation_Updated = DateTime.Now;
        //            //    s_Reservation.Reservation_UserID = User.Identity.Name;
        //            //    s_Reservation.Reservation_FormState = SerializationUtility.SaveXml(reservation);
        //            //    s_Reservation.Reservation_UserCode = frm["DriversLicense"];
        //            //    s_Reservation.Driver_Licence = frm["DriversLicense"];
        //            //    s_Reservation.Driver_Address1 = frm["DriverStreet"];
        //            //    s_Reservation.Driver_Address2 = frm["DriverStreet2"];
        //            //    s_Reservation.Driver_City = frm["City"];
        //            //    s_Reservation.Reservation_Country = frm["CountryOfResidence"];
        //            //    s_Reservation.Driver_State = frm["State"];
        //            //    s_Reservation.Driver_Email = frm["DriverEmail"];
        //            //    s_Reservation.Reservation_ID = Guid.NewGuid();

        //            //    db.Reservations.Add(s_Reservation);
        //            //    db.SaveChanges();
        //            //    reservation.DriverRemarks = s_Reservation.Reservation_ID.ToString();
        //            //}

        //            //var rf = new MakePaymentDto();
        //            //rf.CardNumber = frm["Ecom_Payment_Card_Number"];
        //            ////if (rf.CardNumber.StartsWith("X")) rf.CardNumber = usr.CardNumber;
        //            //if (bool.Parse(ConfigurationManager.AppSettings["SetAllPaymentsTo1Kr"]))
        //            //{
        //            //    rf.Amount = 1;
        //            //}
        //            //else
        //            //{
        //            //    rf.Amount = reservation.Amount;
        //            //}

        //            //rf.Currency = reservation.CurrencyCode;
        //            ////var site = Session["culture"].ToString();
        //            ////switch (site)
        //            ////{
        //            ////    case "nb-NO":
        //            ////        rf.Currency = "NOK";
        //            ////        break;
        //            ////    case "da-DK":
        //            ////        rf.Currency = "DKK";
        //            ////        break;
        //            ////    default:
        //            ////        rf.Currency = "SEK";
        //            ////        break;
        //            ////}

        //            //rf.CVC = frm["Ecom_Payment_Card_Verification"];
        //            //rf.ExpiryDate = frm["Ecom_Payment_Card_ExpDate_Month"] + "/" + frm["Ecom_Payment_Card_ExpDate_Year"];
        //            ////if (reservation.ID != null && reservation.ID.Trim().Length > 0)
        //            ////{
        //            ////    var tempres = db.Reservations.FirstOrDefault(r => r.Reservation_DCID == reservation.ID);
        //            ////    rf.OrderId = tempres.Reservation_ID.ToString();
        //            ////}
        //            ////else
        //            ////{
        //            //    rf.OrderId = Guid.NewGuid().ToString();
        //            ////}
        //            //var gtw = new OGonePaymentGateway();
        //            //var ret = gtw.MakePayment(rf);

        //            //if(!string.IsNullOrEmpty(ret.Error))
        //            //{
        //            //    logger.Error("Payment error" + Environment.NewLine + ret.Error);
        //            //    return RedirectToAction("bookingsummary", "booking", new { @ctx = "paymenterror" });
        //            //}


        //            //db.Payments.Add(new Payment
        //            //{
        //            //    Amount = rf.Amount,
        //            //    OGonePaymentId = ret.PaymentId,
        //            //    PaymentDate = DateTime.Now,
        //            //    PaymentStatus = "SUCCESS",
        //            //    ReservationUniqueId = s_Reservation.Reservation_ID.ToString(),
        //            //    Comments = rf.Currency,
        //            //    UserReference = ret.PaymentId
        //            //});

        //            //db.SaveChanges();
        //            //reservation.DriverRemarks = s_Reservation.Reservation_ID.ToString();
        //            Session["reservation"] = reservation;

        //        }
        //        catch (Exception ex)
        //        {
        //            logger.Error("Error", ex);
        //            TempData["error"] = Resources.Copy.PaymentError;
        //            return View();

        //        }
        //    }
        //    catch(Exception payEx)
        //    {
        //        logger.Error("payment failed", payEx);
        //        TempData["error"] = Resources.Copy.PaymentError;
        //        return View();
        //    }
        //    return RedirectToAction("ConfirmBooking", "Booking");
        //}

        public IPaymentGateway PaymentGateway { get; private set; }

        //////public JsonResult Projects(string term)
        //////{

        //////    string projects = string.Empty;
        //////    foreach (
        //////        var proj in db.Projects.Where(m => m.ProjectName.Contains(term) || m.ProjectNo.Contains(term)).ToList())
        //////    {
        //////        projects += proj.ProjectNo + " - " + proj.ProjectName + "~";
        //////    }

        //////    return Json(projects.Split('~'), JsonRequestBehavior.AllowGet);

        //////}

        //public ActionResult ConfirmBooking()
        //{
        //    try{
        //    var editMode = false;
        //    var reservation = Session["reservation"] as Service.DCReservation;
        //    if (reservation == null) return RedirectToAction("BookingDetails", "Booking");
        //    reservation.DriverPostName = string.IsNullOrEmpty(reservation.DriverPostName) ? "N/A" : reservation.DriverPostName;

        //    //reservation = SetCultureAccountDetails(reservation);
        //    reservation.PaymentVoucherElectronic = true;

        //    if (reservation.CountryCode.ToUpper() == "SE")
        //    {

        //        reservation.DriverSurname = FilterString(reservation.DriverSurname, false);
        //        reservation.DriverGivenName = FilterString(reservation.DriverGivenName,
        //                                                           false);
        //        reservation.DriverPostName = FilterString(reservation.DriverPostName, false);

        //        reservation.CardHolderStreet = reservation.CardHolderStreet != null
        //                                                   ? FilterString(
        //                                                       reservation.CardHolderStreet, false)
        //                                                   : null;
        //        reservation.DriverStreet = reservation.DriverStreet != null
        //                                               ? FilterString(reservation.DriverStreet,
        //                                                              false)
        //                                               : null;
        //        reservation.DriverCountryName = reservation.DriverCountryName != null
        //                                                    ? FilterString(
        //                                                        reservation.DriverCountryName, false)
        //                                                    : null;
        //        reservation.DriverPostCode = reservation.DriverPostCode != null
        //                                                 ? FilterString(reservation.DriverPostCode,
        //                                                                false)
        //                                                 : null;
        //        reservation.CardHolderPostalCode = reservation.CardHolderPostalCode != null
        //                                                       ? FilterString(
        //                                                           reservation.CardHolderPostalCode,
        //                                                           false)
        //                                                       : null;
        //        reservation.CardHolderCountryCode = reservation.CardHolderCountryCode !=
        //                                                    null
        //                                                        ? FilterString(
        //                                                            reservation
        //                                                                   .CardHolderCountryCode, false)
        //                                                        : null;

        //        reservation.DeliveryAddress1 = reservation.DeliveryAddress1 != null
        //                                                   ? FilterString(
        //                                                       reservation.DeliveryAddress1, false)
        //                                                   : null;
        //        reservation.DeliveryAddress2 = reservation.DeliveryAddress2 != null
        //                                                   ? FilterString(
        //                                                       reservation.DeliveryAddress2, false)
        //                                                   : null;
        //        reservation.DeliveryInstructions1 = reservation.DeliveryInstructions1 !=
        //                                                    null
        //                                                        ? FilterString(
        //                                                            reservation
        //                                                                   .DeliveryInstructions1, false)
        //                                                        : null;
        //        reservation.CollectionAddress1 = reservation.CollectionAddress1 != null
        //                                                     ? FilterString(
        //                                                         reservation.CollectionAddress1,
        //                                                         false)
        //                                                     : null;
        //        reservation.CollectionAddress2 = reservation.CollectionAddress2 != null
        //                                                     ? FilterString(
        //                                                         reservation.CollectionAddress2,
        //                                                         false)
        //                                                     : null;
        //        reservation.CollectionInstructions1 = reservation.CollectionInstructions1 !=
        //                                                      null
        //                                                          ? FilterString(
        //                                                              reservation
        //                                                                     .CollectionInstructions1, false)
        //                                                          : null;

        //    }

        //    else
        //    {
        //        reservation.DriverSurname = FilterString(reservation.DriverSurname, true);
        //        reservation.DriverGivenName = FilterString(reservation.DriverGivenName, true);
        //        reservation.DriverPostName = FilterString(reservation.DriverPostName, true);

        //        reservation.CardHolderStreet = reservation.CardHolderStreet != null
        //                                                   ? FilterString(
        //                                                       reservation.CardHolderStreet, true)
        //                                                   : null;
        //        reservation.DriverStreet = reservation.DriverStreet != null
        //                                               ? FilterString(reservation.DriverStreet, true)
        //                                               : null;
        //        reservation.DriverCountryName = reservation.DriverCountryName != null
        //                                                    ? FilterString(
        //                                                        reservation.DriverCountryName, true)
        //                                                    : null;
        //        reservation.DriverPostCode = reservation.DriverPostCode != null
        //                                                 ? FilterString(reservation.DriverPostCode,
        //                                                                true)
        //                                                 : null;
        //        reservation.CardHolderPostalCode = reservation.CardHolderPostalCode != null
        //                                                       ? FilterString(
        //                                                           reservation.CardHolderPostalCode,
        //                                                           true)
        //                                                       : null;
        //        reservation.CardHolderCountryCode = reservation.CardHolderCountryCode !=
        //                                                    null
        //                                                        ? FilterString(
        //                                                            reservation
        //                                                                   .CardHolderCountryCode, true)
        //                                                        : null;

        //        reservation.DeliveryAddress1 = reservation.DeliveryAddress1 != null
        //                                                   ? FilterString(
        //                                                       reservation.DeliveryAddress1, true)
        //                                                   : null;
        //        reservation.DeliveryAddress2 = reservation.DeliveryAddress2 != null
        //                                                   ? FilterString(
        //                                                       reservation.DeliveryAddress2, true)
        //                                                   : null;
        //        reservation.DeliveryInstructions1 = reservation.DeliveryInstructions1 !=
        //                                                    null
        //                                                        ? FilterString(
        //                                                            reservation
        //                                                                   .DeliveryInstructions1, true)
        //                                                        : null;
        //        reservation.CollectionAddress1 = reservation.CollectionAddress1 != null
        //                                                     ? FilterString(
        //                                                         reservation.CollectionAddress1,
        //                                                         true)
        //                                                     : null;
        //        reservation.CollectionAddress2 = reservation.CollectionAddress2 != null
        //                                                     ? FilterString(
        //                                                         reservation.CollectionAddress2,
        //                                                         true)
        //                                                     : null;
        //        reservation.CollectionInstructions1 = reservation.CollectionInstructions1 !=
        //                                                      null
        //                                                          ? FilterString(
        //                                                              reservation
        //                                                                     .CollectionInstructions1, true)
        //                                                          : null;

        //    }

        //    var response = new ReservationCreateResponse();

        //    if (!string.IsNullOrEmpty(reservation.ID))
        //    {
        //        var modifyResponse = new ReservationModifyResponse();
        //        reservation.PaymentMOP = "CH";
        //        reservation.DriverRemarks = "PP amount: " + reservation.Amount;
        //        reservation.PaymentVoucherElectronic = true;
        //        reservation.WizardNumber = null;

        //        using (var client = new ServiceClient())
        //        {
        //            if (ConfigurationManager.AppSettings["mockwebservice"] != "true")
        //            {
        //                var request = new ReservationModifyRequest()
        //                                  {
        //                                      Reservation = reservation
        //                                  };
        //                modifyResponse = client.ReservationModify(request);
        //                client.Close();
        //            }
        //            else
        //            {
        //                modifyResponse.Reservation = reservation;
        //                modifyResponse.Reservation.Errors = new DCEntity.ErrorItem[0];
        //            }

        //            if (modifyResponse.Error == null && !modifyResponse.Reservation.Errors.Any())
        //            {

        //                var originalReservationId =
        //                        db.Reservations.FirstOrDefault(r => r.Reservation_DCID == reservation.ID)
        //                          .Reservation_ID.ToString();
        //                    var originalPayment =
        //                        db.Payments.FirstOrDefault(o => o.ReservationUniqueId == originalReservationId);
        //                    var rf = new RefundPaymentDto();
        //                    rf.OrderId = originalPayment.UserReference;
        //                    rf.Amount = Math.Abs(originalPayment.Amount);
        //                    var gtw = new OGonePaymentGateway();
        //                    var ret = gtw.RefundPayment(rf);

        //                    var refund = new Payment();
        //                    refund.Amount = rf.Amount;
        //                    refund.OGonePaymentId = originalReservationId;
        //                    refund.PaymentDate = DateTime.Now;
        //                    refund.PaymentStatus = "RESERVATION MODIFIED - REFUND OF ORIGINAL PAYMENT";
        //                    refund.ReservationUniqueId = originalReservationId;
        //                    refund.UserReference = ret.PaymentId;
        //                    db.Payments.Add(refund);
        //                    db.SaveChanges();
        //                //}
        //            }
        //            else
        //            {
        //                if (modifyResponse.Error != null) logger.Info("Modify reservation issue: " + modifyResponse.Error.Message);
        //                if (modifyResponse.Reservation != null && modifyResponse.Reservation.Errors.Any()) logger.Info("Modify reservation issue: " + modifyResponse.Reservation.Errors[0].Value);
        //                logger.Info("Failed to modify the reservation - refunding payment");

        //                var originalReservationId =
        //                       db.Reservations.FirstOrDefault(r => r.Reservation_DCID == reservation.ID)
        //                         .Reservation_ID.ToString();
        //                var originalPayment =
        //                    db.Payments.OrderByDescending(o=>o.PaymentDate).FirstOrDefault(o => o.ReservationUniqueId == originalReservationId);
        //                var rf = new RefundPaymentDto();
        //                rf.OrderId = originalPayment.UserReference;
        //                rf.Amount = Math.Abs(originalPayment.Amount);
        //                var gtw = new OGonePaymentGateway();
        //                var ret = gtw.RefundPayment(rf);

        //                var refund = new Payment();
        //                refund.Amount = rf.Amount;
        //                refund.OGonePaymentId = originalReservationId;
        //                refund.PaymentDate = DateTime.Now;
        //                refund.PaymentStatus = "RESERVATION MODIFICATION FAILED - REFUND OF MODIFICATION PAYMENT";
        //                refund.ReservationUniqueId = originalReservationId;
        //                refund.UserReference = ret.PaymentId;
        //                db.Payments.Add(refund);
        //                db.SaveChanges();

        //                logger.Error("Error modifying the reservation - modification payment refunded");
        //                return RedirectToAction("bookingsummary", "booking", new { @ctx = "bookingerror"});
        //            }
        //        }

        //        reservation.AWDNumber = Session["AWDNumber"].ToString();
        //        editMode = true;
        //    }

        //    else
        //    {

        //    try
        //    {
        //        using (var client = new ServiceClient())
        //        {
        //            var request = new ReservationCreateRequest();
        //            request.Reservation = reservation;

        //            if (ConfigurationManager.AppSettings["MockWebService"] == "true")
        //            {
        //                response = RebuildReservationResponse();
        //            }
        //            else
        //            {
        //                reservation = SetCultureAccountDetails(reservation);
        //                request.Reservation.PaymentCostControl = null;
        //                request.Reservation.PaymentVoucherElectronic = true;
        //                request.Reservation.PaymentMOP = "CH";
        //                request.Reservation.PaymentCID = request.Reservation.PaymentCID + (db.VoucherCodes.First().LastCodeUsed + 1).ToString();
                        
        //                logger.Info("Creating Reservation");

        //                response = client.ReservationCreate(request);
        //            }

        //            if (response.Error != null || (response.Reservation != null && response.Reservation.Errors.Any()))
        //            {

        //                if (response.Error != null) logger.Info("Create reservation issue: " + response.Error.Message);
        //                if (response.Reservation != null && response.Reservation.Errors.Any()) logger.Info("Create reservation issue: " + response.Reservation.Errors[0].Value);
        //                logger.Info("Failed to create the reservation - refunding payment");

        //                var originalPayment =
        //                    db.Payments.FirstOrDefault(o => o.ReservationUniqueId == reservation.DriverRemarks);
        //                var rf = new RefundPaymentDto();
        //                rf.OrderId = originalPayment.UserReference;
        //                rf.Amount = Math.Abs(originalPayment.Amount);
        //                var gtw = new OGonePaymentGateway();
        //                var ret = gtw.RefundPayment(rf);

        //                var refund = new Payment();
        //                refund.Amount = rf.Amount;
        //                refund.OGonePaymentId = originalPayment.ReservationUniqueId;
        //                refund.PaymentDate = DateTime.Now;
        //                refund.PaymentStatus = "RESERVATION CREATE FAILED - REFUND OF PAYMENT";
        //                refund.ReservationUniqueId = originalPayment.ReservationUniqueId;
        //                refund.UserReference = ret.PaymentId;
        //                db.Payments.Add(refund);
        //                db.SaveChanges();

        //                return RedirectToAction("BookingSummary", "Booking", new { ctx = "bookingError" });
        //            }



        //            if (response.Reservation.Errors.Count() > 0)
        //            {
        //                ViewBag.Message = "<script>alert('" + response.Reservation.Errors[0].Text + " - " +
        //                                  response.Reservation.Errors[0].Value + "');</script>";
        //            }

        //            if (System.Configuration.ConfigurationManager.AppSettings["log"] == "true")
        //                Log("create_reservation_response", "create_reservation_response", response);


        //            if (string.IsNullOrEmpty(response.Reservation.ID))
        //            {
        //                ViewBag.Message =
        //                    "<script>alert('" + Resources.Copy.BookingError + "');</script>";
        //            }

        //            logger.Info("Reservation created. ID number: " + response.Reservation.ID);
        //            reservation.ID = response.Reservation.ID;


        //            if (ConfigurationManager.AppSettings["mockwebservice"] == "true")
        //            {
        //                reservation.ID = Guid.NewGuid().ToString().Substring(0, 11);
        //            }

        //            client.Close();


        //        }
        //    }
        //    catch (Exception e)
        //    {
        //       logger.Error("ERROR", e);
        //    }
        //}
        //    logger.Info("Adding reservation to database");

        //    var s_Reservation = new Reservation();
        //    var resid = new Guid();

        //    if (editMode)
        //    {
        //        s_Reservation = db.Reservations.FirstOrDefault(r => r.Reservation_DCID == reservation.ID);
        //        s_Reservation.Reservation_Status = "UPDATED";
        //    }
        //    else
        //    {
        //        resid = Guid.Parse(reservation.DriverRemarks);
        //        s_Reservation = db.Reservations.FirstOrDefault(r => r.Reservation_ID == resid);
        //        s_Reservation.Reservation_Status = "WIZARD CONFIRMED";

        //        var vc = db.VoucherCodes.First();
        //        var newVoucherCode = vc.LastCodeUsed + 1;
        //        vc.LastCodeUsed = newVoucherCode;
        //        db.Entry(vc).State = EntityState.Modified;

        //    }

        //    s_Reservation.Reservation_DCID = reservation.ID;
        //    s_Reservation.Reservation_DriverFirstName = reservation.DriverGivenName;
        //    s_Reservation.Reservation_DriverLastName = reservation.DriverSurname;
        //    s_Reservation.Reservation_PickupDate = reservation.PickupDateTime;
        //    s_Reservation.Reservation_PickupLocation = reservation.PickupLocation;
        //    s_Reservation.Reservation_DropoffDate = reservation.ReturnDateTime;
        //    s_Reservation.Reservation_DropoffLocation = reservation.ReturnLocation;
        //    s_Reservation.Reservation_Updated = DateTime.Now;
        //    s_Reservation.Reservation_UserID = User.Identity.Name;
        //    s_Reservation.Reservation_FormState = SerializationUtility.SaveXml(reservation);
        //    s_Reservation.Reservation_Amount = reservation.Amount;
           

        //    db.Entry(s_Reservation).State = EntityState.Modified;
        //    db.SaveChanges();
        //    logger.Info("Modifying new reservation with PP amount");
        //    using (var client = new ServiceClient())
        //    {
        //        if (ConfigurationManager.AppSettings["mockwebservice"] != "true")
        //        {
        //            var tmpCID = reservation.PaymentCID;
        //            reservation = SetCultureAccountDetails(reservation);
        //            reservation.PaymentCID = tmpCID;
        //            reservation.PaymentMOP = "CH";
        //            reservation.PaymentVoucherElectronic = true;
                    
        //            reservation.WizardNumber = null;

        //            reservation.DriverRemarks = "PP amount: " + reservation.Amount;
        //            var request = new ReservationModifyRequest()
        //                              {
        //                                  Reservation = reservation
        //                              };
        //            var modifyResponse = client.ReservationModify(request);
        //            client.Close();
        //        }
        //    }

        //    logger.Info("Reservation added to DB");

        //    logger.Info("Processing extras to DB");

        //    foreach (var extra in reservation.Extras)
        //    {
        //        var r = new ReservationExtra()
        //        {
        //            ReservationId = Guid.Parse(response.Reservation.ID),
        //            ExtraId = extra.Code,
        //            Quantity = int.Parse(extra.Quantity.ToString())
        //        };
        //        db.ReservationExtras.Add(r);
        //        db.SaveChanges();
        //    }
        //    logger.Info("Extras processed to DB");
        //    logger.Info("Processing times, totals, and locations for viewbag");


        //    TimeSpan ts = reservation.ReturnDateTime - reservation.PickupDateTime;
        //    ViewBag.Days = ts.Days;
        //    if (ts.Hours > 0) ViewBag.Days += 1;

        //    ViewBag.Total = reservation.Amount;
        //    foreach (var ex in reservation.Extras)
        //    {
        //        ViewBag.Total += ex.Total;
        //    }

        //    var Pickup = FindLocation(reservation.PickupLocation);// bm.Locations.Where(p => p.Location_Code == reservation.PickupLocation).First();
        //    var Return = FindLocation(reservation.ReturnLocation); //bm.Locations.Where(p => p.Location_Code == reservation.ReturnLocation).First();

        //    ViewBag.Pickup = GetOpeningHoursForLocation(Pickup);
        //    ViewBag.Return = GetOpeningHoursForLocation(Return);
        //    ViewBag.Rev_Req = reservation;

        //    Session["reservation"] = reservation;

        //    if (!string.IsNullOrEmpty(reservation.ID))
        //    {
        //        var html = RenderRazorViewToString("_confirmationEmail", reservation);
        //        logger.Info("Sending confirmation email");

        //        CommonHelper.SendMail(Resources.Copy.YourBookingConfirmation, html, reservation.DriverEmail, "", "");

        //        //var smsBody = "Avis res nr.: " + response.Reservation.ID + System.Environment.NewLine;
        //        //smsBody += "Pickup: " + response.Reservation.PickupLocationName + ", " +
        //        //           response.Reservation.PickupDateTime.ToLongDateString() + " " +
        //        //           response.Reservation.PickupDateTime.ToShortTimeString() + "." + System.Environment.NewLine;
        //        //smsBody += "Phone: " + response.Reservation.PickupLocationPhone + "." + System.Environment.NewLine;
        //        //smsBody += "Vehicle: " + reservation.VehicleMakeModelName + "." + System.Environment.NewLine;
        //        //smsBody += "Thank you for choosing AVIS";
        //        //webservices.SmsDto sms = new webservices.SmsDto();
        //        //sms.Body = smsBody;
        //        //sms.SenderName = "AVIS";
        //        //sms.CountryDialingCode = 44;
        //        //sms.PhoneNumber = 07702929891;

        //        //webservices.SendWebservice smsClient = new webservices.SendWebservice();
        //        //logger.Info("Sending SMS to:" + sms.CountryDialingCode + sms.PhoneNumber);

        //        //smsClient.Sms(sms);

        //        //logger.Info("SMS response:" + smsResponse.Delivered);

        //    }

        //    if (editMode)
        //    {
        //        //if (paymentAmount > 0)
        //        //{
        //        //    ViewBag.Message = "<script>alert('Your booking been updated and " + Math.Abs(paymentAmount) + " extra has been charged to your card');</script>";
        //        //}
        //        //else if (paymentAmount < 0)
        //        //{
        //        //    ViewBag.Message = "<script>alert('Your booking been updated and " + Math.Abs(paymentAmount) + " has been refunded to your card');</script>";
        //        //}
        //        //else
        //        //{
        //            ViewBag.Message = "<script>alert('" + Resources.Copy.BookingUpdated + "');</script>";
        //        //}
        //    }

        //    try
        //    {
        //        DCRate[] Rates = (DCRate[])Session["Rates"];
        //        ViewBag.Inclusions = Rates.First(r => r.Code == reservation.VehicleGroupValue);
        //    }
        //    catch(Exception ex)
        //    {
        //    }
        //    logger.Info("Reservation completed");

        //    Session["reservation"] = null;
        //    Session["Project_Full"] = null;
        //    Session["ProjectNumber"] = null;
        //    Session["Company_Name"] = null;
        //    Session["IsDelivery"] = null;
        //    Session["IsCollection"] = null;

        //    return View(reservation);

        //    }
        //    catch (Exception ex1)
        //    {
        //        logger.Error("Error", ex1);
        //        throw ex1;
        //    }

        //}

        



        [HttpPost]
        public ActionResult ConfirmBooking(FormCollection frm)
        {
            if (Session == null || Session["chosenLanguage"] == null)
            {
                return RedirectToAction("bookingdetails", "booking");
            }

            var mp = new helpers.Maps();
            var car = (car)Session["car"];

            var editMode = false;
            var reservation = Session["reservation"] as Service.DCReservation;
            if (reservation == null) return RedirectToAction("BookingDetails", "Booking");

            if (frm["chkNoFlight"] == "on")
            {
                reservation.ArrivalFlightNumber = "WI";
                reservation.ArrivalFlightAirline = null;
                reservation.ArrivalFlightCode = "24";
            }
            else
            {
                var codePart = string.Empty;
                var letters = "abcdefghijklmnopqrstuvwxyz";
                for (var p = 0; p < frm["FlightNumber"].Trim().Length; p++)
                {
                    if (letters.Contains(frm["FlightNumber"].Trim().ToLower().Substring(p, 1)))
                    {
                        codePart = codePart + frm["FlightNumber"].Trim().ToLower().Substring(p, 1);
                    }
                    else
                    {
                        break;
                    }
                }

                var airline = db.Airlines.FirstOrDefault(a => a.AirlineCode == codePart);
                if (airline != null)
                {
                    reservation.ArrivalFlightNumber = frm["FlightNumber"];
                    reservation.ArrivalFlightAirline = codePart.ToUpper();
                    reservation.ArrivalFlightCode = "14";
                }
                else
                {
                    reservation.ArrivalFlightNumber = frm["FlightNumber"];
                    reservation.ArrivalFlightAirline = codePart.ToUpper();
                    reservation.ArrivalFlightCode = "26";
                }
            }




            var doCreditCardDTO = false;

            reservation.DriverGivenName = frm["DriverGivenName"];
            reservation.DriverSurname = frm["DriverSurname"];
            reservation.DriverEmail = frm["DriverEmail"];
            reservation.DriverPhone = frm["DriverPhone"];
            reservation.DriverStreet = frm["DriverStreet"];
            reservation.DriverPostName = frm["City"];
            reservation.DriverPostCode = frm["DriverPostCode"];

            var rateAmount = reservation.Amount;

            var voucherNumber = frm["VoucherNumber"];
            decimal vv = 0M;

            if (frm["VoucherValue"] != null)
            {
                vv = string.IsNullOrEmpty(frm["VoucherValue"].ToString()) ? 0M : Decimal.Round(Convert.ToDecimal(frm["VoucherValue"].ToString().Replace(".", ",")), 2);
            }

            var voucherValue = vv; // Decimal.Round(Decimal.Parse(frm["VoucherValue"].ToString()), 2);

            //var countryCode = int.Parse(frm["CountryOfResidence"]); //int.Parse(frm["Country"]);
            //var countryText = db.Countries.FirstOrDefault(c => c.id == countryCode).CountryText;
            //var country2CharCode = bm.Regions.FirstOrDefault(r => r.Country_Name == countryText && r.Language_ID == Culture).Country_Code;

            var country2CharCode = frm["CountryOfResidence"].ToString();
            reservation.DriverCountry = country2CharCode; //bm.Regions.FirstOrDefault(r => r.Country_Name == countryText).Country_Code;
            var dateOfBirth = new DateTime(int.Parse(frm["DriverBirthDate"].ToString().Substring(6, 4)), int.Parse(frm["DriverBirthDate"].ToString().Substring(3, 2)), int.Parse(frm["DriverBirthDate"].ToString().Substring(0, 2)));
            reservation.DriverBirthDate = dateOfBirth; //DateTime.Parse(frm["DriverBirthDate"]);
            reservation.DriversLicence = country2CharCode.ToUpper() + "XX" + frm["DriversLicense"];

            //Extras


            if (!string.IsNullOrEmpty(reservation.DeliveryAddress1) || !string.IsNullOrEmpty(reservation.CollectionAddress1))
            {
                reservation.ArrivalFlightCode = "14";
                reservation.ArrivalFlightNumber = "DL";
                reservation.ArrivalFlightAirline = null;
            }
            else
            {
                reservation.ArrivalFlightCode = null;
                reservation.ArrivalFlightNumber = null;
                reservation.ArrivalFlightAirline = null;
            }

            if (reservation.CountryCode.ToUpper() == "SE")
            {

                reservation.DriverSurname = FilterString(reservation.DriverSurname, false);
                reservation.DriverGivenName = FilterString(reservation.DriverGivenName, false);
                reservation.DriverPostName = FilterString(reservation.DriverPostName, false);

                reservation.CardHolderStreet = reservation.CardHolderStreet != null ? FilterString(reservation.CardHolderStreet, false) : null;
                reservation.DriverStreet = reservation.DriverStreet != null ? FilterString(reservation.DriverStreet, false) : null;
                reservation.DriverCountryName = reservation.DriverCountryName != null ? FilterString(reservation.DriverCountryName, false) : null;
                reservation.DriverPostCode = reservation.DriverPostCode != null ? FilterString(reservation.DriverPostCode, false) : null;
                reservation.CardHolderPostalCode = reservation.CardHolderPostalCode != null ? FilterString(reservation.CardHolderPostalCode, false) : null;
                reservation.CardHolderCountryCode = reservation.CardHolderCountryCode != null ? FilterString(reservation.CardHolderCountryCode, false) : null;

                reservation.DeliveryAddress1 = reservation.DeliveryAddress1 != null ? FilterString(reservation.DeliveryAddress1, false) : null;
                reservation.DeliveryAddress2 = reservation.DeliveryAddress2 != null ? FilterString(reservation.DeliveryAddress2, false) : null;
                reservation.DeliveryInstructions1 = reservation.DeliveryInstructions1 != null ? FilterString(reservation.DeliveryInstructions1, false) : null;
                reservation.CollectionAddress1 = reservation.CollectionAddress1 != null ? FilterString(reservation.CollectionAddress1, false) : null;
                reservation.CollectionAddress2 = reservation.CollectionAddress2 != null ? FilterString(reservation.CollectionAddress2, false) : null;
                reservation.CollectionInstructions1 = reservation.CollectionInstructions1 != null ? FilterString(reservation.CollectionInstructions1, false) : null;

                reservation.DriverRemarks = reservation.DriverRemarks != null ? FilterString(reservation.DriverRemarks, false) : null;


            }

            else
            {
                reservation.DriverSurname = FilterString(reservation.DriverSurname, true);
                reservation.DriverGivenName = FilterString(reservation.DriverGivenName, true);
                reservation.DriverPostName = FilterString(reservation.DriverPostName, true);

                reservation.CardHolderStreet = reservation.CardHolderStreet != null ? FilterString(reservation.CardHolderStreet, true) : null;
                reservation.DriverStreet = reservation.DriverStreet != null ? FilterString(reservation.DriverStreet, true) : null;
                reservation.DriverCountryName = reservation.DriverCountryName != null ? FilterString(reservation.DriverCountryName, true) : null;
                reservation.DriverPostCode = reservation.DriverPostCode != null ? FilterString(reservation.DriverPostCode, true) : null;
                reservation.CardHolderPostalCode = reservation.CardHolderPostalCode != null ? FilterString(reservation.CardHolderPostalCode, true) : null;
                reservation.CardHolderCountryCode = reservation.CardHolderCountryCode != null ? FilterString(reservation.CardHolderCountryCode, true) : null;

                reservation.DeliveryAddress1 = reservation.DeliveryAddress1 != null ? FilterString(reservation.DeliveryAddress1, true) : null;
                reservation.DeliveryAddress2 = reservation.DeliveryAddress2 != null ? FilterString(reservation.DeliveryAddress2, true) : null;
                reservation.DeliveryInstructions1 = reservation.DeliveryInstructions1 != null ? FilterString(reservation.DeliveryInstructions1, true) : null;
                reservation.CollectionAddress1 = reservation.CollectionAddress1 != null ? FilterString(reservation.CollectionAddress1, true) : null;
                reservation.CollectionAddress2 = reservation.CollectionAddress2 != null ? FilterString(reservation.CollectionAddress2, true) : null;
                reservation.CollectionInstructions1 = reservation.CollectionInstructions1 != null ? FilterString(reservation.CollectionInstructions1, true) : null;

                reservation.DriverRemarks = reservation.DriverRemarks != null ? FilterString(reservation.DriverRemarks, true) : null;


            }


            // remove all other special characters
            reservation.DriverGivenName = CommonHelper.TranslateSpecialCharacters(reservation.DriverGivenName);
            reservation.DriverSurname = CommonHelper.TranslateSpecialCharacters(reservation.DriverSurname);
            reservation.DriverPostName = CommonHelper.TranslateSpecialCharacters(reservation.DriverPostName);
            reservation.DriverStreet = CommonHelper.TranslateSpecialCharacters(reservation.DriverStreet);
            reservation.DriverStreet2 = CommonHelper.TranslateSpecialCharacters(reservation.DriverStreet2);
            reservation.DriverCountryName = CommonHelper.TranslateSpecialCharacters(reservation.DriverCountryName);
            reservation.DriverPostCode = CommonHelper.TranslateSpecialCharacters(reservation.DriverPostCode);
            reservation.CardHolderPostalCode = CommonHelper.TranslateSpecialCharacters(reservation.CardHolderPostalCode);
            reservation.CardHolderCountryCode = CommonHelper.TranslateSpecialCharacters(reservation.CardHolderCountryCode);
            reservation.DeliveryAddress1 = CommonHelper.TranslateSpecialCharacters(reservation.DeliveryAddress1);
            reservation.DeliveryAddress2 = CommonHelper.TranslateSpecialCharacters(reservation.DeliveryAddress2);
            reservation.DeliveryInstructions1 = CommonHelper.TranslateSpecialCharacters(reservation.DeliveryInstructions1);
            reservation.CollectionAddress1 = CommonHelper.TranslateSpecialCharacters(reservation.CollectionAddress1);
            reservation.CollectionAddress2 = CommonHelper.TranslateSpecialCharacters(reservation.CollectionAddress2);
            reservation.CollectionInstructions1 = CommonHelper.TranslateSpecialCharacters(reservation.CollectionInstructions1);
            reservation.DriverRemarks = CommonHelper.TranslateSpecialCharacters(reservation.DriverRemarks);

            reservation.DriversLicence = country2CharCode.ToUpper() + "XX" + frm["DriversLicense"].ToString();
            reservation.DriverBirthDate = dateOfBirth; //DateTime.Parse(frm["DriverBirthDate"].ToString());
            reservation.DriverPhone = frm["DriverPhone"].ToString();

            if (string.IsNullOrEmpty(voucherNumber))
            {
                //Credit Card

                //if (frm["cboCardType"].ToString() != "DN")
                //{

                //TODO: Uncomment for live!!!!

                    reservation.PaymentCID = frm["cboCardType"] + frm["Ecom_Payment_Card_Number"].ToString();
                    reservation.PaymentCEX = frm["Ecom_Payment_Card_ExpDate_Month"] + frm["Ecom_Payment_Card_ExpDate_Year"];
                    reservation.PaymentMOP = "CV";
                //}
            }
            else
            {

                //Voucher
                reservation.PaymentCID = "AV894097480004TE" + voucherNumber;
                reservation.PaymentMOP = "CH";
                reservation.IATANumber = "0107203N";
                reservation.PaymentVoucherElectronic = true;
                reservation.PaymentVoucherCode = "FixedValue";
                reservation.Amount = Decimal.Round(voucherValue, 2);
                doCreditCardDTO = true;
            }


            var response = new ReservationCreateResponse();
            var pickup_budgetlocationmap = db.BudgetLocationMapping.FirstOrDefault(b => b.AvisLocation == reservation.PickupLocation);
            var return_budgetlocationmap = db.BudgetLocationMapping.FirstOrDefault(b => b.AvisLocation == reservation.ReturnLocation);


            if (!string.IsNullOrEmpty(reservation.ID))
            {
                var modifyResponse = new ReservationModifyResponse();
                using (var client = new ServiceClient())
                {
                    if (ConfigurationManager.AppSettings["mockwebservice"] != "true")
                    {

                        var request = new ReservationModifyRequest()
                        {
                            Reservation = reservation
                        };

                        if (car.company == "AVIS")
                        {
                            modifyResponse = client.ReservationModify(request);
                        }
                        else
                        {
                            var sc = new SEB.Maestro.ServiceClient();
                            var req = new SEB.Maestro.ReservationModifyRequest();
                            req.Reservation = CommonHelper.MapReservation(reservation);
                            req.Reservation.PickupLocation = pickup_budgetlocationmap.BudgetLocation;
                            req.Reservation.ReturnLocation = return_budgetlocationmap.BudgetLocation;

                            List<SEB.Maestro.DCExtra> mEx = new List<Maestro.DCExtra>();
                            foreach (var sEx in reservation.Extras)
                            {
                                mEx.Add(Mapper.Map<SEB.Service.DCExtra, SEB.Maestro.DCExtra>(sEx));
                                if (sEx.Code != null) req.Reservation.DriverRemarks += sEx.Code + "/";
                            }
                            req.Reservation.Extras = mEx.ToArray();

                            modifyResponse = Mapper.Map<SEB.Maestro.ReservationModifyResponse, SEB.Service.ReservationModifyResponse>(sc.ReservationModify(req));
                        }
                        
                        client.Close();
                    }
                    else
                    {
                        modifyResponse.Reservation = reservation;
                        modifyResponse.Reservation.Errors = new DCEntity.ErrorItem[0];
                    }
                }

                if (modifyResponse.Error != null || (modifyResponse.Reservation != null && modifyResponse.Reservation.Errors.Any()))
                {
                    if (modifyResponse.Error != null) { CommonHelper.SendErrorMail("Booking Error in SEB Microsite", modifyResponse.Error.Message + Environment.NewLine + modifyResponse.Error.StackTrace); logger.Info("Modify reservation issue: " + modifyResponse.Error.Message + Environment.NewLine + modifyResponse.Error.StackTrace); Session["ReservationError"] = modifyResponse.Error.Message; }
                    if (modifyResponse.Reservation != null && modifyResponse.Reservation.Errors.Any()) { CommonHelper.SendErrorMail("Booking Error in SEB Microsite", modifyResponse.Reservation.Errors[0].Text + " " + modifyResponse.Reservation.Errors[0].Value); logger.Info("Modify reservation issue: " + modifyResponse.Reservation.Errors[0].Text); Session["ReservationError"] = modifyResponse.Reservation.Errors[0].Text; }

                    return RedirectToAction("BookingSummary", "Booking", new { ctx = "bookingError" });
                }

                var s_Reservation = db.Reservations.Where(r => r.Reservation_DCID == reservation.ID).FirstOrDefault();
                s_Reservation.Reservation_Amount = rateAmount; // reservation.Amount;
                s_Reservation.Reservation_DriverFirstName = reservation.DriverGivenName;
                s_Reservation.Reservation_DriverLastName = reservation.DriverSurname;
                s_Reservation.Reservation_PickupDate = reservation.PickupDateTime;
                s_Reservation.Reservation_PickupLocation = reservation.PickupLocation;
                s_Reservation.Reservation_DropoffDate = reservation.ReturnDateTime;
                s_Reservation.Reservation_DropoffLocation = reservation.ReturnLocation;
                s_Reservation.Reservation_Updated = DateTime.Now;
                s_Reservation.Reservation_UserID = frm["DriverEmail"]; //User.Identity.Name;
                s_Reservation.Reservation_Info = reservation.PaymentCCI;
                s_Reservation.Reservation_FormState = SerializationUtility.SaveXml(reservation);
                //s_Reservation.Reservation_ID = Guid.NewGuid();
                s_Reservation.Reservation_UserCode = frm["DriverEmail"];//loggedInUser.ID.ToString();
                //s_Reservation.Reservation_Company = frm["Subunit"];

                s_Reservation.Reservation_Status = "UPDATE";
                s_Reservation.Reservation_Driver = frm["DriversLicense"];
                s_Reservation.RentalCompany = car.company;
                s_Reservation.VoucherNumber = voucherNumber;
                s_Reservation.VoucherValue = voucherValue;
                s_Reservation.Customer = Session["chosenCardmake"].ToString();

                //s_Reservation.DeliveryAddress1 = reservation.DeliveryAddress1;
                //s_Reservation.DeliveryAddress2 = reservation.DeliveryAddress2;
                //s_Reservation.DeliveryInstructions = reservation.DeliveryInstructions1;
                //s_Reservation.CollectionAddress1 = reservation.CollectionAddress1;
                //s_Reservation.CollectionAddress2 = reservation.CollectionAddress2;
                //s_Reservation.CollectionInstructions = reservation.CollectionInstructions1;
                //s_Reservation.Driver_Remarks = reservation.DriverRemarks;
                //s_Reservation.PickupLocationName = bm.Locations.First(l => l.Location_Code == reservation.PickupLocation && l.Location_Language_ID == this.Culture).Location_Name;
                //s_Reservation.ReturnLocationName = bm.Locations.First(l => l.Location_Code == reservation.ReturnLocation && l.Location_Language_ID == this.Culture).Location_Name;


                db.Entry(s_Reservation).State = System.Data.EntityState.Modified;
                db.SaveChanges();

                editMode = true;
            }

            else
            {
                try
                {
                    using (var client = new ServiceClient())
                    {
                        var request = new ReservationCreateRequest();
                        request.Reservation = reservation;
                        //request.Reservation.WizardNumber = db.Units.Where(u => u.LoginId == User.Identity.Name).FirstOrDefault().WizardNumber;

                        if (System.Configuration.ConfigurationManager.AppSettings["MockWebService"] == "true")
                        {
                            response = RebuildReservationResponse();
                        }
                        else
                        {
                            
                           //logger.Info("Creating Reservation");

                            if (car.company == "AVIS")
                            {
                                response = client.ReservationCreate(request);
                            }
                            else
                            {                            
                                var sc = new SEB.Maestro.ServiceClient();
                                var req = new SEB.Maestro.ReservationCreateRequest();
                                req.Reservation = CommonHelper.MapReservation(reservation);
                                var budgetAWD = db.AwdNumbers.FirstOrDefault(a => a.Number == req.Reservation.AWDNumber).BudgetNumber;
                                if (!string.IsNullOrEmpty(budgetAWD)) req.Reservation.AWDNumber = budgetAWD;
                                req.Reservation.PickupLocation = pickup_budgetlocationmap.BudgetLocation;
                                req.Reservation.ReturnLocation = return_budgetlocationmap.BudgetLocation;
                                
                                List<SEB.Maestro.DCExtra> mEx = new List<Maestro.DCExtra>();
                                foreach (var sEx in reservation.Extras)
                                {
                                    mEx.Add(Mapper.Map<SEB.Service.DCExtra, SEB.Maestro.DCExtra>(sEx));
                                    if (sEx.Code != null) req.Reservation.DriverRemarks += sEx.Code + "/";
                                }

                                req.Reservation.Extras = mEx.ToArray();
                                response = Mapper.Map<SEB.Maestro.ReservationCreateResponse, SEB.Service.ReservationCreateResponse>(sc.ReservationCreate(req));
                            }
                        }
                        
                        if (response.Error != null || (response.Reservation != null && response.Reservation.Errors.Count() > 0))
                        {
                            if (response.Error != null) { CommonHelper.SendErrorMail("Booking Error in SEB Microsite", response.Error.Message + Environment.NewLine + response.Error.StackTrace); Session["ReservationError"] = response.Error.Message; }
                            if (response.Reservation != null && response.Reservation.Errors.Any()) { CommonHelper.SendErrorMail("Booking Error in SEB Microsite", response.Reservation.Errors[0].Text + " " + response.Reservation.Errors[0].Value); Session["ReservationError"] = response.Reservation.Errors[0].Text; }

                            return RedirectToAction("BookingSummary", "Booking", new { ctx = "bookingError" });
                        }



                        //if(string.IsNullOrEmpty(response.Reservation.ID))
                        //{
                        //    ViewBag.Message = "<script>alert('Ett fel inträffade och bokningen kunde inte genomföras. Vänligen se över dina detaljer och försök igen.');</script>";
                        //}

                        //if (string.IsNullOrEmpty(response.Reservation.ID))
                        //{
                        //    //ViewBag.Message = "<script>alert('Ett fel inträffade och bokningen kunde inte genomföras. Vänligen se över dina detaljer och försök igen.');</script>";
                        //    ViewBag.OnRequestMessage = "<script>alert('This is an On-Request booking. It will be confirmed within 24 hours.');</script>This is an On-Request booking. It will be confirmed within 24 hours";
                        //}

                        //logger.Info("Reservation created. ID number: " + response.Reservation.ID);
                        reservation.ID = response.Reservation.ID;
                        //logger.Info("Adding reservation to database");
                        var s_Reservation = new Reservation();
                        try
                        {
                            s_Reservation.Reservation_Amount = rateAmount; // reservation.Amount;
                            s_Reservation.Reservation_Created = DateTime.Now;
                            s_Reservation.Reservation_DCID = response.Reservation.ID;
                            s_Reservation.Reservation_DriverFirstName = reservation.DriverGivenName;
                            s_Reservation.Reservation_DriverLastName = reservation.DriverSurname;
                            s_Reservation.Reservation_PickupDate = reservation.PickupDateTime;
                            s_Reservation.Reservation_PickupLocation = reservation.PickupLocation;
                            s_Reservation.Reservation_DropoffDate = reservation.ReturnDateTime;
                            s_Reservation.Reservation_DropoffLocation = reservation.ReturnLocation;
                            s_Reservation.Reservation_Updated = DateTime.Now;
                            s_Reservation.Reservation_UserID = frm["DriverEmail"]; //User.Identity.Name;
                            s_Reservation.Reservation_Info = reservation.PaymentCCI;
                            s_Reservation.Reservation_FormState = SerializationUtility.SaveXml(reservation); //new JavaScriptSerializer().Serialize(reservation);
                            s_Reservation.Reservation_ID = Guid.NewGuid();
                            s_Reservation.Reservation_UserCode = frm["DriverEmail"];
                            //s_Reservation.Reservation_Company = frm["Subunit"];
                            s_Reservation.Reservation_Driver = frm["DriversLicense"].ToString();
                            s_Reservation.RentalCompany = car.company;
                            s_Reservation.VoucherNumber = voucherNumber;
                            s_Reservation.VoucherValue = voucherValue;
                            s_Reservation.Customer = Session["chosenCardmake"].ToString();
                            //s_Reservation.DeliveryAddress1 = reservation.DeliveryAddress1;
                            //s_Reservation.DeliveryAddress2 = reservation.DeliveryAddress2;
                            //s_Reservation.DeliveryInstructions = reservation.DeliveryInstructions1;
                            //s_Reservation.CollectionAddress1 = reservation.CollectionAddress1;
                            //s_Reservation.CollectionAddress2 = reservation.CollectionAddress2;
                            //s_Reservation.CollectionInstructions = reservation.CollectionInstructions1;
                            //s_Reservation.Driver_Remarks = reservation.DriverRemarks;
                            //s_Reservation.PickupLocationName = bm.Locations.First(l => l.Location_Code == reservation.PickupLocation && l.Location_Language_ID == this.Culture).Location_Name;
                            //s_Reservation.ReturnLocationName = bm.Locations.First(l => l.Location_Code == reservation.ReturnLocation && l.Location_Language_ID == this.Culture).Location_Name;


                            if (editMode)
                            {
                                s_Reservation.Reservation_Status = "UPDATE";
                            }
                            else
                            {
                                s_Reservation.Reservation_Status = "NEW";
                            }

                            db.Reservations.Add(s_Reservation);
                            db.SaveChanges();
                            //logger.Info("Reservation added to DB");

                            if (doCreditCardDTO)
                            {
                                //logger.Info("Attempting credit card details update for ID number: " + response.Reservation.ID);
                                var svc = new helpers.WizardHelpers.WizardService();
                                var ct = "CX";
                                switch (frm["cboCardType"].ToString())
                                {
                                    case "MC":
                                        ct = "CM";
                                        break;
                                    case "AX":
                                        ct = "CA";
                                        break;
                                    case "DN":
                                        ct = "CD";
                                        break;
                                }

                                //if(ct != "CD")
                                //{
                                    var scs = svc.DoCreditCardUpdate(response.Reservation.ID, ct + frm["Ecom_Payment_Card_Number"], frm["Ecom_Payment_Card_ExpDate_Month"] + "/" + frm["Ecom_Payment_Card_ExpDate_Year"]);
                                    if (scs)
                                    {
                                        //logger.Info("Credit card details update successful for ID number: " + response.Reservation.ID);
                                    }
                                //}
                                //else
                                //{
                                //    logger.Info("Credit card details update failed for ID number: " + response.Reservation.ID + ". Continuing with the reservation process");

                                //    CommonHelper.SendMail("Credit Card Verification for your Avis booking",
                                //                          "<p>In order to fully verifiy your Avis reservation ( " + response.Reservation.ID +
                                //                          " ) , please follow the link below and complete our card verification process.</p>" +
                                //                          "<p><a href='" + ConfigurationManager.AppSettings["HostUrl"] + "/User/Login?act=ccv&reservationNumber=" + response.Reservation.ID + "'>" + ConfigurationManager.AppSettings["HostUrl"] + "/User/Login?act=ccv&reservationNumber=" + response.Reservation.ID + "</a>",
                                //                          frm["Email"], "",
                                //                          "");
                                //}

                            }


                        }
                        catch (Exception dbEx)
                        {
                            logger.Error("Error adding reservation to DB", dbEx);
                            throw dbEx;
                        }

                        client.Close();

                        //logger.Info("Processing extras to DB");
                        //foreach (var extra in reservation.Extras)
                        //{
                        //    var r = new ReservationExtra()
                        //    {
                        //        ReservationId = s_Reservation.Reservation_ID,// response.Reservation.ID),
                        //        ExtraId = extra.ID,
                        //        Quantity = int.Parse(extra.Quantity.ToString())
                        //    };
                        //    db.ReservationExtras.Add(r);
                        //    db.SaveChanges();

                        //}
                        //logger.Info("Extras processed to DB");
                    }
                }
                catch (Exception e)
                {
                    logger.Error("ERROR", e);
                    CommonHelper.SendErrorMail("Booking Error in SEB Microsite", e.Message + Environment.NewLine + e.StackTrace);

                    return RedirectToAction("BookingSummary", "Booking", new { ctx = "bookingError" });
                }
            }

            //logger.Info("Processing times, totals, and locations for viewbag");
            TimeSpan ts = reservation.ReturnDateTime - reservation.PickupDateTime;
            ViewBag.Days = ts.Days;
            if (ts.Hours > 0) ViewBag.Days += 1;

            ViewBag.VoucherValue = voucherValue; // sw_reservation.VoucherValue != null ? (Decimal)sw_reservation.VoucherValue : 0M;
            ViewBag.Total = rateAmount; //sw_reservation.Reservation_Amount != null ? (Decimal)sw_reservation.Reservation_Amount : 0M; // r.Amount;
            reservation.Amount = rateAmount;

            foreach (var ex in reservation.Extras)
            {
                ViewBag.Total += ex.Total;
            }

            //var culture = Session["chosenLanguage"];
            var Pickup = bm.Locations.Where(p => p.Location_Code == reservation.PickupLocation && p.Location_Language_ID == this.Culture).First();
            var Return = bm.Locations.Where(p => p.Location_Code == reservation.ReturnLocation && p.Location_Language_ID == this.Culture).First();

            ViewBag.Pickup = GetOpeningHoursForLocation(Pickup);
            ViewBag.Return = GetOpeningHoursForLocation(Return);

            ViewBag.Rev_Req = reservation;

            Session["reservation"] = reservation;

            if (!string.IsNullOrEmpty(reservation.ID))
            {
                try
                {
                    string html;

                    if (car.company == "AVIS")
                    {
                        html = RenderRazorViewToString("_confirmationEmail", reservation);
                    }
                    else
                    {
                        html = RenderRazorViewToString("_confirmationEmailBudget", reservation);
                        //ViewBag.OnRequestMessage = Resources.Copy.OnRequest;
                    }

                    //var usr = (Entities.User)Session["loggedInUser"];
                   // logger.Info("Sending confirmation email");
                    CommonHelper.SendMail(Resources.Copy.ConfirmationEmailSubject, html, frm["DriverEmail"], "", ConfigurationManager.AppSettings["ReservationConfirmBCC"]);

                    var smsBody = "Bokn.Nr:" + reservation.ID + System.Environment.NewLine;
                    smsBody += "Lev. adr:" + reservation.DeliveryAddress1 + ", " + reservation.DeliveryAddress2 + System.Environment.NewLine;
                    smsBody += "Datum: " + reservation.PickupDateTime.ToLongDateString() + System.Environment.NewLine;
                    smsBody += "Tid:" + reservation.PickupDateTime.ToShortTimeString() + System.Environment.NewLine;
                    smsBody += "Anm:" + reservation.DeliveryInstructions1;
                    webservices.SmsDto sms = new webservices.SmsDto();
                    sms.Body = smsBody;
                    sms.SenderName = "AVIS";
                    sms.CountryDialingCode = short.Parse(reservation.DriverPhone.Replace("+", "").Substring(0, 2));
                    var phone = reservation.DriverPhone.Replace(" ", "").Replace("-", "").Substring(3, reservation.DriverPhone.Length - 3);
                    sms.PhoneNumber = long.Parse(phone); //long.Parse(phone.TrimStart('0'));

                    webservices.SendWebserviceClient smsClient = new webservices.SendWebserviceClient();// .SendWebservice();
                    //logger.Info("Sending SMS to:" + sms.CountryDialingCode + sms.PhoneNumber);
                    var smsResponse = smsClient.Sms(sms);
                    //logger.Info("SMS response:" + smsResponse.Delivered);
                }
                catch (Exception ex)
                {
                    logger.Error("Error:", ex);
                }

            }

            if (editMode)
            {
                ViewBag.Message = "<script>alert('Din bokning har ändrats! Vänligen notera att bokningsnumret precis som tidigare är: " + reservation.ID + "');</script>";
            }

            try
            {
                SEBRate[] Rates = (SEBRate[])Session["Rates"];
                ViewBag.Inclusions = Rates.First(r => r.Code == reservation.VehicleGroupValue);
            }
            catch (Exception ex)
            {
            }

            Session["reservation"] = null;
            Session["Project_Full"] = null;
            Session["ProjectNumber"] = null;
            Session["Company_Name"] = null;
            Session["IsDelivery"] = null;
            Session["IsCollection"] = null;

            //logger.Info("Reservation completed");

            return View(reservation);

        }

        public string RenderRazorViewToString(string viewName, object model)
        {
            ViewData.Model = model;
            try
            {
                SEBRate[] Rates = (SEBRate[])Session["Rates"];
                ViewData["Inclusions"] = Rates.First(r => r.Code == ((DCReservation) model).VehicleGroupValue);
            }
            catch(Exception ex)
            {
            }
            using (var sw = new StringWriter())
            {
                var viewResult = ViewEngines.Engines.FindPartialView(ControllerContext, viewName);
                var viewContext = new ViewContext(ControllerContext, viewResult.View, ViewData, TempData, sw);
                viewResult.View.Render(viewContext, sw);
                viewResult.ViewEngine.ReleaseView(ControllerContext, viewResult.View);
                return sw.GetStringBuilder().ToString();
            }
        } 










        public void Log(string prefix, string suffix, object value)
        {
            //var xml = value is Exception
            //              ? ErrorUtility.GetExceptionXml((Exception) value)
            //              : SerializationUtility.SaveXml(value);
            //var path = "log\\" + prefix + "\\" + DateTime.UtcNow.ToString("yyyyMMdd_HHmmss_fff") + "_" + suffix + ".xml";

            var txt = value.ToString();

            var path = LogFileLocation + prefix + "\\" + DateTime.UtcNow.ToString("yyyyMMdd_HHmmss_fff") + "_" + suffix +
                       ".txt";

            Directory.CreateDirectory(Path.GetDirectoryName(path));
            System.IO.File.WriteAllText(path, txt, Encoding.UTF8);
        }

        private RateListResponse RebuildRateListResponse()
        {

            var resp = new RateListResponse();

            XmlSerializer serializer = new XmlSerializer(resp.GetType());
            StreamReader reader = new StreamReader(LogFileLocation + @"rate_list_response\rate_list_response.xml");
            object deserialized = serializer.Deserialize(reader.BaseStream);
            resp = (RateListResponse) deserialized;

            return resp;

        }

        private ReservationCreateResponse RebuildReservationResponse()
        {

            var resp = new ReservationCreateResponse();

            XmlSerializer serializer = new XmlSerializer(resp.GetType());
            StreamReader reader =
                new StreamReader(LogFileLocation + @"create_reservation_response\create_reservation_response.xml");
            object deserialized = serializer.Deserialize(reader.BaseStream);
            resp = (ReservationCreateResponse) deserialized;

            return resp;

        }

        private ReservationGetResponse RebuildReservationGetResponse()
        {

            var resp = new ReservationGetResponse();

            XmlSerializer serializer = new XmlSerializer(resp.GetType());
            StreamReader reader =
                new StreamReader(LogFileLocation + @"reservation_get_response\reservation_get_response.xml");
            object deserialized = serializer.Deserialize(reader.BaseStream);
            resp = (ReservationGetResponse) deserialized;

            return resp;

        }

        private ReservationCancelResponse RebuildReservationCancelResponse()
        {

            var resp = new ReservationCancelResponse();

            //XmlSerializer serializer = new XmlSerializer(resp.GetType());
            //StreamReader reader =
            //    new StreamReader(LogFileLocation + @"reservation_cancel_response\reservation_cancel_response.xml");
            //object deserialized = serializer.Deserialize(reader.BaseStream);
            //resp = (ReservationCancelResponse) deserialized;

            resp.Reservation = new DCReservation();
            resp.Reservation.Status = "Cancelled";

            return resp;

        }

        public ActionResult AutoCompleteLocation(string location, string country, string startdate, string starthour, string startmin, string direction)
        {
            var model = GetLocationstest(location, country, startdate, starthour, startmin, direction);
            return Json(model.ToArray(), JsonRequestBehavior.AllowGet);
        }

        public IList<Location> GetLocationstest(string location, string country, string startdate, string starthour, string startmin, string direction)
        {
            IList<Location> list = null;

            list =
                bm.Locations.Where(
                    x => x.Region_Code.StartsWith(country) && x.Location_Language_ID == this.Culture && (x.Location_Name.ToUpper().Contains(location.ToUpper()) || x.Location_Code.Contains(location.ToUpper()))).
                    OrderBy(x => x.Location_Name).ToList();

            List<Location> query = new List<Location>();
            foreach (Location l in list)
            {
                query.Add(GetOpeningHoursForLocation(l, startdate, starthour, startmin, direction));
            }

            return query;
        }

        public string IsLocationInCountry(string location, string country, string mnemonic)
        {
            location = location.Replace("~~", "'");
            var l =
                bm.Locations.Where(
                    x => x.Region_Code.StartsWith(country) && x.Location_Language_ID == this.Culture && (x.Location_Name.ToUpper() == location.ToUpper() || x.Location_Code.ToUpper() == mnemonic.ToUpper())).FirstOrDefault();
            if (l == null) return "false";
            return "true";
        }

        public string DateDiff(string start, string end)
        {
            var s = DateTime.Parse(start);
            var e = DateTime.Parse(end);
            TimeSpan ts = e - s;

            var d = ts.Days;
            var r = ts.TotalHours % 24;

            if (r > 0) d += 1;
            
            return d.ToString();
        }

        public double TimeDiff(DateTime start, DateTime end)
        {
            var s = start;
            var e = end;
            TimeSpan ts = e - s;
            return ts.TotalMinutes;
        }

        private Entities.Location GetOpeningHoursForLocation(Location l, string startdate = "", string starthour = "", string startmin = "", string direction = "")
        {

            try
            {
                var bmHours = bm.Hours.Where(h => h.Location_Code == l.Location_Code && h.Language_ID == Culture).ToList();
                if (bmHours.Any())
                {
                    l.MondayOpeningHours = bmHours.Where(h => h.Hour_Day == 1).FirstOrDefault() != null ? formatHours(bmHours.Where(h => h.Hour_Day == 1).First().Hour_HourFrom.ToString()) +
                                           " - " +
                                           formatHours(bmHours.Where(h => h.Hour_Day == 1).First().Hour_HourTo.ToString()) : Resources.Copy.NotAvailable;
                    l.TuesdayOpeningHours = bmHours.Where(h => h.Hour_Day == 2).FirstOrDefault() != null ? formatHours(bmHours.Where(h => h.Hour_Day == 2).First().Hour_HourFrom.ToString()) +
                                            " - " +
                                            formatHours(bmHours.Where(h => h.Hour_Day == 2).First().Hour_HourTo.ToString()) : Resources.Copy.NotAvailable;

                    l.WednesdayOpeningHours = bmHours.Where(h => h.Hour_Day == 3).FirstOrDefault() != null ?
                        formatHours(bmHours.Where(h => h.Hour_Day == 3).First().Hour_HourFrom.ToString()) + " - " +
                        formatHours(bmHours.Where(h => h.Hour_Day == 3).First().Hour_HourTo.ToString()) : Resources.Copy.NotAvailable;
                    l.ThursdayOpeningHours = bmHours.Where(h => h.Hour_Day == 4).FirstOrDefault() != null ? formatHours(bmHours.Where(h => h.Hour_Day == 4).First().Hour_HourFrom.ToString()) +
                                             " - " +
                                             formatHours(bmHours.Where(h => h.Hour_Day == 4).First().Hour_HourTo.ToString()) : Resources.Copy.NotAvailable;
                    l.FridayOpeningHours = bmHours.Where(h => h.Hour_Day == 5).FirstOrDefault() != null ? formatHours(bmHours.Where(h => h.Hour_Day == 5).First().Hour_HourFrom.ToString()) +
                                           " - " +
                                           formatHours(bmHours.Where(h => h.Hour_Day == 5).First().Hour_HourTo.ToString()) : Resources.Copy.NotAvailable;

                    if (bmHours.Where(h => h.Hour_Day == 6).FirstOrDefault() != null)
                    {
                        l.SaturdayOpeningHours = bmHours.Where(h => h.Hour_Day == 6).FirstOrDefault() != null ?
                            formatHours(bmHours.Where(h => h.Hour_Day == 6).First().Hour_HourFrom.ToString()) + " - " +
                            formatHours(bmHours.Where(h => h.Hour_Day == 6).First().Hour_HourTo.ToString()) : Resources.Copy.NotAvailable;
                    }
                    else
                    {
                        l.SaturdayOpeningHours = Resources.Copy.Closed;
                    }
                    if (bmHours.Where(h => h.Hour_Day == 0).FirstOrDefault() != null)
                    {
                        try
                        {
                            l.SundayOpeningHours = bmHours.Where(h => h.Hour_Day == 0).FirstOrDefault() != null ?
                                formatHours(bmHours.Where(h => h.Hour_Day == 0).First().Hour_HourFrom.ToString()) + " - " +
                                formatHours(bmHours.Where(h => h.Hour_Day == 0).First().Hour_HourTo.ToString()) : Resources.Copy.NotAvailable;
                        }
                        catch (Exception ex)
                        {
                            l.SundayOpeningHours = Resources.Copy.NotAvailable;
                        }
                    }
                    else
                    {
                        l.SundayOpeningHours = Resources.Copy.Closed;
                    }

                }
                else
                {
                    l.MondayOpeningHours = Resources.Copy.NotAvailable;
                    l.TuesdayOpeningHours = Resources.Copy.NotAvailable;
                    l.WednesdayOpeningHours = Resources.Copy.NotAvailable;
                    l.ThursdayOpeningHours = Resources.Copy.NotAvailable;
                    l.FridayOpeningHours = Resources.Copy.NotAvailable;
                    l.SaturdayOpeningHours = Resources.Copy.NotAvailable;
                    l.SundayOpeningHours = Resources.Copy.NotAvailable;
                }
            }
            catch (Exception ex)
            {
                l.MondayOpeningHours = Resources.Copy.NotAvailable;
                l.TuesdayOpeningHours = Resources.Copy.NotAvailable;
                l.WednesdayOpeningHours = Resources.Copy.NotAvailable;
                l.ThursdayOpeningHours = Resources.Copy.NotAvailable;
                l.FridayOpeningHours = Resources.Copy.NotAvailable;
                l.SaturdayOpeningHours = Resources.Copy.NotAvailable;
                l.SundayOpeningHours = Resources.Copy.NotAvailable;
            }

            if (direction != string.Empty)
            {
                var d = DateTime.Parse(startdate + " " + starthour + ":" + startmin);
                if (IsLocationOpenNow(l.Location_Code, d) == "OK" || (direction == "in" && l.Location_AfterHoursReturn))
                {
                    l.IsOpenNow = Resources.Copy.Open;
                    l.StationOpenOrClosed = Resources.Copy.Open;
                }
                else
                {
                    l.IsOpenNow = Resources.Copy.Closed;
                    l.StationOpenOrClosed = Resources.Copy.Closed;
                }
            }
            return l;
        }

        private string formatHours(string hours)
        {
            try
            {
                if (hours.Length == 1) return "00:0" + hours;
                if (hours.Length == 2) return "00:" + hours;
                if (hours.Length == 3) hours = "0" + hours;
                hours = hours.Substring(0, 2) + ":" + hours.Substring(2, 2);
                return hours;
            }
            catch (Exception ex)
            {
                return Resources.Copy.NotAvailable;
            }
        }

        private DCReservation RebuildExtras(DCReservation reservation, Guid uniqueId)
        {
            try
            {
                TimeSpan ts = reservation.ReturnDateTime - reservation.PickupDateTime;

                int days = ts.Days;
                if (ts.Hours > 0) days += 1;
                List<Entities.Equipment> extras = new List<Equipment>();
                List<ReservationExtra> storedExtras =
                    db.ReservationExtras.Where(x => x.ReservationId == uniqueId).ToList();
                foreach (var storedExtra in storedExtras)
                {
                    var s = Guid.Parse(storedExtra.ExtraId);
                    extras.Add(bm.Equipments.Where(e => e.Equipment_ID == s).First());
                }

                var dcExtras = new DCExtra[] { };
                int ctr = -1;
                foreach (var extra in extras)
                {

                    Service.DCExtra e = null;
                    if (extra.Equipment_Description.Contains(" Seat") ||
                        extra.Equipment_Description == "Additional Driver")
                    {

                        var equip =
                            bm.EquipmentCodes.Where(eq => eq.Equipment_Description == extra.Equipment_Description).
                                FirstOrDefault();
                        string sEquip = string.Empty;
                        if (equip != null) sEquip = equip.Equipment_Number.ToString();
                        e = new DCExtra();
                        e.Code = extra.Equipment_ID.ToString();
                        e.Quantity = storedExtras.Where(x => x.ExtraId == extra.Equipment_ID.ToString()).First().Quantity;
                        e.Number = sEquip;
                        e.Type = "";
                        e.Name = extra.Equipment_Description;
                        e.Text = extra.Equipment_AdditionalInfo;
                        e.UnitName = "";
                        e.Availability = extra.Equipment_Availability;
                        e.IncludedInRate = false;
                        e.Amount = extra.Equipment_Price;

                        if (extra.Equipment_ChargePer == "Day")
                        {
                            e.Total = extra.Equipment_Price * e.Quantity * days;

                        }
                        else
                        {
                            e.Total = extra.Equipment_Price * e.Quantity;
                        }

                        if (e.Total > (extra.Equipment_PriceMax * e.Quantity))
                        {
                            e.Total = extra.Equipment_PriceMax * e.Quantity;
                        }
                        ctr += 1;


                    }
                    else
                    {

                        var equip =
                            bm.EquipmentCodes.Where(eq => eq.Equipment_Description == extra.Equipment_Description).
                                FirstOrDefault();
                        string sEquip = string.Empty;
                        if (equip != null) sEquip = equip.Equipment_Number.ToString();
                        e = new DCExtra();
                        e.Code = extra.Equipment_ID.ToString();
                        e.Quantity = 1;
                        e.Number = sEquip;
                        e.Type = "";
                        e.Name = extra.Equipment_Description;
                        e.Text = extra.Equipment_AdditionalInfo;
                        e.UnitName = "";
                        e.Availability = extra.Equipment_Availability;
                        e.IncludedInRate = false;
                        e.Amount = extra.Equipment_Price;
                        if (extra.Equipment_ChargePer == "Day")
                        {
                            e.Total = extra.Equipment_Price * days;
                        }
                        else
                        {
                            e.Total = extra.Equipment_Price;
                        }


                        if (e.Total > extra.Equipment_PriceMax)
                        {
                            e.Total = extra.Equipment_PriceMax;
                        }
                        ctr += 1;

                    }

                    if (e != null)
                    {
                        Array.Resize(ref dcExtras, ctr + 1);
                        dcExtras[ctr] = e;
                    }


                }
                reservation.Extras = dcExtras;
                return reservation;
            }
            catch
            {
                return reservation;
            }
        }

        private string IsLocationOpenNow(string locationCode, DateTime pickupDateTime)
        {
            var message = "OK";
            var hrs = bm.Hours.FirstOrDefault(h => h.Location_Code == locationCode && h.Hour_Day == (int)pickupDateTime.DayOfWeek);

            if (hrs != null)
            {
                var from = DateTime.Parse(pickupDateTime.Date.ToShortDateString() + " " + PadTime24(hrs.Hour_HourFrom.ToString()));
                var to = DateTime.Parse(pickupDateTime.Date.ToShortDateString() + " " + PadTime24(hrs.Hour_HourTo.ToString()));

                if (to < from) to = to.AddDays(1);

                if (pickupDateTime < from || pickupDateTime > to) return Copy.Info_PickupLocationClosed;
            }
            else
            {
                return Copy.Info_PickupLocationClosed;
            }

            return message;
        }

        private string IsLocationOpenAndWithinTimeContraints(string locationCode, DateTime pickupDateTime, int hoursBeforePickup, bool isReturn = false)
        {
            var message = "OK";
            var hrs = bm.Hours.FirstOrDefault(h=>h.Location_Code == locationCode && h.Hour_Day == (int) pickupDateTime.DayOfWeek);

            if (hrs != null)
            {
                var from = DateTime.Parse(pickupDateTime.Date.ToShortDateString() + " " + PadTime24(hrs.Hour_HourFrom.ToString()));
                var to = DateTime.Parse(pickupDateTime.Date.ToShortDateString() + " " + PadTime24(hrs.Hour_HourTo.ToString()));
                if (to < from) to = to.AddDays(1);
                if (pickupDateTime < from || pickupDateTime > to) return Copy.Info_PickupLocationClosed;
            }
            else
            {
                return Copy.Info_PickupLocationClosed;
            }

            if (isReturn) return message;

            var openMinutes = 0;
            var z = 1;
            var bookingTime = DateTime.Now;
            do
            {
                if (IsLocationOpenNow(locationCode, DateTime.Now.AddMinutes(z)) == "OK")
                {
                    openMinutes += 1;
                }
                if (openMinutes >= (hoursBeforePickup*60))
                {
                    bookingTime = DateTime.Now.AddMinutes(z);
                    break;
                }
                z += 1;
            } while (true);

            if (bookingTime > pickupDateTime)
            {
                return Copy.Info_EarliestPickup + bookingTime.ToString("dd MMMM yyyy HH:mm");
            }



            //int totalDays =  int.Parse(DateDiff(DateTime.Now.ToString(), pickupDateTime.ToString()));
            //if (DateTime.Now.Day == pickupDateTime.Day) totalDays = 0;
            //double totalMins = 0;

            //if(totalDays == 0)
            //{
            //    totalMins = TimeDiff(DateTime.Now, pickupDateTime);
            //}
            //else
            //{
            //    for(int i = 0; i <= totalDays; i++)
            //    {
            //        if(i == 0)
            //        {
            //            totalMins += OpenFromPointInTimeOfDay(locationCode, DateTime.Now, "after");
            //        }
            //        if(i > 0 && i < totalDays)
            //        {
            //            totalMins += OpenFromPointInTimeOfDay(locationCode, DateTime.Now.AddDays(i), "both");
            //        }
            //        if(i == totalDays)
            //        {
            //            totalMins += OpenFromPointInTimeOfDay(locationCode, pickupDateTime, "before");
            //        }
            //    }
            //}

            //var totalHours = totalMins/60;
            //if (totalHours < hoursBeforePickup)
            //{
            //    var shortfall = (hoursBeforePickup*60) - totalMins;
            //    return Copy.Info_EarliestPickup + pickupDateTime.AddMinutes(shortfall).ToString("dd MMMM yyyy HH:mm");
            //}

            return message;
        }

        private double OpenFromPointInTimeOfDay(string locationCode, DateTime pointInTime, string interest)
        {
            double before = 0;
            double after = 0;

            var hrs = bm.Hours.FirstOrDefault(h => h.Location_Code == locationCode && h.Hour_Day == (int)pointInTime.DayOfWeek);

            if (hrs != null)
            {
                var from = DateTime.Parse(pointInTime.Date.ToShortDateString() + " " + PadTime24(hrs.Hour_HourFrom.ToString()));
                var to = DateTime.Parse(pointInTime.Date.ToShortDateString() + " " + PadTime24(hrs.Hour_HourTo.ToString()));
                before = TimeDiff(from, pointInTime);
                after = TimeDiff(pointInTime, to);
            }

            if (interest == "before")
            {
                return before;
            }

            if (interest == "after")
            {
                return after;
            }
            return before + after;
        }

        private string PadTime24(string time)
        {
            try
            {
                if (time.Length == 1) return "00:0" + time;

                if (time.Length == 3) time = "0" + time;

                time = time.Substring(0, 2) + ":" + time.Substring(2, 2);

                return time;
            }
            catch (Exception ex)
            {
                return "09:00";
            }
        }


        private string FilterString(string input, bool Scandinavian)
        {
            if (Scandinavian)
            {
                input = input.Replace("Æ", "AE")
                    .Replace("Ä", "AE")
                    .Replace("Ø", "OE")
                    .Replace("Ö", "OE")
                    .Replace("Å", "AA")
                    .Replace("ö", "oe")
                    .Replace("ä", "ae")
                    .Replace("å", "aa")
                    .Replace("ø", "oe")
                    .Replace("æ", "ae")
                    .Replace("é", "e");
            }
            else
            {
                
                input = input.Replace("Å", "A")
                    .Replace("Ä", "A")
                    .Replace("Æ", "A")
                    .Replace("Ø", "O")
                    .Replace("Ö", "O")
                    .Replace("ö", "o")
                    .Replace("ä", "a")
                    .Replace("å", "a")
                    .Replace("ø", "o")
                    .Replace("æ", "a")
                    .Replace("é", "e");
            }
            
            return input;
        }

        public void SetCulture(string culture)
        {
            // Validate input
            culture = Session["culture"].ToString();

            // Save culture in a cookie
            HttpCookie cookie = Request.Cookies["_culture"];
            if (cookie != null)
                cookie.Value = culture; // update cookie value
            else
            {

                cookie = new HttpCookie("_culture");
                cookie.HttpOnly = false; // Not accessible by JS.
                cookie.Value = culture;
                cookie.Expires = DateTime.Now.AddYears(1);
            }
            Response.Cookies.Add(cookie);
        }

        

        public FileContentResult GetFile(int id)
        {
            SqlDataReader rdr;
            byte[] fileContent = null;
            string mimeType = "";
            string fileName = "";
            var connect = System.Configuration.ConfigurationManager.AppSettings["CMSDatabase"];

            using (var conn = new SqlConnection(connect))
            {
                var qry = "SELECT image, ImageType, ImageFileName FROM items WHERE ID = " + id;
                var cmd = new SqlCommand(qry, conn);
                conn.Open();
                rdr = cmd.ExecuteReader();
                if (rdr.HasRows)
                {
                    rdr.Read();
                    fileContent = (byte[])rdr["image"];
                    mimeType = rdr["ImageType"].ToString();
                    fileName = rdr["ImageFileName"].ToString();
                }
            }
            return File(fileContent, mimeType, fileName);
        }



//        private static string _Normalize(string input)
//        {

//            input = input
//                .Replace("�", "oe")
//                .Replace("�", "OE")
//                .Replace("�", "ae")
//                .Replace("�", "AE")
//                .Replace("�", "aa")
//                .Replace("�", "AA");
//            return Normalize(input, true, null, null, null, null, null);
//        }


//        public static string Normalize(
//             string input,
//             bool replaceScandinavian,
//             string replaceWhitespace,
//             string replacePunctuation,
//             string replaceSymbols,
//             string replaceNonAscii,
//             string replaceNonUrl)
//        {
//            string normalized =
//input.Normalize(NormalizationForm.FormKD);
//            var buffer = new StringBuilder();
//            for (int i = 0; i < normalized.Length; i++)
//            {
//                char c = normalized[i];
//                UnicodeCategory cat =
//                CharUnicodeInfo.GetUnicodeCategory(c);
//                // Also replace Scandinavian characters not considered composites by unicode
//                if ("������".IndexOf(c) >= 0)
//                {
//                    if (replaceScandinavian)
//                    {
//                        if /**/ (c == '�') buffer.Append('o');
//                        else if (c == '�') buffer.Append('O');
//                        else if (c == '�') buffer.Append('a');
//                        else if (c == '�') buffer.Append('A');
//                        else if (c == '�') buffer.Append('a');
//                        else if (c == '�') buffer.Append('A');
//                    }
//                    else
//                    {
//                        buffer.Append(c);
//                    }
//                }
//                else if (char.IsWhiteSpace(c) && replaceWhitespace !=
//null)
//                {
//                    buffer.Append(replaceWhitespace);
//                }
//                else if (char.IsPunctuation(c) && replacePunctuation !=
//null)
//                {
//                    buffer.Append(replacePunctuation);
//                }
//                else if (char.IsSymbol(c) && replaceSymbols != null)
//                {
//                    buffer.Append(replaceSymbols);
//                }
//                else if (c >= 0x80 && replaceNonAscii != null)
//                {
//                    buffer.Append(replaceNonAscii);
//                }
//                else if (IsUrlChar(c, false, false) && replaceNonUrl !=
//null)
//                {
//                    buffer.Append(replaceNonUrl);
//                }
//                else if (
//                    cat != UnicodeCategory.NonSpacingMark &&
//                    cat != UnicodeCategory.SpacingCombiningMark &&
//                    cat != UnicodeCategory.ModifierSymbol &&
//                    cat != UnicodeCategory.ModifierLetter)
//                {
//                    buffer.Append(c);
//                }
//            }
//            return buffer.ToString();
//        }



    }
}
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                