﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Mvc;

namespace SEB.Controllers
{
    public class BaseController : Controller
    {
        protected static readonly log4net.ILog logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public string Culture;

        protected override void ExecuteCore()
        {
            string cultureName = null;
            // Attempt to read the culture cookie from Request
            HttpCookie cultureCookie = Request.Cookies["_culture"];
            if (cultureCookie != null)
                cultureName = cultureCookie.Value;
            else
                cultureName = "sv-SE"; 


            // Modify current thread's culture 
            Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(cultureName);
            Thread.CurrentThread.CurrentUICulture = CultureInfo.CreateSpecificCulture(cultureName);

            ViewBag.CMSItem1 = SEB.helpers.CMSHelper.GetCMSItem(1);
            ViewBag.CMSItem2 = SEB.helpers.CMSHelper.GetCMSItem(2);
            ViewBag.CMSItem3 = SEB.helpers.CMSHelper.GetCMSItem(3);

            Culture = MapCultureStrings(cultureName);

            HttpCookie cardCookie = Request.Cookies["_cardMake"];
            if (cardCookie != null && Session["chosenCardmake"] == null)
            {
                Session["chosenCardmake"] = cardCookie.Value;
            }

            
            base.ExecuteCore();
        }

        private void getCardMakeCookie()
        {
            HttpCookie cardCookie = Request.Cookies["_cardMake"];
            if (cardCookie != null)
                Session["chosenCardmake"] = cardCookie.Value;

        }
        private string MapCultureStrings(string fullCulture)
        {
            switch (fullCulture)
            {
                case "sv-SE":
                    return "SE";
                case "nb-NO":
                    return "NO";
                case "fi-FI":
                    return "GB";
                case "da-DK":
                    return "DK";
                default:
                    return "GB";
            }
        }

    }
}