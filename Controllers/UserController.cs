﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Validation;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Security;
//using SEB.DCService;
using SEB.Service;
using SEB.Entities;
using System.Data.Entity.Infrastructure;
using System.Configuration;
using System.Threading;
using System.Globalization;
using Xeed.Runtime.Serialization;

namespace SEB.Controllers
{
    public class UserController : BaseController
    {

        SEBContext db = new SEBContext();
        BookMakerContext bm = new BookMakerContext();

        [Authorize]
        public ActionResult Detail()
        {
            User u = db.Users.Find(User.Identity.Name);

            var c = db.Countries.OrderBy(o => o.SortOrder).ThenBy(o => o.CountryText).ToList();
            var l = new List<SelectListItem>();
            foreach (var cty in c)
            {
                var li = new SelectListItem {Text = cty.CountryText, Value = cty.id.ToString()};
                if (u.Country == cty.id) li.Selected = true;
                l.Add(new SelectListItem { Text = cty.CountryText, Value = cty.id.ToString() });
            }

            ViewBag.Countries = l;

            return View(u);
        }

        [Authorize]
        [HttpPost]
        public ActionResult Detail(User user)
        {

            if (ModelState.IsValid)
            {
                var reservation = Session["reservation"] as DCReservation ?? new DCReservation();
                reservation.DriverGivenName = user.FirstName;
                reservation.DriverSurname = user.LastName;
                reservation.DriverBirthDate = user.DateOfBirth;
                reservation.DriverPostName = user.LastName;
                Session["reservation"] = reservation;
                user.UserStatus = 1; 
                //db.Users.Attach(user);
                db.Entry(user).State = EntityState.Modified;
                db.SaveChanges();
                ViewBag.Message = "<script>alert('" + Resources.Copy.Info_DetailsSaved + "');</script>";
            }
            else
            {
                ViewBag.Message = "<script>alert('" + Resources.Copy.Info_DetailsSavedError + "');</script>";
            }

            var c = db.Countries.OrderBy(o => o.SortOrder).ThenBy(o => o.CountryText).ToList();
            var l = new List<SelectListItem>();
            foreach (var cty in c)
            {
                var li = new SelectListItem { Text = cty.CountryText, Value = cty.id.ToString() };
                if (user.Country == cty.id) li.Selected = true;
                l.Add(new SelectListItem { Text = cty.CountryText, Value = cty.id.ToString() });
            }

            ViewBag.Countries = l;

            return View(user);
        }

        
        public ActionResult Register()
        {
            if (User.Identity.IsAuthenticated) return RedirectToAction("detail", "user");

            var c = db.Countries.OrderBy(o => o.SortOrder).ThenBy(o=>o.CountryText).ToList();
            var l = new List<SelectListItem>();
            foreach (var cty in c)
            {
                l.Add(new SelectListItem{Text = cty.CountryText, Value = cty.id.ToString()});
            }

            ViewBag.Countries = l;
            return View();
        }

        
        [HttpPost]
        public ActionResult Register(User user, FormCollection frm)
        {

            var c = db.Countries.OrderBy(o => o.SortOrder).ThenBy(o => o.CountryText).ToList();
            var l = new List<SelectListItem>();
            foreach (var cty in c)
            {
                l.Add(new SelectListItem { Text = cty.CountryText, Value = cty.id.ToString() });
            }

            ViewBag.Countries = l;

            if (db.Users.FirstOrDefault(u => u.Email == user.Email) != null)
            {
                ViewBag.Message1 = "<script>alert('" + Resources.Copy.UserExistsRegister + "');</script>";
                return View(user);
            }

            if (ModelState.IsValid)
            {
                user.UserStatus = 1;
                db.Users.Add(user);
                db.SaveChanges();
                Session["user"] = user.Email;
                string body = string.Empty;

                body =
                    "<html><body>" + Resources.Copy.Dear + user.FirstName + Resources.Copy.Registration_Email_Text;

                body += "<a href=\"" + ConfigurationManager.AppSettings["HostUrl"] + "/booking/bookingdetails\">" + ConfigurationManager.AppSettings["HostUrl"] +
                        "/booking/bookingdetails</a></body></html>";

                CommonHelper.SendMail(Resources.Copy.Registration_Email_Subject, body, user.Email, "", ConfigurationManager.AppSettings["ThirdPartyBCCEmailAddress"]);
                
                FormsAuthentication.RedirectFromLoginPage(user.Email,false);
                if (Request["return"] != null && Request["return"] == "step3")
                {
                    return RedirectToAction("bookingsummary", "booking");
                }
                return RedirectToAction("detail", "user");
            }

           

            ViewBag.Message = Resources.Copy.RegistrationFailed;

            ViewBag.terms = true;

            return View(user);
        }

        public ActionResult Login(string name)
        {
            if (Request["act"] == "terms")
            {
                return View("../home/terms");
            }

            if (Request["act"] == "portal")
            {
                ViewBag.User = Request["user"];
                return View("../user/portal");
            }

            if (!string.IsNullOrEmpty(name))
            {
                var usr = db.Users.FirstOrDefault(u => u.Email == name);
                if (usr != null)
                {
                    var msg = Resources.Copy.YourPassword + ": " + usr.Pw;
                    CommonHelper.SendMail(Resources.Copy.YourPassword, msg, usr.Email, null, null);
                }

                var success = "ok";
                return Json(success, JsonRequestBehavior.AllowGet);
            }

            if (Request["c"] == "sweden")
            {
                Thread.CurrentThread.CurrentCulture = new CultureInfo("sv-SE");
                Thread.CurrentThread.CurrentUICulture = Thread.CurrentThread.CurrentCulture;
                Session["culture"] = "sv-SE"; 
                SetCulture("sv-SE");
                return new ContentResult(); }
            if (Request["c"] == "uk")
            {
                Thread.CurrentThread.CurrentCulture = new CultureInfo("en-GB");
                Thread.CurrentThread.CurrentUICulture = Thread.CurrentThread.CurrentCulture;
                Session["culture"] = "en-GB";
                SetCulture("en-GB");
                return new ContentResult(); }

            if(Request["act"] == "logout")
            {
                FormsAuthentication.SignOut();
                Session["user"] = null;
                Session["reservation"] = null;
                Session["Project_Full"] = null;
                Session["ProjectNumber"] = null;
                Session["Company_Name"] = null;
                Session["IsDelivery"] = null;
                Session["IsCollection"] = null;

                return RedirectToAction("bookingdetails", "Booking");
            }

            //string ip;

            //if(MvcApplication.OnTrustedNetwork(out ip))
            //{
            //    var bits = Request.Url.ToString().Split('/');
            //    string user = bits.Last().Split('?')[0];
            //    User usr = db.Users.FirstOrDefault(u => u.EmployeeId == user);

            //    if(usr != null && (usr.EndDate == null || usr.EndDate > DateTime.Now))
            //    {
                    
            //        FormsAuthentication.RedirectFromLoginPage(usr.EmployeeId, false);
            //        if (Request["p"] != null && Request["p"].ToLower() == "public")
            //        {
            //            return RedirectToAction("public", "Booking");
            //        }
            //        if (Request["context"] != null && Request["context"].ToLower() == "wizard")
            //        {
            //            return Redirect(Request["ReturnUrl"]);
            //        }
            //        return RedirectToAction("detail", "user", new { @id = usr.EmployeeId });

            //    }
            //}

            //ViewBag.Message = Resources.Copy.Login_YourIP + ip;

            return View();
        }

        public void SetCulture(string culture)
        {
            // Validate input
            culture = Session["culture"].ToString();

            // Save culture in a cookie
            HttpCookie cookie = Request.Cookies["_culture"];
            if (cookie != null)
                cookie.Value = culture; // update cookie value
            else
            {

                cookie = new HttpCookie("_culture");
                cookie.HttpOnly = false; // Not accessible by JS.
                cookie.Value = culture;
                cookie.Expires = DateTime.Now.AddYears(1);
            }
            Response.Cookies.Add(cookie);
        }

        public string setLanguage(string language, string card)
        {
            switch (language)
            {
                case "SE":
                    Thread.CurrentThread.CurrentCulture = new CultureInfo("sv-SE");
                    Thread.CurrentThread.CurrentUICulture = Thread.CurrentThread.CurrentCulture;
                    Session["culture"] = "sv-SE";
                    SetCulture("sv-SE");
                    break;
                case "DK":
                    Thread.CurrentThread.CurrentCulture = new CultureInfo("da-DK");
                    Thread.CurrentThread.CurrentUICulture = Thread.CurrentThread.CurrentCulture;
                    Session["culture"] = "da-DK";
                    SetCulture("da-DK");
                    break;
                case "FI":
                    Thread.CurrentThread.CurrentCulture = new CultureInfo("fi-FI");
                    Thread.CurrentThread.CurrentUICulture = Thread.CurrentThread.CurrentCulture;
                    Session["culture"] = "fi-FI";
                    SetCulture("fi-FI");
                    break;
                default:
                    Thread.CurrentThread.CurrentCulture = new CultureInfo("nb-NO");
                    Thread.CurrentThread.CurrentUICulture = Thread.CurrentThread.CurrentCulture;
                    Session["culture"] = "nb-NO";
                    SetCulture("nb-NO");
                    break;
            }
            Session["chosenLanguage"] = language;
            Session["reservation"] = null;
            return "true";
        }

        [HttpPost]
        public ActionResult LoginWidget(string username, string pw)
        {
            var usr = db.Users.FirstOrDefault(u => u.Email == username && u.Pw == pw);
            if (usr != null)
            {
                Session["user"] = usr.Email;
                FormsAuthentication.SetAuthCookie(usr.Email, false);
                return Json(new
                {
                    firstname = usr.FirstName,
                    lastname = usr.LastName,
                    email = usr.Email,
                    add1 = usr.Address1,
                    add2 = usr.Address2,
                    city = usr.City,
                    state = usr.State,
                    postcode = usr.PostCode,
                    country = usr.Country,
                    dob = usr.DateOfBirth.ToShortDateString(),
                    license = usr.DriversLicense,
                    phone = usr.MobileNumber
                }); 
            }
            else
            {
                return Json(new {email = ""}, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult DoesNotLoggedInUserExist(string email)
        {
            if (!User.Identity.IsAuthenticated && db.Users.FirstOrDefault(u => u.Email == email) != null)
            {
                return Json(new { result = "true" });
            }
            return Json(new { result = "false" });
        }


        [HttpPost]
        public ActionResult SubmitDriverDetails(string firstname, string lastname,string email,string phone,string add1,string postcode,string country, string dob, string license)
        {
            try
            {
            var countryCode = int.Parse(country);
            var countryText = db.Countries.FirstOrDefault(c => c.id == countryCode).CountryText;
                var reservation = Session["reservation"] as Service.DCReservation;
                reservation.DriverGivenName = firstname;
                reservation.DriverSurname = lastname;
                reservation.DriverEmail = email;
                reservation.DriverPhone = phone;
                reservation.DriverStreet = add1;
                reservation.DriverPostCode = postcode;
                reservation.DriverCountry = bm.Regions.FirstOrDefault(r=>r.Country_Name == countryText).Country_Code;
                reservation.DriverBirthDate = DateTime.Parse(dob);
                //reservation.DriversLicense = license;
                //reservation.DriverPostName = 

                var resid = Guid.Parse(reservation.DriverRemarks);
                var s_Reservation = db.Reservations.FirstOrDefault(r => r.Reservation_ID == resid);
                s_Reservation.Reservation_Amount = reservation.Amount;
                s_Reservation.Reservation_DriverFirstName = reservation.DriverGivenName;
                s_Reservation.Reservation_DriverLastName = reservation.DriverSurname;
                s_Reservation.Reservation_PickupDate = reservation.PickupDateTime;
                s_Reservation.Reservation_PickupLocation = reservation.PickupLocation;
                s_Reservation.Reservation_DropoffDate = reservation.ReturnDateTime;
                s_Reservation.Reservation_DropoffLocation = reservation.ReturnLocation;
                s_Reservation.Reservation_Updated = DateTime.Now;
                s_Reservation.Reservation_UserID = User.Identity.Name;
                s_Reservation.Reservation_FormState = SerializationUtility.SaveXml(reservation);
                s_Reservation.Driver_Licence = license;

                db.Entry(s_Reservation).State = EntityState.Modified;
                db.SaveChanges();

                Session["reservation"] = reservation;

                return Json(new { success = "true" });
            }
            catch (DbEntityValidationException e)
            {
                foreach (var eve in e.EntityValidationErrors)
                {
                    Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                        eve.Entry.Entity.GetType().Name, eve.Entry.State);
                    foreach (var ve in eve.ValidationErrors)
                    {
                        Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                            ve.PropertyName, ve.ErrorMessage);
                    }
                }
                return Json(new { success = "false" });


            }
            catch (Exception ex)
            {
                return Json(new { success = "false" });
            }
            
        }


        [HttpPost]
        public ActionResult Login(FormCollection frm, string button)
        {
            string user = frm["txtUser"];
            string wizard = frm["txtPassword"];

            if (button == Resources.Copy.Login)
            {
                var usr =
                    db.Users.FirstOrDefault(u => u.Email == user && u.Pw == wizard);

                //////if (usr != null && (usr.EndDate == null || usr.EndDate > DateTime.Now))
                //////{
                if (usr != null)
                {
                    FormsAuthentication.RedirectFromLoginPage(usr.Email, false);
                    return RedirectToAction("bookingdetails", "booking", new { @id = usr.Email });
                }
                //////    if (Request["p"] != null && Request["p"].ToLower() == "public")
                //////    {
                //////        return RedirectToAction("public", "Booking");
                //////    }
                //////}

                ViewBag.Message = Resources.Copy.Info_LoginUnsuccessful;
            }
            //////else
            //////{
            //////    if (string.IsNullOrEmpty(user))
            //////    {
            //////        ViewBag.Message = "<script>alert('" + Resources.Copy.Info_enterUserId + "')</script>";
            //////        return View();
            //////    }
            //////    var usr = db.Users.Where(u => u.EmployeeId == user && u.WizardNumber != "").FirstOrDefault();
            //////    if (usr == null)
            //////    {
            //////        ViewBag.Message = "<script>alert('" + Resources.Copy.Info_NoUserOrNotActive + "')</script>";
            //////        return View();
            //////    }

            //////    string body = Resources.Copy.Info_YourWizardNumberIs + usr.WizardNumber + "\n\r";

            //////    CommonHelper.SendMail(Resources.Copy.Info_YourWizardNumber, body, usr.Email, null, null);

            //////    ViewBag.Message = "<script>alert('" + Resources.Copy.Info_WizardNumberSent + "')</script>";
            //////}


            return View();
        }

        public ActionResult Contact()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Contact(ViewModels.contactViewModel viewModel)
        {

            var email = "klas.olsson@avis.se";
            var language = Session["chosenLanguage"].ToString();
            var enquiryRecipient = db.EnquiryRecipient.FirstOrDefault(e => e.Company == viewModel.Company && e.Enquiry == viewModel.EnquiryType
                    && e.Language == language);
            var company = viewModel.Company == 1 ? "Avis" : "Budget";
            var enquiryType = "";
            switch (viewModel.EnquiryType)
            {
                case 1:
                    enquiryType = "Technical issue/help";
                    break;
                case 2:
                    enquiryType = "Reservation Enquiry";
                    break;
                case 3:
                    enquiryType = "Customer service";
                    break;
            }

            if (enquiryRecipient != null) email = enquiryRecipient.EquiryRecipient;

            string body = "<b>Company:</b> " + company + "<br /><br />";
            body += "<b>Enquiry Type:</b> " + enquiryType + "<br /><br />";
            body += "<b>First name:</b> " + viewModel.FirstName + "<br /><br />";
            body += "<b>Last name:</b> " + viewModel.LastName + "<br /><br />";
            body += "<b>Email Address:</b> " + viewModel.EmailAddress + "<br /><br />";
            body += "<b>Phone:</b> " + viewModel.PhoneNumber + "<br /><br />";
            body += "<b>Message:</b><br />" + viewModel.Enquiry + "<br /><br />";
            CommonHelper.SendMail(company + " enquiry from SEB Booking System : " + enquiryType, body, email, null, null);

            ViewBag.Message = "<script>alert('" + Resources.Copy.Info_EnquirySent + "'); window.location.href=('" + CommonHelper.GetHostUrl() + "/booking/bookingdetails?ctx=new');</script>";
            //return RedirectToAction("bookingdetails", "booking");
            return View();
        }

        public string sweden()
        {
            Thread.CurrentThread.CurrentCulture = new CultureInfo("se-SE");

            return "sweden";
        }

        public string english()
        {
            Thread.CurrentThread.CurrentCulture = new CultureInfo("en-GB");

            return "sweden";
        }

        [Authorize]
        public ActionResult ChangePassword()
        {
            var pw = new ViewModels.ChangePasswordViewModel();
            return View(pw);
        }

        [Authorize]
        [HttpPost]
        public ActionResult ChangePassword(ViewModels.ChangePasswordViewModel viewModel)
        {
            if (ModelState.IsValid)
            {
                if (viewModel.NewPassword == viewModel.ConfirmNewPassword)
                {
                    var userId = User.Identity.Name;
                    var user = db.Users.First(u => u.Email == userId);
                    if (user.Pw == viewModel.OldPassword)
                    {
                        user.Pw = viewModel.NewPassword;
                        db.Entry(user).State = EntityState.Modified;
                        db.SaveChanges();
                        ViewBag.Message = "<script>alert('" + Resources.Copy.PasswordChanged + "');</script>";
                    }
                    else
                    {
                        ViewBag.Message = "<script>alert('" + Resources.Copy.IncorrectOldPassword + "');</script>";
                    }

                }
                else
                {
                    ViewBag.Message = "<script>alert('" + Resources.Copy.PasswordConfirmationMatch + "');</script>";
                }
            }

            return View();
        }
    }

    
}
