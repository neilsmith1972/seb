﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SEB.Controllers
{
    public class HomeController : BaseController
    {
        //
        // GET: /Home/

        public ActionResult PUL()
        {
            return View();
        }

        public ActionResult AvisPreferred(int item)
        {

            return View();
        }

        public ActionResult terms()
        {
            return View();
        }

        public ActionResult BookingConditions()
        {
            return View();
        }

        public ActionResult Index(string state)
        {
            switch (state)
            {
                case "noUser":
                    ViewBag.Message =
                        Resources.Copy.Info_NoUser;
                    break;

                case "activationRequested":
                    ViewBag.Message =
                        Resources.Copy.Info_ProfileRegistered;
                    break;
                case"activationCompleted":
                    ViewBag.Message =
                        Resources.Copy.Info_ProfileUpdated;
                    break;
                    
                case "unabletobook":
                    ViewBag.Message =
                        Resources.Copy.Info_UnableToBook;
                    break;

            }
            return View();
        }

       

    }
}
