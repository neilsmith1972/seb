﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SEB.InvoiceService;
using SEB.Models;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.IO;
using System.Net;

namespace SEB.Controllers
{
    public class UtilsController : BaseController
    {

        SEBContext db = new SEBContext();

        //public ActionResult LostWizardNumber()
        //{
        //    return View();
        //}

        //[HttpPost]
        //public ActionResult LostWizardNumber(ViewModels.WizardViewModel viewModel)
        //{

        //    var usr = db.Units.Where(u => u.LoginId == viewModel.UserName && u.WizardNumber != "").FirstOrDefault();
        //    if (usr == null)
        //    {
        //        ViewBag.Message = "<script>alert('" + Resources.Copy.Info_NoUserOrNotActive + "')</script>";
        //        return View(viewModel);
        //    }

        //    string body = Resources.Copy.Info_YourWizardNumberIs + usr.WizardNumber + "\n\r";

        //    CommonHelper.SendMail(Resources.Copy.Info_YourWizardNumber, body, usr.EmailAddress, null, null);

        //    ViewBag.Message = "<script>alert('" + Resources.Copy.Info_WizardNumberSent + "')</script>";
        //    return View();
        //}

        public ActionResult GetInvoice(string InvoiceNumber)
        {

            try
            {
                WebClient client = new WebClient();
                MemoryStream stream = new MemoryStream(client.DownloadData(ConfigurationManager.AppSettings["invoiceserviceUrl"] + InvoiceNumber));

                client.Dispose();

                stream.Position = 0;
                return new FileStreamResult(stream, "application/pdf") { FileDownloadName = "invoice_" + InvoiceNumber + ".pdf" };
            }

            
            catch (Exception ex)
            {

            }
            
            return View();
        }

    }
}
