using System;
using System.Collections.Generic;

namespace SEB.Entities
{
	public class Region
	{
		public System.Guid ID { get; set; }
		public Nullable<int> Import_ID { get; set; }
		public Nullable<System.DateTime> Region_Created { get; set; }
		public Nullable<int> Region_Priority { get; set; }
		public string Region_Code { get; set; }
		public string Region_Name { get; set; }
		public string Country_Code { get; set; }
		public string Country_Name { get; set; }
		public string Continent_Code { get; set; }
		public string Continent_Name { get; set; }
        public string Language_ID { get; set; }
	}
}

