using System;
using System.Collections.Generic;

namespace SEB.Entities
{
	public class ReservationHistory
	{
		public System.Guid Reservation_ID { get; set; }
		public Nullable<System.Guid> State_ID { get; set; }
		public System.DateTime Reservation_Created { get; set; }
		public Nullable<System.DateTime> Reservation_Updated { get; set; }
		public string Reservation_UserID { get; set; }
		public string Reservation_UserCode { get; set; }
		public string Reservation_DCID { get; set; }
		public string Reservation_DCName { get; set; }
		public string Reservation_Name { get; set; }
		public string Reservation_Type { get; set; }
		public string Reservation_Info { get; set; }
		public string Reservation_Country { get; set; }
		public string Reservation_Driver { get; set; }
		public string Reservation_DriverLastName { get; set; }
		public string Reservation_DriverFirstName { get; set; }
		public string Reservation_Company { get; set; }
		public string Reservation_PickupLocation { get; set; }
		public Nullable<System.DateTime> Reservation_PickupDate { get; set; }
		public string Reservation_DropoffLocation { get; set; }
		public Nullable<System.DateTime> Reservation_DropoffDate { get; set; }
		public Nullable<decimal> Reservation_Amount { get; set; }
		public string Reservation_FormState { get; set; }
	}
}

