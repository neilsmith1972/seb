using System;
using System.Collections.Generic;

namespace SEB.Entities
{
	public class Login
	{
		public System.Guid LoginID { get; set; }
		public System.Guid User_ID { get; set; }
		public System.DateTime Login_Date { get; set; }
		public string Login_RemoteAddress { get; set; }
		public string Login_UserAgent { get; set; }
	}
}

