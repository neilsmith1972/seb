﻿

namespace SEB.Entities
{
    public class Country
    {
        public int id { get; set; }
        public string CountryText { get; set; }
        public int SortOrder { get; set; }
    }
}