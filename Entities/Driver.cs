using System;
using System.Collections.Generic;

namespace SEB.Entities
{
	public class Driver
	{
		public System.Guid Driver_ID { get; set; }
		public string Driver_ExternalID { get; set; }
		public System.DateTime Driver_Created { get; set; }
		public Nullable<System.DateTime> Driver_Updated { get; set; }
		public string Driver_WizardNumber { get; set; }
		public string Driver_AwdNumber { get; set; }
		public Nullable<System.DateTime> Driver_Exported { get; set; }
		public string Driver_LastName { get; set; }
		public string Driver_FirstName { get; set; }
		public string Driver_Street { get; set; }
		public string Driver_PostalCode { get; set; }
		public string Driver_PostalName { get; set; }
		public string Driver_Country { get; set; }
		public Nullable<System.DateTime> Driver_BirthDate { get; set; }
		public string Driver_Gender { get; set; }
		public string Driver_Phone { get; set; }
		public string Driver_Email { get; set; }
		public string Driver_License { get; set; }
		public string Driver_FrequentFlyerNo { get; set; }
		public string Driver_Remarks { get; set; }
	}
}

