using System;
using System.Collections.Generic;

namespace SEB.Entities
{
	public class WSCache
	{
		public string ID { get; set; }
		public System.DateTime Added { get; set; }
		public Nullable<System.DateTime> Expires { get; set; }
		public byte[] Data { get; set; }
	}
}

