﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SEB.Service;

namespace SEB.Entities
{
    public class car
    {
        public string carGroup { get; set; }
        public DCVehicleSize sizeCode { get; set; }
        public DCVehicleCategory category { get; set; }
        public string rateQualifier { get; set; }
        public decimal total { get; set; }
        public string carCurrencyCode { get; set; }
        public string carGroupLetter { get; set; }
        public string status { get; set; }
        public string company { get; set; }
    }
}