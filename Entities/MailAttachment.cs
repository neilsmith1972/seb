using System;
using System.Collections.Generic;

namespace SEB.Entities
{
	public class MailAttachment
	{
		public System.Guid Mail_ID { get; set; }
		public string MailAttachment_Name { get; set; }
		public string MailAttachment_Type { get; set; }
		public byte[] MailAttachment_Data { get; set; }
	}
}

