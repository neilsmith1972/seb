namespace SEB.Entities
{
	public class Site
	{
		public int id { get; set; }
		public string ShortCultureCode { get; set; }
		public string FullCultureCode { get; set; }
		public string BackgroundImage { get; set; }
		public string PromoHtml { get; set; }
		public string PromoCarsHtml { get; set; }
        public string PaymentCID { get; set; }
        public string IATANumber { get; set; }
        public string AWDNumber { get; set; }
        public string WizardNumber { get; set; }
    }
}

