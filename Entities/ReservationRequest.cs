using System;
using System.Collections.Generic;

namespace SEB.Entities
{
	public class ReservationRequest
	{
		public System.Guid Reservation_ID { get; set; }
		public System.Guid Request_ID { get; set; }
		public Nullable<bool> ReservationRequest_Current { get; set; }
	}
}

