using System;
using System.Collections.Generic;

namespace SEB.Entities
{
	public class Location
	{
		public System.Guid Location_ID { get; set; }
		public Nullable<int> Import_ID { get; set; }
		public System.DateTime Location_Created { get; set; }
		public string Location_Code { get; set; }
        public string Location_Language_ID { get; set; }
		public string Location_Name { get; set; }
		public string Region_Code { get; set; }
		public string Region_Name { get; set; }
		public string Location_StationType { get; set; }
		public double Location_Latitude { get; set; }
		public double Location_Longitude { get; set; }
		public string Location_Phone { get; set; }
		public string Location_PhoneAlt { get; set; }
		public bool Location_GeoCodeAccurate { get; set; }
		public bool Location_AfterHoursReturn { get; set; }
		public bool Location_HasSkiRack { get; set; }
		public bool Location_HasSnowTyres { get; set; }
		public bool Location_HasSnowChains { get; set; }
		public bool Location_HasChildSeat { get; set; }
		public bool Location_HasRoofLuggage { get; set; }
		public bool Location_HasHandControl { get; set; }
		public bool Location_IsGpsAvailable { get; set; }
		public bool Location_IsAvisPreferred { get; set; }
		public bool Location_IsShuttleServiceAvailable { get; set; }
		public bool Location_IsRoadServiceAvailable { get; set; }
		public string Location_Email { get; set; }
		public string Location_Address { get; set; }
		public string Location_City { get; set; }
		public string Location_PostCode { get; set; }

	    public string LocationText;
	    public int LocationCode;
	    public string DistanceFromWork;
        //public Address Address { get; set; }
	    public string StationName;
	    public string PhoneNumber;
	    public string StationType;
	    public string StationOpenOrClosed;
	    public string SundayCaption;
	    public string SundayOpeningHours;
	    public string MondayCaption;
	    public string MondayOpeningHours;
	    public string TuesdayCaption;
	    public string TuesdayOpeningHours;
	    public string WednesdayCaption;
	    public string WednesdayOpeningHours;
	    public string ThursdayCaption;
	    public string ThursdayOpeningHours;
	    public string FridayCaption;
	    public string FridayOpeningHours;
	    public string SaturdayCaption;
	    public string SaturdayOpeningHours;

	    public string IsOpenNow;
	}
}

