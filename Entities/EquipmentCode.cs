using System;
using System.Collections.Generic;

namespace SEB.Entities
{
	public class EquipmentCode
	{
		public string Equipment_Description { get; set; }
		public string Equipment_Code { get; set; }
		public Nullable<int> Equipment_Number { get; set; }
	}
}

