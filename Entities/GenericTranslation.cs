﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SEB.Entities
{
    public class GenericTranslation
    {
        public int id { get; set; }
        public string Source { get; set; }
        public string CountryCode { get; set; }
        public string Translation { get; set; }
    }
}