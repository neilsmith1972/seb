using System;
using System.Collections.Generic;

namespace SEB.Entities
{
	public class Fleet
	{
		public System.Guid Fleet_ID { get; set; }
        public string Language_ID { get; set; }
		public Nullable<int> Import_ID { get; set; }
		public System.DateTime Fleet_Created { get; set; }
		public string Region_Code { get; set; }
		public string Fleet_CarGroup { get; set; }
		public string Fleet_SippCode { get; set; }
		public string Fleet_ModelName { get; set; }
		public string Fleet_ModelCode { get; set; }
		public Nullable<byte> Fleet_DoorCount { get; set; }
		public Nullable<byte> Fleet_SeatCount { get; set; }
		public Nullable<bool> Fleet_AirCondition { get; set; }
		public Nullable<bool> Fleet_AntiLockBreaks { get; set; }
		public Nullable<bool> Fleet_RadioCassette { get; set; }
		public Nullable<bool> Fleet_DriverAirbag { get; set; }
		public Nullable<bool> Fleet_PassengerAirbag { get; set; }
		public Nullable<bool> Fleet_ElectricWindow { get; set; }
		public Nullable<bool> Fleet_SunRoof { get; set; }
		public Nullable<bool> Fleet_Automatic { get; set; }
		public Nullable<byte> Fleet_LuggageCount { get; set; }
		public string Fleet_Description { get; set; }
		public Nullable<bool> Fleet_PowerSteering { get; set; }
		public Nullable<bool> Fleet_RearSeatbelts { get; set; }
		public Nullable<bool> Fleet_FourWheelDrive { get; set; }
		public Nullable<bool> Fleet_RadioOnly { get; set; }
		public string Fleet_Sequence { get; set; }
        public Nullable<bool> Fleet_DisplayImage { get; set; }
	}
}

