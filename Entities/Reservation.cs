using System;
using System.Collections.Generic;

namespace SEB.Entities
{
	public class Reservation
	{
	    public Reservation()
		{
			this.ReservationExtras = new List<ReservationExtra>();
		}

		public System.Guid Reservation_ID { get; set; }
		public Nullable<System.Guid> State_ID { get; set; }
		public System.DateTime Reservation_Created { get; set; }
		public Nullable<System.DateTime> Reservation_Updated { get; set; }
		public string Reservation_UserID { get; set; }
		public string Reservation_UserCode { get; set; }
		public string Reservation_DCID { get; set; }
		public string Reservation_DCName { get; set; }
		public string Reservation_Name { get; set; }
		public string Reservation_Type { get; set; }
		public string Reservation_Info { get; set; }
		public string Reservation_Country { get; set; }
		public string Reservation_Driver { get; set; }
		public string Reservation_DriverLastName { get; set; }
		public string Reservation_DriverFirstName { get; set; }
		public string Reservation_Company { get; set; }
		public string Reservation_PickupLocation { get; set; }
		public Nullable<System.DateTime> Reservation_PickupDate { get; set; }
		public string Reservation_DropoffLocation { get; set; }
		public Nullable<System.DateTime> Reservation_DropoffDate { get; set; }
		public Nullable<decimal> Reservation_Amount { get; set; }
		public string Reservation_FormState { get; set; }
		public Nullable<int> InvoiceReady { get; set; }
		public Nullable<int> EmailSent { get; set; }
		public Nullable<int> InvoiceNumber { get; set; }
		public string Reservation_Status { get; set; }
		public string Reservation_Number_Cancelled { get; set; }
		public virtual ICollection<ReservationExtra> ReservationExtras { get; set; }

        public string Driver_Address1 { get; set; }
        public string Driver_Address2 { get; set; }
        public string Driver_State { get; set; }
        public string Driver_City { get; set; }
        public string Driver_Licence { get; set; }
        public string Driver_Email { get; set; }
        public string RentalCompany { get; set; }
        public string VoucherNumber { get; set; }
        public decimal? VoucherValue { get; set; }
        public string Customer { get; set; }
    }
}

