using System;
using System.Collections.Generic;

namespace SEB.Entities
{
	public class Person
	{
		public string User_ID { get; set; }
		public System.DateTime Person_Created { get; set; }
		public string Person_Source { get; set; }
		public string Person_Action { get; set; }
		public string Person_ExternalID { get; set; }
		public string Person_Wizard_Old { get; set; }
		public string Person_Wizard_New { get; set; }
		public string Person_AWD_Old { get; set; }
		public string Person_AWD_New { get; set; }
		public string Person_FirstName_Old { get; set; }
		public string Person_FirstName_New { get; set; }
		public string Person_LastName_Old { get; set; }
		public string Person_LastName_New { get; set; }
		public Nullable<System.DateTime> Person_BirthDate_Old { get; set; }
		public Nullable<System.DateTime> Person_BirthDate_New { get; set; }
		public Nullable<bool> Person_Gender_Old { get; set; }
		public Nullable<bool> Person_Gender_New { get; set; }
		public string Person_Street_Old { get; set; }
		public string Person_Street_New { get; set; }
		public string Person_PostalCode_Old { get; set; }
		public string Person_PostalCode_New { get; set; }
		public string Person_PostalName_Old { get; set; }
		public string Person_PostalName_New { get; set; }
		public string Person_Phone_Old { get; set; }
		public string Person_Phone_New { get; set; }
		public string Person_Email_Old { get; set; }
		public string Person_Email_New { get; set; }
	}
}

