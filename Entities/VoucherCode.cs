﻿

namespace SEB.Entities
{
    public class VoucherCode
    {
        [System.ComponentModel.DataAnnotations.Key]
        public int id {get; set;}
        public int LastCodeUsed { get; set; }
        
    }
}