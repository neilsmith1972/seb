using System;
using System.Collections.Generic;

namespace SEB.Entities
{
	public class TravelAgent
	{
		public System.Guid TravelAgent_ID { get; set; }
		public string TravelAgent_Code { get; set; }
		public string TravelAgent_Name { get; set; }
	}
}

