﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace SEB.Entities
{
    public class Airline
    {
        [Key]
        public int id { get; set; }
        public string AirlineCode { get; set; }
        public string AirlineName { get; set; }
    }
}