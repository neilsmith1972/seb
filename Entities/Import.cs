using System;
using System.Collections.Generic;

namespace SEB.Entities
{
	public class Import
	{
		public int Import_ID { get; set; }
		public string Import_Table { get; set; }
		public System.DateTime Import_Started { get; set; }
		public Nullable<System.DateTime> Import_Ended { get; set; }
		public Nullable<System.Guid> Import_Hash { get; set; }
	}
}

