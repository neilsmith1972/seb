using System;
using System.Collections.Generic;

namespace SEB.Entities
{
	public class Currency
	{
		public string Currency_Code { get; set; }
		public string Currency_Name { get; set; }
	}
}

