using System;
using System.Collections.Generic;

namespace SEB.Entities
{
	public class State
	{
		public System.Guid State_ID { get; set; }
		public System.DateTime State_Created { get; set; }
		public Nullable<System.DateTime> State_Updated { get; set; }
		public string State_UserID { get; set; }
		public string State_UserCode { get; set; }
		public string State_Name { get; set; }
		public byte[] State_Data { get; set; }
	}
}

