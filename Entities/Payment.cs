﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SEB.Entities
{
    public class Payment
    {
        public int id { get; set; }
        public string ReservationUniqueId { get; set; }
        public decimal Amount { get; set; }
        public string PaymentStatus { get; set; }
        public string OGonePaymentId { get; set; }
        public DateTime PaymentDate { get; set; }
        public string Comments { get; set; }
        public string UserReference { get; set; }
    }
}