using System;
using System.Collections.Generic;

namespace SEB.Entities
{
	public class Hour
	{
		public System.Guid Hour_ID { get; set; }
        public string Language_ID { get; set; }
		public Nullable<int> Import_ID { get; set; }
		public System.DateTime Hour_Created { get; set; }
		public string Location_Code { get; set; }
		public string Hour_Name { get; set; }
		public Nullable<byte> Hour_Day { get; set; }
		public Nullable<int> Hour_DateFrom { get; set; }
		public Nullable<int> Hour_DateTo { get; set; }
		public short Hour_HourFrom { get; set; }
		public short Hour_HourTo { get; set; }
	}
}

