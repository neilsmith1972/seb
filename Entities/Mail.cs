using System;
using System.Collections.Generic;

namespace SEB.Entities
{
	public class Mail
	{
		public System.Guid Mail_ID { get; set; }
		public Nullable<System.DateTime> Mail_Added { get; set; }
		public Nullable<System.DateTime> Mail_Sent { get; set; }
		public string Mail_ToAddress { get; set; }
		public string Mail_ToDisplay { get; set; }
		public string Mail_BCCs { get; set; }
		public string Mail_From { get; set; }
		public string Mail_ReplyTo { get; set; }
		public string Mail_Subject { get; set; }
		public string Mail_BodyText { get; set; }
		public string Mail_BodyHtml { get; set; }
	}
}

