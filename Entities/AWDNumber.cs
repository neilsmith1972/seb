﻿

namespace SEB.Entities
{
    public class AwdNumber
    {
        public int id { get; set; }
        public string Region { get; set; }
        public string Number { get; set; }
        public string CardType { get; set; }
        public string BudgetNumber { get; set; }
    }
}