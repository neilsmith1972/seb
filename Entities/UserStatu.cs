using System;
using System.Collections.Generic;

namespace SEB.Entities
{
	public class UserStatu
	{
	    public UserStatu()
		{
			this.Users = new List<User>();
		}

		public int id { get; set; }
		public string StatusText { get; set; }
		public virtual ICollection<User> Users { get; set; }
	}
}

