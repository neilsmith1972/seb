using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace SEB.Entities
{
	public class User
	{
	    public int id { get; set; }
        [Required]
        [MinLength(8,ErrorMessage="At least 8 characters.")]
		public string Pw { get; set; }
        [Required(ErrorMessageResourceType=typeof(Resources.Copy),ErrorMessageResourceName= "Validations_FirstName")]
		public string FirstName { get; set; }
        [Required(ErrorMessageResourceType = typeof(Resources.Copy), ErrorMessageResourceName = "Validations_LastName")]
		public string LastName { get; set; }
        [Required(ErrorMessageResourceType = typeof(Resources.Copy), ErrorMessageResourceName = "Validations_FirstLine")]
		public string Address1 { get; set; }
		public string Address2 { get; set; }
        [Required(ErrorMessageResourceType = typeof(Resources.Copy), ErrorMessageResourceName = "Validations_City")]
		public string City { get; set; }
		public string State { get; set; }
        [Required(ErrorMessageResourceType = typeof(Resources.Copy), ErrorMessageResourceName = "Validations_PostCode")]
		public string PostCode { get; set; }
		public int Country { get; set; }
        [Required(ErrorMessageResourceType = typeof(Resources.Copy), ErrorMessageResourceName = "Validations_DriversLicence")]
		public string DriversLicense { get; set; }
        [Required(ErrorMessageResourceType = typeof(Resources.Copy), ErrorMessageResourceName = "Validations_DOB")]
		public DateTime DateOfBirth { get; set; }
		public string MobileNumber { get; set; }
        [Required(ErrorMessageResourceType = typeof(Resources.Copy), ErrorMessageResourceName = "Validations_Email")]
        [RegularExpression(@"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$", ErrorMessage = "Please enter a valid e-mail address")]
        [Key]
		public string Email { get; set; }
		public System.DateTime DateCreated { get; set; }
		public System.DateTime DateUpdated { get; set; }
		public int UserStatus { get; set; }
        public string CardNumber { get; set; }
        public string CardExpiryMonth { get; set; }
        public string CardExpiryYear { get; set; }
        public string CardCVC { get; set; }
	}
}

