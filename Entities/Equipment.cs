using System;
using System.Collections.Generic;

namespace SEB.Entities
{
	public class Equipment
	{
		public System.Guid Equipment_ID { get; set; }
        public string Language_ID { get; set; }
		public string Equipment_Code { get; set; }
		public Nullable<int> Import_ID { get; set; }
		public System.DateTime Equipment_Created { get; set; }
		public string Location_Code { get; set; }
		public string Region_Code { get; set; }
		public string Region_Name { get; set; }
		public string Equipment_Description { get; set; }
		public string Equipment_Currency { get; set; }
		public decimal Equipment_Price { get; set; }
		public decimal Equipment_PriceMax { get; set; }
		public string Equipment_ChargePer { get; set; }
		public string Equipment_Availability { get; set; }
		public string Equipment_AdditionalInfo { get; set; }
	}
}

