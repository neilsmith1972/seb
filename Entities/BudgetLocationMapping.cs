﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace SEB.Entities
{
    public class BudgetLocationMapping
    {
        [Key]
        public string AvisLocation { get; set; }
        public string BudgetLocation { get; set; }
    }
}