﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SEB.Entities
{
    public class ErrorTranslation
    {

        public int id { get; set; }
        public int ErrorCode { get; set; }
        public string GB { get; set; }
        public string NO { get; set; }
        public string SE { get; set; }
        public string DK { get; set; }
        public string FI { get; set; }


    }
}