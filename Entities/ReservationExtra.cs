using System;
using System.Collections.Generic;

namespace SEB.Entities
{
	public class ReservationExtra
	{
		public int id { get; set; }
        public Guid ReservationId { get; set; }
		public string ExtraId { get; set; }
		public int Quantity { get; set; }
		public virtual Reservation Reservation { get; set; }
	}
}

