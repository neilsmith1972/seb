using System;
using System.Collections.Generic;

namespace SEB.Entities
{
	public class Request
	{
		public System.Guid Request_ID { get; set; }
		public System.DateTime Request_Requested { get; set; }
		public Nullable<System.DateTime> Request_Responded { get; set; }
		public string Request_Command { get; set; }
		public string Request_Status { get; set; }
		public string Request_Request { get; set; }
		public string Request_Response { get; set; }
		public string Request_ResponseInfo { get; set; }
		public string Request_ResponseError { get; set; }
	}
}

