using System;
using System.Data.Entity.ModelConfiguration;
using System.Data.Common;
using System.Data.Entity;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using SEB.Entities;

namespace SEB.Mapping
{
	public class AirlineMap : EntityTypeConfiguration<Airline>
	{
        public AirlineMap()
		{
			// Primary Key
			this.HasKey(t => t.id);

				
			this.Property(t => t.AirlineCode)
				.IsRequired()
				.HasMaxLength(50);
				
			
				
			// Table & Column Mappings
			this.ToTable("Airlines");
			this.Property(t => t.id).HasColumnName("id");
            this.Property(t => t.AirlineCode).HasColumnName("AirlineCode");
            this.Property(t => t.AirlineName).HasColumnName("AirlineName");
		}
	}
}

