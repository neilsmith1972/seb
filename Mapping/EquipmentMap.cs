using System;
using System.Data.Entity.ModelConfiguration;
using System.Data.Common;
using System.Data.Entity;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using SEB.Entities;

namespace SEB.Mapping
{
	public class EquipmentMap : EntityTypeConfiguration<Equipment>
	{
		public EquipmentMap()
		{
			// Primary Key
			this.HasKey(t => t.Equipment_ID);

			// Properties
			this.Property(t => t.Equipment_Code)
				.HasMaxLength(8);
				
			this.Property(t => t.Location_Code)
				.IsRequired()
				.HasMaxLength(8);
				
			this.Property(t => t.Region_Code)
				.IsRequired()
				.HasMaxLength(8);
				
			this.Property(t => t.Region_Name)
				.IsRequired()
				.HasMaxLength(256);
				
			this.Property(t => t.Equipment_Description)
				.IsRequired()
				.HasMaxLength(512);
				
			this.Property(t => t.Equipment_Currency)
				.IsRequired()
				.HasMaxLength(32);
				
			this.Property(t => t.Equipment_ChargePer)
				.IsRequired()
				.HasMaxLength(8);
				
			this.Property(t => t.Equipment_Availability)
				.IsRequired()
				.HasMaxLength(16);
				
			this.Property(t => t.Equipment_AdditionalInfo)
				.IsRequired();
				
			// Table & Column Mappings
            this.ToTable("Equipment_Ex");
			this.Property(t => t.Equipment_ID).HasColumnName("Equipment_ID");
            this.Property(t => t.Language_ID).HasColumnName("Language_ID");
			this.Property(t => t.Equipment_Code).HasColumnName("Equipment_Code");
			this.Property(t => t.Import_ID).HasColumnName("Import_ID");
			this.Property(t => t.Equipment_Created).HasColumnName("Equipment_Created");
			this.Property(t => t.Location_Code).HasColumnName("Location_Code");
			this.Property(t => t.Region_Code).HasColumnName("Region_Code");
			this.Property(t => t.Region_Name).HasColumnName("Region_Name");
			this.Property(t => t.Equipment_Description).HasColumnName("Equipment_Description");
			this.Property(t => t.Equipment_Currency).HasColumnName("Equipment_Currency");
			this.Property(t => t.Equipment_Price).HasColumnName("Equipment_Price");
			this.Property(t => t.Equipment_PriceMax).HasColumnName("Equipment_PriceMax");
			this.Property(t => t.Equipment_ChargePer).HasColumnName("Equipment_ChargePer");
			this.Property(t => t.Equipment_Availability).HasColumnName("Equipment_Availability");
			this.Property(t => t.Equipment_AdditionalInfo).HasColumnName("Equipment_AdditionalInfo");
		}
	}
}

