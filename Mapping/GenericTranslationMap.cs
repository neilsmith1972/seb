using System;
using System.Data.Entity.ModelConfiguration;
using System.Data.Common;
using System.Data.Entity;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using SEB.Entities;

namespace SEB.Mapping
{
	public class GenericTranslationMap : EntityTypeConfiguration<GenericTranslation>
	{
        public GenericTranslationMap()
		{
			// Primary Key
			this.HasKey(t => t.id);

			// Properties
			this.Property(t => t.id)
				.HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
				
						
				
			// Table & Column Mappings
			this.ToTable("GenericTranslations");
			this.Property(t => t.id).HasColumnName("id");
            this.Property(t => t.CountryCode).HasColumnName("CountryCode");
            this.Property(t => t.Source).HasColumnName("Source");
            this.Property(t => t.Translation).HasColumnName("Translation");
        }
	}
}

