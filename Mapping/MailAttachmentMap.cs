using System;
using System.Data.Entity.ModelConfiguration;
using System.Data.Common;
using System.Data.Entity;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using SEB.Entities;

namespace SEB.Mapping
{
	public class MailAttachmentMap : EntityTypeConfiguration<MailAttachment>
	{
		public MailAttachmentMap()
		{
			// Primary Key
			this.HasKey(t => new { t.Mail_ID, t.MailAttachment_Name });

			// Properties
			this.Property(t => t.MailAttachment_Name)
				.IsRequired()
				.HasMaxLength(128);
				
			this.Property(t => t.MailAttachment_Type)
				.HasMaxLength(32);
				
			// Table & Column Mappings
			this.ToTable("MailAttachment");
			this.Property(t => t.Mail_ID).HasColumnName("Mail_ID");
			this.Property(t => t.MailAttachment_Name).HasColumnName("MailAttachment_Name");
			this.Property(t => t.MailAttachment_Type).HasColumnName("MailAttachment_Type");
			this.Property(t => t.MailAttachment_Data).HasColumnName("MailAttachment_Data");
		}
	}
}

