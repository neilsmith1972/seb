using System;
using System.Data.Entity.ModelConfiguration;
using System.Data.Common;
using System.Data.Entity;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using SEB.Entities;

namespace SEB.Mapping
{
	public class UserStatuMap : EntityTypeConfiguration<UserStatu>
	{
		public UserStatuMap()
		{
			// Primary Key
			this.HasKey(t => t.id);

			// Properties
			this.Property(t => t.StatusText)
				.IsRequired()
				.HasMaxLength(50);
				
			// Table & Column Mappings
			this.ToTable("UserStatus");
			this.Property(t => t.id).HasColumnName("id");
			this.Property(t => t.StatusText).HasColumnName("StatusText");
		}
	}
}

