using System;
using System.Data.Entity.ModelConfiguration;
using System.Data.Common;
using System.Data.Entity;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using SEB.Entities;

namespace SEB.Mapping
{
	public class CurrencyMap : EntityTypeConfiguration<Currency>
	{
		public CurrencyMap()
		{
			// Primary Key
			this.HasKey(t => t.Currency_Code);

			// Properties
			this.Property(t => t.Currency_Code)
				.IsRequired()
				.IsFixedLength()
				.HasMaxLength(3);
				
			this.Property(t => t.Currency_Name)
				.IsRequired()
				.HasMaxLength(32);
				
			// Table & Column Mappings
			this.ToTable("Currency");
			this.Property(t => t.Currency_Code).HasColumnName("Currency_Code");
			this.Property(t => t.Currency_Name).HasColumnName("Currency_Name");
		}
	}
}

