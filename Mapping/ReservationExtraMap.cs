using System;
using System.Data.Entity.ModelConfiguration;
using System.Data.Common;
using System.Data.Entity;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using SEB.Entities;

namespace SEB.Mapping
{
	public class ReservationExtraMap : EntityTypeConfiguration<ReservationExtra>
	{
		public ReservationExtraMap()
		{
			// Primary Key
			this.HasKey(t => t.id);

			// Properties
			
				
			this.Property(t => t.ExtraId)
				.IsRequired();
				
			// Table & Column Mappings
			this.ToTable("ReservationExtras");
			this.Property(t => t.id).HasColumnName("id");
			this.Property(t => t.ReservationId).HasColumnName("ReservationId");
			this.Property(t => t.ExtraId).HasColumnName("ExtraId");
			this.Property(t => t.Quantity).HasColumnName("Quantity");

			// Relationships
			this.HasRequired(t => t.Reservation)
				.WithMany(t => t.ReservationExtras)
				.HasForeignKey(d => d.ReservationId);
				
		}
	}
}

