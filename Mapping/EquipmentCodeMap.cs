using System;
using System.Data.Entity.ModelConfiguration;
using System.Data.Common;
using System.Data.Entity;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using SEB.Entities;

namespace SEB.Mapping
{
	public class EquipmentCodeMap : EntityTypeConfiguration<EquipmentCode>
	{
		public EquipmentCodeMap()
		{
			// Primary Key
			this.HasKey(t => t.Equipment_Description);

			// Properties
			this.Property(t => t.Equipment_Description)
				.IsRequired()
				.HasMaxLength(512);
				
			this.Property(t => t.Equipment_Code)
				.HasMaxLength(8);
				
			// Table & Column Mappings
			this.ToTable("EquipmentCode");
			this.Property(t => t.Equipment_Description).HasColumnName("Equipment_Description");
			this.Property(t => t.Equipment_Code).HasColumnName("Equipment_Code");
			this.Property(t => t.Equipment_Number).HasColumnName("Equipment_Number");
		}
	}
}

