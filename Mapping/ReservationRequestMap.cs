using System;
using System.Data.Entity.ModelConfiguration;
using System.Data.Common;
using System.Data.Entity;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using SEB.Entities;

namespace SEB.Mapping
{
	public class ReservationRequestMap : EntityTypeConfiguration<ReservationRequest>
	{
		public ReservationRequestMap()
		{
			// Primary Key
			this.HasKey(t => new { t.Reservation_ID, t.Request_ID });

			// Properties
			// Table & Column Mappings
			this.ToTable("ReservationRequest");
			this.Property(t => t.Reservation_ID).HasColumnName("Reservation_ID");
			this.Property(t => t.Request_ID).HasColumnName("Request_ID");
			this.Property(t => t.ReservationRequest_Current).HasColumnName("ReservationRequest_Current");
		}
	}
}

