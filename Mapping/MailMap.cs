using System;
using System.Data.Entity.ModelConfiguration;
using System.Data.Common;
using System.Data.Entity;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using SEB.Entities;

namespace SEB.Mapping
{
	public class MailMap : EntityTypeConfiguration<Mail>
	{
		public MailMap()
		{
			// Primary Key
			this.HasKey(t => t.Mail_ID);

			// Properties
			this.Property(t => t.Mail_ToAddress)
				.IsRequired()
				.HasMaxLength(128);
				
			this.Property(t => t.Mail_ToDisplay)
				.HasMaxLength(128);
				
			this.Property(t => t.Mail_BCCs)
				.HasMaxLength(512);
				
			this.Property(t => t.Mail_From)
				.HasMaxLength(128);
				
			this.Property(t => t.Mail_ReplyTo)
				.HasMaxLength(128);
				
			this.Property(t => t.Mail_Subject)
				.IsRequired()
				.HasMaxLength(128);
				
			// Table & Column Mappings
			this.ToTable("Mail");
			this.Property(t => t.Mail_ID).HasColumnName("Mail_ID");
			this.Property(t => t.Mail_Added).HasColumnName("Mail_Added");
			this.Property(t => t.Mail_Sent).HasColumnName("Mail_Sent");
			this.Property(t => t.Mail_ToAddress).HasColumnName("Mail_ToAddress");
			this.Property(t => t.Mail_ToDisplay).HasColumnName("Mail_ToDisplay");
			this.Property(t => t.Mail_BCCs).HasColumnName("Mail_BCCs");
			this.Property(t => t.Mail_From).HasColumnName("Mail_From");
			this.Property(t => t.Mail_ReplyTo).HasColumnName("Mail_ReplyTo");
			this.Property(t => t.Mail_Subject).HasColumnName("Mail_Subject");
			this.Property(t => t.Mail_BodyText).HasColumnName("Mail_BodyText");
			this.Property(t => t.Mail_BodyHtml).HasColumnName("Mail_BodyHtml");
		}
	}
}

