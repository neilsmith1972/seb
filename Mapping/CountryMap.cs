using System;
using System.Data.Entity.ModelConfiguration;
using System.Data.Common;
using System.Data.Entity;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using SEB.Entities;

namespace SEB.Mapping
{
	public class CountryMap : EntityTypeConfiguration<Country>
	{
        public CountryMap()
		{
			// Primary Key
			this.HasKey(t => t.id);

				
			this.Property(t => t.CountryText)
				.IsRequired()
				.HasMaxLength(50);
				
			
				
			// Table & Column Mappings
			this.ToTable("Country");
			this.Property(t => t.id).HasColumnName("id");
            this.Property(t => t.CountryText).HasColumnName("CountryText");
            this.Property(t => t.SortOrder).HasColumnName("SortOrder");
		}
	}
}

