using System;
using System.Data.Entity.ModelConfiguration;
using System.Data.Common;
using System.Data.Entity;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using SEB.Entities;

namespace SEB.Mapping
{
	public class PersonMap : EntityTypeConfiguration<Person>
	{
		public PersonMap()
		{
			// Primary Key
			this.HasKey(t => new { t.User_ID, t.Person_Created, t.Person_Source });

			// Properties
			this.Property(t => t.User_ID)
				.IsRequired()
				.HasMaxLength(32);
				
			this.Property(t => t.Person_Source)
				.IsRequired()
				.HasMaxLength(32);
				
			this.Property(t => t.Person_Action)
				.HasMaxLength(32);
				
			this.Property(t => t.Person_ExternalID)
				.HasMaxLength(32);
				
			this.Property(t => t.Person_Wizard_Old)
				.HasMaxLength(8);
				
			this.Property(t => t.Person_Wizard_New)
				.HasMaxLength(8);
				
			this.Property(t => t.Person_AWD_Old)
				.HasMaxLength(8);
				
			this.Property(t => t.Person_AWD_New)
				.HasMaxLength(8);
				
			this.Property(t => t.Person_FirstName_Old)
				.HasMaxLength(128);
				
			this.Property(t => t.Person_FirstName_New)
				.HasMaxLength(128);
				
			this.Property(t => t.Person_LastName_Old)
				.HasMaxLength(128);
				
			this.Property(t => t.Person_LastName_New)
				.HasMaxLength(128);
				
			this.Property(t => t.Person_Street_Old)
				.HasMaxLength(128);
				
			this.Property(t => t.Person_Street_New)
				.HasMaxLength(128);
				
			this.Property(t => t.Person_PostalCode_Old)
				.HasMaxLength(8);
				
			this.Property(t => t.Person_PostalCode_New)
				.HasMaxLength(8);
				
			this.Property(t => t.Person_PostalName_Old)
				.HasMaxLength(128);
				
			this.Property(t => t.Person_PostalName_New)
				.HasMaxLength(128);
				
			this.Property(t => t.Person_Phone_Old)
				.HasMaxLength(32);
				
			this.Property(t => t.Person_Phone_New)
				.HasMaxLength(32);
				
			this.Property(t => t.Person_Email_Old)
				.HasMaxLength(128);
				
			this.Property(t => t.Person_Email_New)
				.HasMaxLength(128);
				
			// Table & Column Mappings
			this.ToTable("Person");
			this.Property(t => t.User_ID).HasColumnName("User_ID");
			this.Property(t => t.Person_Created).HasColumnName("Person_Created");
			this.Property(t => t.Person_Source).HasColumnName("Person_Source");
			this.Property(t => t.Person_Action).HasColumnName("Person_Action");
			this.Property(t => t.Person_ExternalID).HasColumnName("Person_ExternalID");
			this.Property(t => t.Person_Wizard_Old).HasColumnName("Person_Wizard_Old");
			this.Property(t => t.Person_Wizard_New).HasColumnName("Person_Wizard_New");
			this.Property(t => t.Person_AWD_Old).HasColumnName("Person_AWD_Old");
			this.Property(t => t.Person_AWD_New).HasColumnName("Person_AWD_New");
			this.Property(t => t.Person_FirstName_Old).HasColumnName("Person_FirstName_Old");
			this.Property(t => t.Person_FirstName_New).HasColumnName("Person_FirstName_New");
			this.Property(t => t.Person_LastName_Old).HasColumnName("Person_LastName_Old");
			this.Property(t => t.Person_LastName_New).HasColumnName("Person_LastName_New");
			this.Property(t => t.Person_BirthDate_Old).HasColumnName("Person_BirthDate_Old");
			this.Property(t => t.Person_BirthDate_New).HasColumnName("Person_BirthDate_New");
			this.Property(t => t.Person_Gender_Old).HasColumnName("Person_Gender_Old");
			this.Property(t => t.Person_Gender_New).HasColumnName("Person_Gender_New");
			this.Property(t => t.Person_Street_Old).HasColumnName("Person_Street_Old");
			this.Property(t => t.Person_Street_New).HasColumnName("Person_Street_New");
			this.Property(t => t.Person_PostalCode_Old).HasColumnName("Person_PostalCode_Old");
			this.Property(t => t.Person_PostalCode_New).HasColumnName("Person_PostalCode_New");
			this.Property(t => t.Person_PostalName_Old).HasColumnName("Person_PostalName_Old");
			this.Property(t => t.Person_PostalName_New).HasColumnName("Person_PostalName_New");
			this.Property(t => t.Person_Phone_Old).HasColumnName("Person_Phone_Old");
			this.Property(t => t.Person_Phone_New).HasColumnName("Person_Phone_New");
			this.Property(t => t.Person_Email_Old).HasColumnName("Person_Email_Old");
			this.Property(t => t.Person_Email_New).HasColumnName("Person_Email_New");
		}
	}
}

