using System;
using System.Data.Entity.ModelConfiguration;
using System.Data.Common;
using System.Data.Entity;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using SEB.Entities;

namespace SEB.Mapping
{
	public class UserMap : EntityTypeConfiguration<User>
	{
		public UserMap()
		{
			// Primary Key
			this.HasKey(t => t.Email);

			// Properties
			this.Property(t => t.id)
				.HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
				
			this.Property(t => t.Pw)
				.IsRequired()
				.HasMaxLength(20);
				
			this.Property(t => t.FirstName)
				.IsRequired()
				.HasMaxLength(50);
				
			this.Property(t => t.LastName)
				.IsRequired()
				.HasMaxLength(50);
				
			this.Property(t => t.Address1)
				.IsRequired()
				.HasMaxLength(50);
				
			
			this.Property(t => t.City)
				.IsRequired()
				.HasMaxLength(50);
				
			this.Property(t => t.State)
				.HasMaxLength(50);
				
			this.Property(t => t.PostCode)
				.IsRequired()
				.HasMaxLength(10);
				
			this.Property(t => t.DriversLicense)
				.IsRequired()
				.HasMaxLength(50);
				
			this.Property(t => t.MobileNumber)
				.HasMaxLength(50);
				
			this.Property(t => t.Email)
				.IsRequired()
				.HasMaxLength(100);
				
			// Table & Column Mappings
			this.ToTable("Users");
			this.Property(t => t.id).HasColumnName("id");
			this.Property(t => t.Pw).HasColumnName("Pw");
			this.Property(t => t.FirstName).HasColumnName("FirstName");
			this.Property(t => t.LastName).HasColumnName("LastName");
			this.Property(t => t.Address1).HasColumnName("Address1");
			this.Property(t => t.Address2).HasColumnName("Address2");
			this.Property(t => t.City).HasColumnName("City");
			this.Property(t => t.State).HasColumnName("State");
			this.Property(t => t.PostCode).HasColumnName("PostCode");
			this.Property(t => t.Country).HasColumnName("Country");
			this.Property(t => t.DriversLicense).HasColumnName("DriversLicense");
			this.Property(t => t.DateOfBirth).HasColumnName("DateOfBirth");
			this.Property(t => t.MobileNumber).HasColumnName("MobileNumber");
			this.Property(t => t.Email).HasColumnName("Email");
			this.Property(t => t.DateCreated).HasColumnName("DateCreated");
			this.Property(t => t.DateUpdated).HasColumnName("DateUpdated");
			this.Property(t => t.UserStatus).HasColumnName("UserStatus");

            this.Property(t => t.CardNumber).HasColumnName("CardNumber");
            this.Property(t => t.CardExpiryMonth).HasColumnName("CardExpiryMonth");
            this.Property(t => t.CardExpiryYear).HasColumnName("CardExpiryYear");
            this.Property(t => t.CardCVC).HasColumnName("CardCVC");
        }
	}
}

