using System;
using System.Data.Entity.ModelConfiguration;
using System.Data.Common;
using System.Data.Entity;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using SEB.Entities;

namespace SEB.Mapping
{
	public class VoucherCodeMap : EntityTypeConfiguration<VoucherCode>
	{
        public VoucherCodeMap()
		{
			// Primary Key
			this.HasKey(t => t.id);

		
			// Table & Column Mappings
			this.ToTable("VoucherCode");
            this.Property(t => t.id).HasColumnName("id");
            this.Property(t => t.LastCodeUsed).HasColumnName("LastCodeUsed");
		}
	}
}

