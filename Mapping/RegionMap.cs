using System;
using System.Data.Entity.ModelConfiguration;
using System.Data.Common;
using System.Data.Entity;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using SEB.Entities;

namespace SEB.Mapping
{
	public class RegionMap : EntityTypeConfiguration<Region>
	{
		public RegionMap()
		{
			// Primary Key
			this.HasKey(t => t.ID);

			// Properties
			this.Property(t => t.Region_Code)
				.HasMaxLength(4);
				
			this.Property(t => t.Region_Name)
				.HasMaxLength(128);
				
			this.Property(t => t.Country_Code)
				.HasMaxLength(4);
				
			this.Property(t => t.Country_Name)
				.HasMaxLength(128);
				
			this.Property(t => t.Continent_Code)
				.HasMaxLength(4);
				
			this.Property(t => t.Continent_Name)
				.HasMaxLength(128);
				
			// Table & Column Mappings
			this.ToTable("Region_Ex");
			this.Property(t => t.ID).HasColumnName("ID");
			this.Property(t => t.Import_ID).HasColumnName("Import_ID");
			this.Property(t => t.Region_Created).HasColumnName("Region_Created");
			this.Property(t => t.Region_Priority).HasColumnName("Region_Priority");
			this.Property(t => t.Region_Code).HasColumnName("Region_Code");
			this.Property(t => t.Region_Name).HasColumnName("Region_Name");
			this.Property(t => t.Country_Code).HasColumnName("Country_Code");
			this.Property(t => t.Country_Name).HasColumnName("Country_Name");
			this.Property(t => t.Continent_Code).HasColumnName("Continent_Code");
			this.Property(t => t.Continent_Name).HasColumnName("Continent_Name");
            this.Property(t => t.Language_ID).HasColumnName("Language_ID");

		}
	}
}

