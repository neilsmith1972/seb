using System;
using System.Data.Entity.ModelConfiguration;
using System.Data.Common;
using System.Data.Entity;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using SEB.Entities;

namespace SEB.Mapping
{
	public class LoginMap : EntityTypeConfiguration<Login>
	{
		public LoginMap()
		{
			// Primary Key
			this.HasKey(t => t.LoginID);

			// Properties
			this.Property(t => t.Login_RemoteAddress)
				.IsRequired()
				.HasMaxLength(15);
				
			this.Property(t => t.Login_UserAgent)
				.IsRequired()
				.HasMaxLength(128);
				
			// Table & Column Mappings
			this.ToTable("Login");
			this.Property(t => t.LoginID).HasColumnName("LoginID");
			this.Property(t => t.User_ID).HasColumnName("User_ID");
			this.Property(t => t.Login_Date).HasColumnName("Login_Date");
			this.Property(t => t.Login_RemoteAddress).HasColumnName("Login_RemoteAddress");
			this.Property(t => t.Login_UserAgent).HasColumnName("Login_UserAgent");
		}
	}
}

