using System;
using System.Data.Entity.ModelConfiguration;
using System.Data.Common;
using System.Data.Entity;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using SEB.Entities;

namespace SEB.Mapping
{
	public class WSCacheMap : EntityTypeConfiguration<WSCache>
	{
		public WSCacheMap()
		{
			// Primary Key
			this.HasKey(t => t.ID);

			// Properties
			this.Property(t => t.ID)
				.IsRequired()
				.HasMaxLength(512);
				
			this.Property(t => t.Data)
				.IsRequired();
				
			// Table & Column Mappings
			this.ToTable("WSCache");
			this.Property(t => t.ID).HasColumnName("ID");
			this.Property(t => t.Added).HasColumnName("Added");
			this.Property(t => t.Expires).HasColumnName("Expires");
			this.Property(t => t.Data).HasColumnName("Data");
		}
	}
}

