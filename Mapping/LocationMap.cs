using System;
using System.Data.Entity.ModelConfiguration;
using System.Data.Common;
using System.Data.Entity;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using SEB.Entities;

namespace SEB.Mapping
{
	public class LocationMap : EntityTypeConfiguration<Location>
	{
		public LocationMap()
		{
			// Primary Key
			this.HasKey(t => t.Location_ID);

			// Properties
			this.Property(t => t.Location_Code)
				.HasMaxLength(8);
				
			this.Property(t => t.Location_Name)
				.HasMaxLength(128);
				
			this.Property(t => t.Region_Code)
				.HasMaxLength(8);
				
			this.Property(t => t.Region_Name)
				.HasMaxLength(128);
				
			this.Property(t => t.Location_StationType)
				.HasMaxLength(16);
				
			this.Property(t => t.Location_Phone)
				.HasMaxLength(32);
				
			this.Property(t => t.Location_PhoneAlt)
				.HasMaxLength(32);
				
			this.Property(t => t.Location_Email)
				.HasMaxLength(128);
				
			this.Property(t => t.Location_Address)
				.HasMaxLength(128);
				
			this.Property(t => t.Location_City)
				.HasMaxLength(128);
				
			this.Property(t => t.Location_PostCode)
				.HasMaxLength(16);
				
			// Table & Column Mappings
			this.ToTable("Location_Ex");
			this.Property(t => t.Location_ID).HasColumnName("Location_ID");
			this.Property(t => t.Import_ID).HasColumnName("Import_ID");
            this.Property(t => t.Location_Language_ID).HasColumnName("Language_ID");
			this.Property(t => t.Location_Created).HasColumnName("Location_Created");
			this.Property(t => t.Location_Code).HasColumnName("Location_Code");
			this.Property(t => t.Location_Name).HasColumnName("Location_Name");
			this.Property(t => t.Region_Code).HasColumnName("Region_Code");
			this.Property(t => t.Region_Name).HasColumnName("Region_Name");
			this.Property(t => t.Location_StationType).HasColumnName("Location_StationType");
			this.Property(t => t.Location_Latitude).HasColumnName("Location_Latitude");
			this.Property(t => t.Location_Longitude).HasColumnName("Location_Longitude");
			this.Property(t => t.Location_Phone).HasColumnName("Location_Phone");
			this.Property(t => t.Location_PhoneAlt).HasColumnName("Location_PhoneAlt");
			this.Property(t => t.Location_GeoCodeAccurate).HasColumnName("Location_GeoCodeAccurate");
			this.Property(t => t.Location_AfterHoursReturn).HasColumnName("Location_AfterHoursReturn");
			this.Property(t => t.Location_HasSkiRack).HasColumnName("Location_HasSkiRack");
			this.Property(t => t.Location_HasSnowTyres).HasColumnName("Location_HasSnowTyres");
			this.Property(t => t.Location_HasSnowChains).HasColumnName("Location_HasSnowChains");
			this.Property(t => t.Location_HasChildSeat).HasColumnName("Location_HasChildSeat");
			this.Property(t => t.Location_HasRoofLuggage).HasColumnName("Location_HasRoofLuggage");
			this.Property(t => t.Location_HasHandControl).HasColumnName("Location_HasHandControl");
			this.Property(t => t.Location_IsGpsAvailable).HasColumnName("Location_IsGpsAvailable");
			this.Property(t => t.Location_IsAvisPreferred).HasColumnName("Location_IsAvisPreferred");
			this.Property(t => t.Location_IsShuttleServiceAvailable).HasColumnName("Location_IsShuttleServiceAvailable");
			this.Property(t => t.Location_IsRoadServiceAvailable).HasColumnName("Location_IsRoadServiceAvailable");
			this.Property(t => t.Location_Email).HasColumnName("Location_Email");
			this.Property(t => t.Location_Address).HasColumnName("Location_Address");
			this.Property(t => t.Location_City).HasColumnName("Location_City");
			this.Property(t => t.Location_PostCode).HasColumnName("Location_PostCode");
		}
	}
}

