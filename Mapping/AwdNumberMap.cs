using System;
using System.Data.Entity.ModelConfiguration;
using System.Data.Common;
using System.Data.Entity;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using SEB.Entities;

namespace SEB.Mapping
{
	public class AwdNumberMap : EntityTypeConfiguration<AwdNumber>
	{
        public AwdNumberMap()
		{
			// Primary Key
			this.HasKey(t => t.id);

			// Properties
			this.Property(t => t.id)
				.HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
				
			this.Property(t => t.Region)
				.IsRequired()
				.HasMaxLength(50);
				
			this.Property(t => t.Number)
				.IsRequired()
				.HasMaxLength(50);
				
			
				
			// Table & Column Mappings
			this.ToTable("AWDNumbers");
			this.Property(t => t.id).HasColumnName("id");
			this.Property(t => t.Region).HasColumnName("Region");
			this.Property(t => t.Number).HasColumnName("Number");
            this.Property(t => t.CardType).HasColumnName("CardType");
            this.Property(t => t.BudgetNumber).HasColumnName("BudgetNumber");
        }
	}
}

