using System;
using System.Data.Entity.ModelConfiguration;
using System.Data.Common;
using System.Data.Entity;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using SEB.Entities;

namespace SEB.Mapping
{
	public class ImportMap : EntityTypeConfiguration<Import>
	{
		public ImportMap()
		{
			// Primary Key
			this.HasKey(t => t.Import_ID);

			// Properties
			this.Property(t => t.Import_Table)
				.IsRequired()
				.HasMaxLength(128);
				
			// Table & Column Mappings
			this.ToTable("Import");
			this.Property(t => t.Import_ID).HasColumnName("Import_ID");
			this.Property(t => t.Import_Table).HasColumnName("Import_Table");
			this.Property(t => t.Import_Started).HasColumnName("Import_Started");
			this.Property(t => t.Import_Ended).HasColumnName("Import_Ended");
			this.Property(t => t.Import_Hash).HasColumnName("Import_Hash");
		}
	}
}

