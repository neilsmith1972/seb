using System;
using System.Data.Entity.ModelConfiguration;
using System.Data.Common;
using System.Data.Entity;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using SEB.Entities;

namespace SEB.Mapping
{
	public class DriverMap : EntityTypeConfiguration<Driver>
	{
		public DriverMap()
		{
			// Primary Key
			this.HasKey(t => new { t.Driver_ID, t.Driver_Created });

			// Properties
			this.Property(t => t.Driver_ExternalID)
				.HasMaxLength(128);
				
			this.Property(t => t.Driver_WizardNumber)
				.HasMaxLength(32);
				
			this.Property(t => t.Driver_AwdNumber)
				.HasMaxLength(32);
				
			this.Property(t => t.Driver_LastName)
				.HasMaxLength(128);
				
			this.Property(t => t.Driver_FirstName)
				.HasMaxLength(128);
				
			this.Property(t => t.Driver_Street)
				.HasMaxLength(128);
				
			this.Property(t => t.Driver_PostalCode)
				.HasMaxLength(8);
				
			this.Property(t => t.Driver_PostalName)
				.HasMaxLength(128);
				
			this.Property(t => t.Driver_Country)
				.HasMaxLength(2);
				
			this.Property(t => t.Driver_Gender)
				.IsFixedLength()
				.HasMaxLength(1);
				
			this.Property(t => t.Driver_Phone)
				.HasMaxLength(32);
				
			this.Property(t => t.Driver_Email)
				.HasMaxLength(128);
				
			this.Property(t => t.Driver_License)
				.HasMaxLength(32);
				
			this.Property(t => t.Driver_FrequentFlyerNo)
				.HasMaxLength(32);
				
			// Table & Column Mappings
			this.ToTable("Driver");
			this.Property(t => t.Driver_ID).HasColumnName("Driver_ID");
			this.Property(t => t.Driver_ExternalID).HasColumnName("Driver_ExternalID");
			this.Property(t => t.Driver_Created).HasColumnName("Driver_Created");
			this.Property(t => t.Driver_Updated).HasColumnName("Driver_Updated");
			this.Property(t => t.Driver_WizardNumber).HasColumnName("Driver_WizardNumber");
			this.Property(t => t.Driver_AwdNumber).HasColumnName("Driver_AwdNumber");
			this.Property(t => t.Driver_Exported).HasColumnName("Driver_Exported");
			this.Property(t => t.Driver_LastName).HasColumnName("Driver_LastName");
			this.Property(t => t.Driver_FirstName).HasColumnName("Driver_FirstName");
			this.Property(t => t.Driver_Street).HasColumnName("Driver_Street");
			this.Property(t => t.Driver_PostalCode).HasColumnName("Driver_PostalCode");
			this.Property(t => t.Driver_PostalName).HasColumnName("Driver_PostalName");
			this.Property(t => t.Driver_Country).HasColumnName("Driver_Country");
			this.Property(t => t.Driver_BirthDate).HasColumnName("Driver_BirthDate");
			this.Property(t => t.Driver_Gender).HasColumnName("Driver_Gender");
			this.Property(t => t.Driver_Phone).HasColumnName("Driver_Phone");
			this.Property(t => t.Driver_Email).HasColumnName("Driver_Email");
			this.Property(t => t.Driver_License).HasColumnName("Driver_License");
			this.Property(t => t.Driver_FrequentFlyerNo).HasColumnName("Driver_FrequentFlyerNo");
			this.Property(t => t.Driver_Remarks).HasColumnName("Driver_Remarks");
		}
	}
}

