using System;
using System.Data.Entity.ModelConfiguration;
using System.Data.Common;
using System.Data.Entity;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using SEB.Entities;

namespace SEB.Mapping
{
	public class BudgetLocationMappingMap : EntityTypeConfiguration<BudgetLocationMapping>
	{
        public BudgetLocationMappingMap()
		{
			
			
				
			// Table & Column Mappings
            this.ToTable("BudgetLocationMapping");
            this.Property(t => t.AvisLocation).HasColumnName("AvisLocation");
            this.Property(t => t.BudgetLocation).HasColumnName("BudgetLocation");
		}
	}
}

