using System;
using System.Data.Entity.ModelConfiguration;
using System.Data.Common;
using System.Data.Entity;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using SEB.Entities;

namespace SEB.Mapping
{
	public class StateMap : EntityTypeConfiguration<State>
	{
		public StateMap()
		{
			// Primary Key
			this.HasKey(t => t.State_ID);

			// Properties
			this.Property(t => t.State_UserID)
				.HasMaxLength(32);
				
			this.Property(t => t.State_UserCode)
				.HasMaxLength(32);
				
			this.Property(t => t.State_Name)
				.IsRequired()
				.HasMaxLength(128);
				
			this.Property(t => t.State_Data)
				.IsRequired();
				
			// Table & Column Mappings
			this.ToTable("State");
			this.Property(t => t.State_ID).HasColumnName("State_ID");
			this.Property(t => t.State_Created).HasColumnName("State_Created");
			this.Property(t => t.State_Updated).HasColumnName("State_Updated");
			this.Property(t => t.State_UserID).HasColumnName("State_UserID");
			this.Property(t => t.State_UserCode).HasColumnName("State_UserCode");
			this.Property(t => t.State_Name).HasColumnName("State_Name");
			this.Property(t => t.State_Data).HasColumnName("State_Data");
		}
	}
}

