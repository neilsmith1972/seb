using System;
using System.Data.Entity.ModelConfiguration;
using System.Data.Common;
using System.Data.Entity;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using SEB.Entities;

namespace SEB.Mapping
{
	public class FleetMap : EntityTypeConfiguration<Fleet>
	{
		public FleetMap()
		{
			// Primary Key
			this.HasKey(t => t.Fleet_ID);

			// Properties
			this.Property(t => t.Region_Code)
				.IsRequired()
				.HasMaxLength(16);
				
			this.Property(t => t.Fleet_CarGroup)
				.IsRequired()
				.HasMaxLength(1);
				
			this.Property(t => t.Fleet_SippCode)
				.IsRequired()
				.HasMaxLength(4);
				
			this.Property(t => t.Fleet_ModelName)
				.IsRequired()
				.HasMaxLength(128);
				
			this.Property(t => t.Fleet_ModelCode)
				.IsRequired()
				.HasMaxLength(16);
				
			this.Property(t => t.Fleet_Description)
				.IsRequired()
				.HasMaxLength(128);
				
			this.Property(t => t.Fleet_Sequence)
				.IsRequired()
				.HasMaxLength(16);
				
			// Table & Column Mappings
			this.ToTable("Fleet_Ex");
			this.Property(t => t.Fleet_ID).HasColumnName("Fleet_ID");
            this.Property(t => t.Language_ID).HasColumnName("Language_ID");
            this.Property(t => t.Import_ID).HasColumnName("Import_ID");
			this.Property(t => t.Fleet_Created).HasColumnName("Fleet_Created");
			this.Property(t => t.Region_Code).HasColumnName("Region_Code");
			this.Property(t => t.Fleet_CarGroup).HasColumnName("Fleet_CarGroup");
			this.Property(t => t.Fleet_SippCode).HasColumnName("Fleet_SippCode");
			this.Property(t => t.Fleet_ModelName).HasColumnName("Fleet_ModelName");
			this.Property(t => t.Fleet_ModelCode).HasColumnName("Fleet_ModelCode");
			this.Property(t => t.Fleet_DoorCount).HasColumnName("Fleet_DoorCount");
			this.Property(t => t.Fleet_SeatCount).HasColumnName("Fleet_SeatCount");
			this.Property(t => t.Fleet_AirCondition).HasColumnName("Fleet_AirCondition");
			this.Property(t => t.Fleet_AntiLockBreaks).HasColumnName("Fleet_AntiLockBreaks");
			this.Property(t => t.Fleet_RadioCassette).HasColumnName("Fleet_RadioCassette");
			this.Property(t => t.Fleet_DriverAirbag).HasColumnName("Fleet_DriverAirbag");
			this.Property(t => t.Fleet_PassengerAirbag).HasColumnName("Fleet_PassengerAirbag");
			this.Property(t => t.Fleet_ElectricWindow).HasColumnName("Fleet_ElectricWindow");
			this.Property(t => t.Fleet_SunRoof).HasColumnName("Fleet_SunRoof");
			this.Property(t => t.Fleet_Automatic).HasColumnName("Fleet_Automatic");
			this.Property(t => t.Fleet_LuggageCount).HasColumnName("Fleet_LuggageCount");
			this.Property(t => t.Fleet_Description).HasColumnName("Fleet_Description");
			this.Property(t => t.Fleet_PowerSteering).HasColumnName("Fleet_PowerSteering");
			this.Property(t => t.Fleet_RearSeatbelts).HasColumnName("Fleet_RearSeatbelts");
			this.Property(t => t.Fleet_FourWheelDrive).HasColumnName("Fleet_FourWheelDrive");
			this.Property(t => t.Fleet_RadioOnly).HasColumnName("Fleet_RadioOnly");
			this.Property(t => t.Fleet_Sequence).HasColumnName("Fleet_Sequence");
			this.Property(t => t.Fleet_DisplayImage).HasColumnName("Fleet_DisplayImage");
		}
	}
}

