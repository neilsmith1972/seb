using System;
using System.Data.Entity.ModelConfiguration;
using System.Data.Common;
using System.Data.Entity;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using SEB.Entities;

namespace SEB.Mapping
{
	public class ReservationHistoryMap : EntityTypeConfiguration<ReservationHistory>
	{
		public ReservationHistoryMap()
		{
			// Primary Key
			this.HasKey(t => t.Reservation_ID);

			// Properties
			this.Property(t => t.Reservation_UserID)
				.HasMaxLength(32);
				
			this.Property(t => t.Reservation_UserCode)
				.HasMaxLength(32);
				
			this.Property(t => t.Reservation_DCID)
				.HasMaxLength(32);
				
			this.Property(t => t.Reservation_DCName)
				.HasMaxLength(128);
				
			this.Property(t => t.Reservation_Name)
				.HasMaxLength(128);
				
			this.Property(t => t.Reservation_Type)
				.HasMaxLength(32);
				
			this.Property(t => t.Reservation_Info)
				.HasMaxLength(32);
				
			this.Property(t => t.Reservation_Country)
				.HasMaxLength(2);
				
			this.Property(t => t.Reservation_Driver)
				.HasMaxLength(512);
				
			this.Property(t => t.Reservation_DriverLastName)
				.HasMaxLength(128);
				
			this.Property(t => t.Reservation_DriverFirstName)
				.HasMaxLength(128);
				
			this.Property(t => t.Reservation_Company)
				.HasMaxLength(128);
				
			this.Property(t => t.Reservation_PickupLocation)
				.HasMaxLength(8);
				
			this.Property(t => t.Reservation_DropoffLocation)
				.HasMaxLength(8);
				
			// Table & Column Mappings
			this.ToTable("ReservationHistory");
			this.Property(t => t.Reservation_ID).HasColumnName("Reservation_ID");
			this.Property(t => t.State_ID).HasColumnName("State_ID");
			this.Property(t => t.Reservation_Created).HasColumnName("Reservation_Created");
			this.Property(t => t.Reservation_Updated).HasColumnName("Reservation_Updated");
			this.Property(t => t.Reservation_UserID).HasColumnName("Reservation_UserID");
			this.Property(t => t.Reservation_UserCode).HasColumnName("Reservation_UserCode");
			this.Property(t => t.Reservation_DCID).HasColumnName("Reservation_DCID");
			this.Property(t => t.Reservation_DCName).HasColumnName("Reservation_DCName");
			this.Property(t => t.Reservation_Name).HasColumnName("Reservation_Name");
			this.Property(t => t.Reservation_Type).HasColumnName("Reservation_Type");
			this.Property(t => t.Reservation_Info).HasColumnName("Reservation_Info");
			this.Property(t => t.Reservation_Country).HasColumnName("Reservation_Country");
			this.Property(t => t.Reservation_Driver).HasColumnName("Reservation_Driver");
			this.Property(t => t.Reservation_DriverLastName).HasColumnName("Reservation_DriverLastName");
			this.Property(t => t.Reservation_DriverFirstName).HasColumnName("Reservation_DriverFirstName");
			this.Property(t => t.Reservation_Company).HasColumnName("Reservation_Company");
			this.Property(t => t.Reservation_PickupLocation).HasColumnName("Reservation_PickupLocation");
			this.Property(t => t.Reservation_PickupDate).HasColumnName("Reservation_PickupDate");
			this.Property(t => t.Reservation_DropoffLocation).HasColumnName("Reservation_DropoffLocation");
			this.Property(t => t.Reservation_DropoffDate).HasColumnName("Reservation_DropoffDate");
			this.Property(t => t.Reservation_Amount).HasColumnName("Reservation_Amount");
			this.Property(t => t.Reservation_FormState).HasColumnName("Reservation_FormState");
		}
	}
}

