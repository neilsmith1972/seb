using System;
using System.Data.Entity.ModelConfiguration;
using System.Data.Common;
using System.Data.Entity;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using SEB.Entities;

namespace SEB.Mapping
{
	public class HourMap : EntityTypeConfiguration<Hour>
	{
		public HourMap()
		{
			// Primary Key
			this.HasKey(t => t.Hour_ID);

			// Properties
			this.Property(t => t.Location_Code)
				.IsRequired()
				.HasMaxLength(8);
				
			this.Property(t => t.Hour_Name)
				.IsRequired()
				.HasMaxLength(128);
				
			// Table & Column Mappings
			this.ToTable("Hour_Ex");
			this.Property(t => t.Hour_ID).HasColumnName("Hour_ID");
			this.Property(t => t.Import_ID).HasColumnName("Import_ID");
            this.Property(t => t.Language_ID).HasColumnName("Language_ID");
			this.Property(t => t.Hour_Created).HasColumnName("Hour_Created");
			this.Property(t => t.Location_Code).HasColumnName("Location_Code");
			this.Property(t => t.Hour_Name).HasColumnName("Hour_Name");
			this.Property(t => t.Hour_Day).HasColumnName("Hour_Day");
			this.Property(t => t.Hour_DateFrom).HasColumnName("Hour_DateFrom");
			this.Property(t => t.Hour_DateTo).HasColumnName("Hour_DateTo");
			this.Property(t => t.Hour_HourFrom).HasColumnName("Hour_HourFrom");
			this.Property(t => t.Hour_HourTo).HasColumnName("Hour_HourTo");
		}
	}
}

