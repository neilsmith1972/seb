using System;
using System.Data.Entity.ModelConfiguration;
using System.Data.Common;
using System.Data.Entity;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using SEB.Entities;

namespace SEB.Mapping
{
	public class ErrorTranslationMap : EntityTypeConfiguration<ErrorTranslation>
	{
        public ErrorTranslationMap()
		{
			// Primary Key
			this.HasKey(t => t.id);

			// Properties
			this.Property(t => t.id)
				.HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
				
						
				
			// Table & Column Mappings
			this.ToTable("ErrorTranslations");
			this.Property(t => t.id).HasColumnName("id");
            this.Property(t => t.ErrorCode).HasColumnName("ErrorCode");
            this.Property(t => t.DK).HasColumnName("DK");
            this.Property(t => t.FI).HasColumnName("FI");
            this.Property(t => t.GB).HasColumnName("GB");
            this.Property(t => t.SE).HasColumnName("SE");
            this.Property(t => t.NO).HasColumnName("NO");

        }
	}
}

