using System.Data.Entity.ModelConfiguration;
using SEB.Entities;

namespace SEB.Mapping
{
	public class ReservationMap : EntityTypeConfiguration<Reservation>
	{
		public ReservationMap()
		{
			// Primary Key
			this.HasKey(t => t.Reservation_ID);

			// Properties
			this.Property(t => t.Reservation_UserID)
				.HasMaxLength(32);
				
			this.Property(t => t.Reservation_UserCode)
				.HasMaxLength(32);
				
			this.Property(t => t.Reservation_DCID)
				.HasMaxLength(50);
				
			this.Property(t => t.Reservation_DCName)
				.HasMaxLength(128);
				
			this.Property(t => t.Reservation_Name)
				.HasMaxLength(128);
				
			this.Property(t => t.Reservation_Type)
				.HasMaxLength(32);
				
			this.Property(t => t.Reservation_Info)
				.HasMaxLength(32);
				
			this.Property(t => t.Reservation_Country)
				.HasMaxLength(2);
				
			this.Property(t => t.Reservation_Driver)
				.HasMaxLength(512);
				
			this.Property(t => t.Reservation_DriverLastName)
				.HasMaxLength(128);
				
			this.Property(t => t.Reservation_DriverFirstName)
				.HasMaxLength(128);
				
			this.Property(t => t.Reservation_Company)
				.HasMaxLength(128);
				
			this.Property(t => t.Reservation_PickupLocation)
				.HasMaxLength(8);
				
			this.Property(t => t.Reservation_DropoffLocation)
				.HasMaxLength(8);
				
			this.Property(t => t.Reservation_Status)
				.HasMaxLength(50);
				
			this.Property(t => t.Reservation_Number_Cancelled)
				.HasMaxLength(50);
				
			// Table & Column Mappings
			this.ToTable("Reservation");
			this.Property(t => t.Reservation_ID).HasColumnName("Reservation_ID");
			this.Property(t => t.State_ID).HasColumnName("State_ID");
			this.Property(t => t.Reservation_Created).HasColumnName("Reservation_Created");
			this.Property(t => t.Reservation_Updated).HasColumnName("Reservation_Updated");
			this.Property(t => t.Reservation_UserID).HasColumnName("Reservation_UserID");
			this.Property(t => t.Reservation_UserCode).HasColumnName("Reservation_UserCode");
			this.Property(t => t.Reservation_DCID).HasColumnName("Reservation_DCID");
			this.Property(t => t.Reservation_DCName).HasColumnName("Reservation_DCName");
			this.Property(t => t.Reservation_Name).HasColumnName("Reservation_Name");
			this.Property(t => t.Reservation_Type).HasColumnName("Reservation_Type");
			this.Property(t => t.Reservation_Info).HasColumnName("Reservation_Info");
			this.Property(t => t.Reservation_Country).HasColumnName("Reservation_Country");
			this.Property(t => t.Reservation_Driver).HasColumnName("Reservation_Driver");
			this.Property(t => t.Reservation_DriverLastName).HasColumnName("Reservation_DriverLastName");
			this.Property(t => t.Reservation_DriverFirstName).HasColumnName("Reservation_DriverFirstName");
			this.Property(t => t.Reservation_Company).HasColumnName("Reservation_Company");
			this.Property(t => t.Reservation_PickupLocation).HasColumnName("Reservation_PickupLocation");
			this.Property(t => t.Reservation_PickupDate).HasColumnName("Reservation_PickupDate");
			this.Property(t => t.Reservation_DropoffLocation).HasColumnName("Reservation_DropoffLocation");
			this.Property(t => t.Reservation_DropoffDate).HasColumnName("Reservation_DropoffDate");
			this.Property(t => t.Reservation_Amount).HasColumnName("Reservation_Amount");
			this.Property(t => t.Reservation_FormState).HasColumnName("Reservation_FormState");
			this.Property(t => t.InvoiceReady).HasColumnName("InvoiceReady");
			this.Property(t => t.EmailSent).HasColumnName("EmailSent");
			this.Property(t => t.InvoiceNumber).HasColumnName("InvoiceNumber");
			this.Property(t => t.Reservation_Status).HasColumnName("Reservation_Status");
			this.Property(t => t.Reservation_Number_Cancelled).HasColumnName("Reservation_Number_Cancelled");

            this.Property(t => t.Driver_Address1).HasColumnName("Driver_Address1");
            this.Property(t => t.Driver_Address2).HasColumnName("Driver_Address2");
            this.Property(t => t.Driver_City).HasColumnName("Driver_City");
            this.Property(t => t.Driver_State).HasColumnName("Driver_State");
            this.Property(t => t.Driver_Licence).HasColumnName("Driver_Licence");
            this.Property(t => t.Driver_Email).HasColumnName("Driver_Email");
            this.Property(t => t.RentalCompany).HasColumnName("RentalCompany");
            this.Property(t => t.VoucherNumber).HasColumnName("VoucherNumber");
            this.Property(t => t.VoucherValue).HasColumnName("VoucherValue");
            this.Property(t => t.Customer).HasColumnName("Customer");
        }
	}
}

