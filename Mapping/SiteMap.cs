using System;
using System.Data.Entity.ModelConfiguration;
using System.Data.Common;
using System.Data.Entity;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using SEB.Entities;

namespace SEB.Mapping
{
	public class SiteMap : EntityTypeConfiguration<Site>
	{
		public SiteMap()
		{
			// Primary Key
			this.HasKey(t => new { t.id, t.ShortCultureCode, t.FullCultureCode });

			// Properties
			this.Property(t => t.id)
				.HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
				
			this.Property(t => t.ShortCultureCode)
				.IsRequired()
				.IsFixedLength()
				.HasMaxLength(2);
				
			this.Property(t => t.FullCultureCode)
				.IsRequired()
				.IsFixedLength()
				.HasMaxLength(5);
				
			this.Property(t => t.BackgroundImage)
				.HasMaxLength(50);
				
			// Table & Column Mappings
			this.ToTable("Sites");
			this.Property(t => t.id).HasColumnName("id");
			this.Property(t => t.ShortCultureCode).HasColumnName("ShortCultureCode");
			this.Property(t => t.FullCultureCode).HasColumnName("FullCultureCode");
			this.Property(t => t.BackgroundImage).HasColumnName("BackgroundImage");
			this.Property(t => t.PromoHtml).HasColumnName("PromoHtml");
			this.Property(t => t.PromoCarsHtml).HasColumnName("PromoCarsHtml");

            this.Property(t => t.PaymentCID).HasColumnName("PaymentCID");
            this.Property(t => t.IATANumber).HasColumnName("IATANumber");
            this.Property(t => t.AWDNumber).HasColumnName("AWDNumber");
            this.Property(t => t.WizardNumber).HasColumnName("WizardNumber");
        }
	}
}

