using System;
using System.Data.Entity.ModelConfiguration;
using System.Data.Common;
using System.Data.Entity;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using SEB.Entities;

namespace SEB.Mapping
{
	public class TravelAgentMap : EntityTypeConfiguration<TravelAgent>
	{
		public TravelAgentMap()
		{
			// Primary Key
			this.HasKey(t => t.TravelAgent_ID);

			// Properties
			this.Property(t => t.TravelAgent_Code)
				.IsRequired()
				.HasMaxLength(8);
				
			this.Property(t => t.TravelAgent_Name)
				.IsRequired()
				.HasMaxLength(128);
				
			// Table & Column Mappings
			this.ToTable("TravelAgent");
			this.Property(t => t.TravelAgent_ID).HasColumnName("TravelAgent_ID");
			this.Property(t => t.TravelAgent_Code).HasColumnName("TravelAgent_Code");
			this.Property(t => t.TravelAgent_Name).HasColumnName("TravelAgent_Name");
		}
	}
}

