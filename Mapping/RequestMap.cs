using System;
using System.Data.Entity.ModelConfiguration;
using System.Data.Common;
using System.Data.Entity;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using SEB.Entities;

namespace SEB.Mapping
{
	public class RequestMap : EntityTypeConfiguration<Request>
	{
		public RequestMap()
		{
			// Primary Key
			this.HasKey(t => t.Request_ID);

			// Properties
			this.Property(t => t.Request_Command)
				.IsRequired()
				.HasMaxLength(128);
				
			this.Property(t => t.Request_Status)
				.HasMaxLength(32);
				
			this.Property(t => t.Request_Request)
				.IsRequired();
				
			// Table & Column Mappings
			this.ToTable("Request");
			this.Property(t => t.Request_ID).HasColumnName("Request_ID");
			this.Property(t => t.Request_Requested).HasColumnName("Request_Requested");
			this.Property(t => t.Request_Responded).HasColumnName("Request_Responded");
			this.Property(t => t.Request_Command).HasColumnName("Request_Command");
			this.Property(t => t.Request_Status).HasColumnName("Request_Status");
			this.Property(t => t.Request_Request).HasColumnName("Request_Request");
			this.Property(t => t.Request_Response).HasColumnName("Request_Response");
			this.Property(t => t.Request_ResponseInfo).HasColumnName("Request_ResponseInfo");
			this.Property(t => t.Request_ResponseError).HasColumnName("Request_ResponseError");
		}
	}
}

