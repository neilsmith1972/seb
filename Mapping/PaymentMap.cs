using System;
using System.Data.Entity.ModelConfiguration;
using System.Data.Common;
using System.Data.Entity;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using SEB.Entities;

namespace SEB.Mapping
{
	public class PaymentMap : EntityTypeConfiguration<Payment>
	{
        public PaymentMap()
		{
			// Primary Key
			this.HasKey(t => t.id);

			// Properties
			this.Property(t => t.id)
				.HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
				
			this.Property(t => t.ReservationUniqueId)
				.IsRequired()
				.HasMaxLength(50);
				
			this.Property(t => t.PaymentStatus)
				.IsRequired()
				.HasMaxLength(50);
				
			this.Property(t => t.OGonePaymentId)
				.IsRequired()
				.HasMaxLength(50);
								
			// Table & Column Mappings
			this.ToTable("Payments");
			this.Property(t => t.id).HasColumnName("id");
            this.Property(t => t.ReservationUniqueId).HasColumnName("ReservationUniqueId");
            this.Property(t => t.PaymentStatus).HasColumnName("PaymentStatus");
            this.Property(t => t.OGonePaymentId).HasColumnName("OGonePaymentId");
            this.Property(t => t.Amount).HasColumnName("Amount");
            this.Property(t => t.PaymentDate).HasColumnName("PaymentDate");
            this.Property(t => t.Comments).HasColumnName("Comments");
            this.Property(t => t.UserReference).HasColumnName("UserReference");

		}
	}
}

