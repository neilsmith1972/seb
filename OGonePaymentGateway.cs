﻿namespace SolutionTemplate.ExternalServices.Impl
{
    using System;
    using System.Collections.Generic;
    using System.Collections.Specialized;
    using System.Configuration;
    using System.Linq;
    using System.Net;
    using System.Security.Cryptography;
    using System.Text;
    using System.Web;

    using MvcPayment;

    using SolutionTemplate.ExternalServices.Dto;

    public class OGonePaymentGateway : IPaymentGateway
    {
        private string baseUrl;
        private string pspId;
        private string apiUserId;
        private string apiPassword;
        private static string apiSecretKey;
        private string paymentPortalBaseUrl;

        public OGonePaymentGateway()
        {
            // Load config settings
            try
            {

                if (bool.Parse(ConfigurationManager.AppSettings["PaymentGatewayLiveMode"]))
                {
                    baseUrl = ConfigurationManager.AppSettings["LIVEogoneBaseUrl"];
                    pspId = ConfigurationManager.AppSettings["LIVEogonePspId"];
                    apiUserId = ConfigurationManager.AppSettings["LIVEogoneApiUserId"];
                    apiPassword = ConfigurationManager.AppSettings["LIVEogoneApiUserPassword"];
                    apiSecretKey = ConfigurationManager.AppSettings["LIVEogoneApiSecretKey"];
                    paymentPortalBaseUrl = CommonHelper.GetHostUrl() + "/"; //ConfigurationManager.AppSettings["LIVEpaymentPortalBaseUrl"];
                }
                else
                {
                    baseUrl = ConfigurationManager.AppSettings["ogoneBaseUrl"];
                    pspId = ConfigurationManager.AppSettings["ogonePspId"];
                    apiUserId = ConfigurationManager.AppSettings["ogoneApiUserId"];
                    apiPassword = ConfigurationManager.AppSettings["ogoneApiUserPassword"];
                    apiSecretKey = ConfigurationManager.AppSettings["ogoneApiSecretKey"];
                    paymentPortalBaseUrl = CommonHelper.GetHostUrl() + "/"; //ConfigurationManager.AppSettings["paymentPortalBaseUrl"];
                }
            }
            catch (Exception ex)
            {
                throw new ConfigurationException("Error loading OGone configuration settings", ex);
            }
        }        

        public PaymentResponseDto CapturePayment(CapturePaymentDto paymentToCapture)
        {
            if (paymentToCapture == null)
            {
                throw new ArgumentNullException("paymentToCapture");
            }

            try
            {            
                var d = new Dictionary<string, object>();
                // var c = new NameValueCollection();            
                d.Add("PSPID", pspId); // Config
                d.Add("USERID", apiUserId); // Config
                d.Add("PSWD", apiPassword); // Config
                // d.Add("PAYID", paymentToCapture.OrderId);  // Only 1
                d.Add("PAYID", paymentToCapture.OrderId); // Only 1
                d.Add("AMOUNT", (long)paymentToCapture.Amount * 100); // * 100; amount to capture                
                d.Add("OPERATION", "SAL"); // SAL - Capture and leave open, SAS - Capture and close authorization

                string hash, requestString;
            
                CreateRequestAndHashStrings(d, out hash, out requestString);

                // Append the request hash to the request
                d.Add("SHASIGN", hash);
            
                // POST the request
                var requestUrl = string.Format("{0}maintenancedirect.asp", this.baseUrl);
                // var responseString = HttpHelper.HttpPost(requestUrl, d, GetNetworkCredentials(requestUrl));
                var responseString = HttpHelper.HttpPost(requestUrl, d);
            
                // Extract XML from the Request Body...
                var xmlResponse = new System.Xml.XmlDocument();
                xmlResponse.LoadXml(responseString);
                var navigator = xmlResponse.CreateNavigator();

                var orderId = navigator.SelectSingleNode("//ncresponse").GetAttribute("orderid", string.Empty);
                var payId = navigator.SelectSingleNode("//ncresponse").GetAttribute("PAYID", string.Empty);
                var payIdSub = navigator.SelectSingleNode("//ncresponse").GetAttribute("PAYIDSUB", string.Empty);
                var acceptance = navigator.SelectSingleNode("//ncresponse").GetAttribute("ACCEPTANCE", string.Empty);
                var status = navigator.SelectSingleNode("//ncresponse").GetAttribute("STATUS", string.Empty);
                var ncError = navigator.SelectSingleNode("//ncresponse").GetAttribute("NCERROR", string.Empty);
                var ncStatus = navigator.SelectSingleNode("//ncresponse").GetAttribute("NCSTATUS", string.Empty);
                var ncErrorPlus = navigator.SelectSingleNode("//ncresponse").GetAttribute("NCERRORPLUS", string.Empty);
                var amount = navigator.SelectSingleNode("//ncresponse").GetAttribute("amount", string.Empty);
                var currency = navigator.SelectSingleNode("//ncresponse").GetAttribute("currency", string.Empty);

                if (status != "91")
                {
                    // The transaction was not successful.
                    throw new PaymentException(
                        string.Format("Payment with PayId '{0}' and OrderId '{1}' could not be captured using OGone.", payId, orderId),
                        string.Format("Status: {0}. NCError: {1}. NcStatus: {2}. Acquirer Acceptance Code: '{3}'.", status, ncError, ncStatus, acceptance),
                        ncErrorPlus);
                }

                return new PaymentResponseDto()
                           {
                               PaymentId = payId,
                               OrderId = orderId,
                               Amount = decimal.Parse(amount),
                               Currency = currency
                           };
            }
            catch (Exception ex)
            {
                throw new InvalidOperationException("OGone Payment Gateway Error", ex);
            }
        }

        public static string GenerateHash(string input)
        {
            byte[] bytes = new UTF8Encoding().GetBytes(input);
            var sha = new SHA1CryptoServiceProvider();
            byte[] hash = sha.ComputeHash(bytes);

            var alternative = BitConverter.ToString(hash).Replace("-", "");
            StringBuilder result = new StringBuilder();
            for (int i = 0; i < 20; i++)
            {
                string temp = hash[i].ToString("X2");
                if (temp.Length == 1)
                {
                    temp = "0" + temp;
                }

                result.Append(temp);
            }

            return result.ToString();
        }

        public static bool VerifyShaOutHash(NameValueCollection parameters, string shaOutHash)
        {
            var d = new Dictionary<string, object>();
            foreach (var parameter in parameters.AllKeys)
            {
                // NOTE: Any 'CUSTOM' parameters must be added to this list, or the SHA check will fail.
                if (parameter.ToUpperInvariant() == "SHASIGN" || parameter.ToUpperInvariant() == "ID" || parameter.ToUpperInvariant() == "CARREQUESTID")
                {
                    continue;
                }

                d.Add(parameter.ToUpperInvariant(), parameters[parameter]);
            }

            string calculatedHash;
            string requestString;
            CreateRequestAndHashStrings(d, out calculatedHash, out requestString);
            if (calculatedHash == shaOutHash)
            {
                return true;
            }

            return false;
        }

        public static void CreateRequestAndHashStrings(Dictionary<string, object> d, out string requestHash, out string requestString)
        {
            var hashBuilder = new StringBuilder();
            var requestBuilder = new StringBuilder();
            foreach (var entry in d.OrderBy(x => x.Key))
            {
                // Write params in preperation of SHA1 generation 
                if (entry.Value != null)
                {
                    hashBuilder.AppendFormat("{0}={1}{2}", entry.Key, entry.Value, apiSecretKey);
                    requestBuilder.AppendFormat("{0}={1}&", entry.Key, entry.Value);
                }
            }

            var toHash = hashBuilder.ToString();
            requestString = requestBuilder.ToString();
            requestHash = GenerateHash(toHash);
        }

        private static CredentialCache GetNetworkCredentials(string url)
        {
            var credentials = new CredentialCache();
            credentials.Add(
                new Uri(url),
                "Basic",
                new NetworkCredential("UserName", "Password"));
            return credentials;
        }

        public PaymentResponseDto RefundPayment(RefundPaymentDto paymentToCapture)
        {     
       
            //throw new NotImplementedException("Not Done");
            if (paymentToCapture == null)
            {
                throw new ArgumentNullException("paymentToCapture");
            }

            try
            {
                var d = new Dictionary<string, object>();
                // var c = new NameValueCollection();            
                d.Add("PSPID", pspId); // Config
                d.Add("USERID", apiUserId); // Config
                d.Add("PSWD", apiPassword); // Config
                // d.Add("PAYID", paymentToCapture.OrderId);  // Only 1
                d.Add("PAYID", paymentToCapture.OrderId); // Only 1
                d.Add("AMOUNT", (long)paymentToCapture.Amount * 100); // * 100; amount to capture                
                d.Add("OPERATION", "RFD"); // RFD - Full or partial refund

                string hash, requestString;

                CreateRequestAndHashStrings(d, out hash, out requestString);

                // Append the request hash to the request
                d.Add("SHASIGN", hash);

                // POST the request
                var requestUrl = string.Format("{0}maintenancedirect.asp", this.baseUrl);
                // var responseString = HttpHelper.HttpPost(requestUrl, d, GetNetworkCredentials(requestUrl));
                var responseString = HttpHelper.HttpPost(requestUrl, d);

                // Extract XML from the Request Body...
                var xmlResponse = new System.Xml.XmlDocument();
                xmlResponse.LoadXml(responseString);
                var navigator = xmlResponse.CreateNavigator();

                var orderId = navigator.SelectSingleNode("//ncresponse").GetAttribute("orderid", string.Empty);
                var payId = navigator.SelectSingleNode("//ncresponse").GetAttribute("PAYID", string.Empty);
                var payIdSub = navigator.SelectSingleNode("//ncresponse").GetAttribute("PAYIDSUB", string.Empty);
                var acceptance = navigator.SelectSingleNode("//ncresponse").GetAttribute("ACCEPTANCE", string.Empty);
                var status = navigator.SelectSingleNode("//ncresponse").GetAttribute("STATUS", string.Empty);
                var ncError = navigator.SelectSingleNode("//ncresponse").GetAttribute("NCERROR", string.Empty);
                var ncStatus = navigator.SelectSingleNode("//ncresponse").GetAttribute("NCSTATUS", string.Empty);
                var ncErrorPlus = navigator.SelectSingleNode("//ncresponse").GetAttribute("NCERRORPLUS", string.Empty);
                var amount = navigator.SelectSingleNode("//ncresponse").GetAttribute("amount", string.Empty);
                var currency = navigator.SelectSingleNode("//ncresponse").GetAttribute("currency", string.Empty);

                if (status != "81" && status != "8")
                {
                    // The transaction was not successful.
                    throw new PaymentException(
                        string.Format("Payment with PayId '{0}' and OrderId '{1}' could not be captured using OGone.", payId, orderId),
                        string.Format("Status: {0}. NCError: {1}. NcStatus: {2}. Acquirer Acceptance Code: '{3}'.", status, ncError, ncStatus, acceptance),
                        ncErrorPlus);
                }

                return new PaymentResponseDto()
                {
                    PaymentId = payId,
                    OrderId = orderId,
                    Amount = decimal.Parse(amount),
                    Currency = currency
                };
            }
            catch (Exception ex)
            {
                throw new InvalidOperationException("OGone Payment Gateway Error", ex);
            }
        }

        public PaymentResponseDto CancelPayment(CancelPaymentDto paymentToCapture)
        {
            throw new NotImplementedException("Not Done");
        }

        public PaymentResponseDto ReleaseHeldFunds(CancelPaymentDto paymentToCapture)
        {
            // TODO: Release Auth'd funds, without cancelling the auth. 
            throw new NotImplementedException("Not Done");
        }

        public Dictionary<string, object> GetFormParametersForPostingPaymentAuthorization(AuthorizationDto paymentToAuthorize, string carRequestId)
        {
            // var cardAuthorization = this.Api.Get(cardAuthId);
            // var customer = this.MyAccountApi.Get(cardAuthorization.CustomerId);

            if (!string.IsNullOrWhiteSpace(carRequestId))
            {
                carRequestId = HttpUtility.UrlEncode(carRequestId);
            }

            var formParameters = new Dictionary<string, object>();

            // <form method="post" action="https://secure.ogone.com/ncol/test/orderstandard.asp" id=form1 name=form1>
            // <!-- general parameters -->
            formParameters.Add("PSPID", pspId);
            formParameters.Add("ORDERID", paymentToAuthorize.OrderId);
            formParameters.Add("AMOUNT", (long)paymentToAuthorize.Amount * 100);
            formParameters.Add("CURRENCY", paymentToAuthorize.Currency);
            formParameters.Add("LANGUAGE", "nb_NO");
            formParameters.Add("CN", paymentToAuthorize.CustomerName);
            formParameters.Add("EMAIL", paymentToAuthorize.CustomerEmail);
            formParameters.Add("OWNERZIP", null);
            formParameters.Add("OWNERADDRESS", null);
            formParameters.Add("OWNERCTY", null);
            formParameters.Add("OWNERTOWN", null);
            formParameters.Add("OWNERTELNO", null);
            // <!-- layout information: see Look and feel of the payment page -->
            formParameters.Add("TITLE", null);
            formParameters.Add("BGCOLOR", null);
            formParameters.Add("TXTCOLOR", null);
            formParameters.Add("TBLBGCOLOR", null);
            formParameters.Add("TBLTXTCOLOR", null);
            formParameters.Add("BUTTONBGCOLOR", null);
            formParameters.Add("BUTTONTXTCOLOR", null);
            formParameters.Add("LOGO", null);
            formParameters.Add("FONTTYPE", null);
            // <!-- Capture Operation -->
            formParameters.Add("OPERATION", paymentToAuthorize.InstantCapture ? "SAL" : "RES");            
            
            // throw new NotImplementedException("You will need to change these values. They define where the user is redirected after a payment/authorization attempt.");
            // <!-- post payment redirection: see Transaction feedback to the customer -->
            formParameters.Add("ACCEPTURL", this.paymentPortalBaseUrl + "CardAuthorization/AcceptAuthorization?id=" + paymentToAuthorize.RedirectUrlId + "&carRequestId=" + carRequestId);
            formParameters.Add("DECLINEURL", this.paymentPortalBaseUrl + "CardAuthorization/DeclineAuthorization?id=" + paymentToAuthorize.RedirectUrlId + "&carRequestId=" + carRequestId);
            formParameters.Add("EXCEPTIONURL", this.paymentPortalBaseUrl + "CardAuthorization/ExceptionAuthorization?id=" + paymentToAuthorize.RedirectUrlId + "&carRequestId=" + carRequestId);
            formParameters.Add("CANCELURL", this.paymentPortalBaseUrl + "CardAuthorization/CancelAuthorization?id=" + paymentToAuthorize.RedirectUrlId + "&carRequestId=" + carRequestId);
            // <input type="submit" value="" id=submit2 name=submit2>
            // <!-- check before the payment: see Security: Check before the payment -->            

            if (paymentToAuthorize.IsMobileView)
            {
                // <input type="hidden" name="TP" value="PaymentPage_1_iPhone.htm">
                formParameters.Add("TP", "PaymentPage_1_iPhone.htm");
            }            

            var notNullParameters = new Dictionary<string, object>();
            foreach (var parameter in formParameters)
            {
                if (parameter.Value != null)
                {
                    notNullParameters.Add(parameter.Key, parameter.Value);
                }
            }

            string requestHash;
            string requestString;
            CreateRequestAndHashStrings(notNullParameters, out requestHash, out requestString);
            notNullParameters.Add("SHASIGN", requestHash);
            
            // Test URL: http://localhost:1954/en-GB/CardAuthorization/PerformAuthorization?id=5177D918-24A3-4193-80FF-8E4577CEBDE6

            return notNullParameters;
        }

        public PaymentResponseDto MakePayment(MakePaymentDto paymentToCapture)
        {

           
            if (paymentToCapture == null)
            {
                throw new ArgumentNullException("paymentToCapture");
            }

            try
            {
                var d = new Dictionary<string, object>();
                // var c = new NameValueCollection();            
                d.Add("PSPID", pspId); // Config
                d.Add("USERID", apiUserId); // Config
                d.Add("PSWD", apiPassword); // Config
                // d.Add("PAYID", paymentToCapture.OrderId);  // Only 1

                if (bool.Parse(ConfigurationManager.AppSettings["SetAllPaymentsTo1Kr"]))
                {
                    d.Add("AMOUNT", (long)1 * 100); // * 100; amount to capture                
                }
                else
                {
                    d.Add("AMOUNT", (long)paymentToCapture.Amount * 100); // * 100; amount to capture                
                }
                d.Add("OPERATION", "SAL"); // RFD - Full or partial refund
                d.Add("CURRENCY", "SEK");
                d.Add("ORDERID", paymentToCapture.OrderId);
                d.Add("CARDNO", paymentToCapture.CardNumber);
                d.Add("ED", paymentToCapture.ExpiryDate);
                d.Add("CVC", paymentToCapture.CVC);

                string hash, requestString;

                CreateRequestAndHashStrings(d, out hash, out requestString);

                // Append the request hash to the request
                d.Add("SHASIGN", hash);

                // POST the request
                var requestUrl = string.Format("{0}orderdirect.asp", this.baseUrl);
                // var responseString = HttpHelper.HttpPost(requestUrl, d, GetNetworkCredentials(requestUrl));
                var responseString = HttpHelper.HttpPost(requestUrl, d);

                // Extract XML from the Request Body...
                var xmlResponse = new System.Xml.XmlDocument();
                xmlResponse.LoadXml(responseString);
                var navigator = xmlResponse.CreateNavigator();

                var orderId = navigator.SelectSingleNode("//ncresponse").GetAttribute("orderID", string.Empty);
                var payId = navigator.SelectSingleNode("//ncresponse").GetAttribute("PAYID", string.Empty);
                var payIdSub = navigator.SelectSingleNode("//ncresponse").GetAttribute("PAYIDSUB", string.Empty);
                var acceptance = navigator.SelectSingleNode("//ncresponse").GetAttribute("ACCEPTANCE", string.Empty);
                var status = navigator.SelectSingleNode("//ncresponse").GetAttribute("STATUS", string.Empty);
                var ncError = navigator.SelectSingleNode("//ncresponse").GetAttribute("NCERROR", string.Empty);
                var ncStatus = navigator.SelectSingleNode("//ncresponse").GetAttribute("NCSTATUS", string.Empty);
                var ncErrorPlus = navigator.SelectSingleNode("//ncresponse").GetAttribute("NCERRORPLUS", string.Empty);
                var amount = navigator.SelectSingleNode("//ncresponse").GetAttribute("amount", string.Empty);
                var currency = navigator.SelectSingleNode("//ncresponse").GetAttribute("currency", string.Empty);

                if (status != "9")
                {
                    // The transaction was not successful.
                    //throw new PaymentException(
                    //    string.Format("Payment with PayId '{0}' and OrderId '{1}' could not be captured using OGone.", payId, orderId),
                    //    string.Format("Status: {0}. NCError: {1}. NcStatus: {2}. Acquirer Acceptance Code: '{3}'.", status, ncError, ncStatus, acceptance),
                    //    ncErrorPlus);
                    return new PaymentResponseDto { Error = string.Format("Status: {0}. NCError: {1}. NcStatus: {2}. Acquirer Acceptance Code: '{3}'.", status, ncError, ncStatus, acceptance) };
                    
                }

                return new PaymentResponseDto()
                {
                    PaymentId = payId,
                    OrderId = orderId,
                    Amount = decimal.Parse(amount),
                    Currency = currency
                };
            }
            catch (Exception ex)
            {
                //throw new InvalidOperationException("OGone Payment Gateway Error", ex);
                return new PaymentResponseDto { Error = ex.Message + Environment.NewLine + ex.StackTrace };

            }
        }
    }
}
