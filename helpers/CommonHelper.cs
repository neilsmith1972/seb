﻿using System;
using System.Configuration;
using System.Net.Mail;
using System.Linq;
using System.Reflection;
using SEB;

public static class CommonHelper {

    static readonly log4net.ILog logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

    public static object GetPropertyValue(object car, string propertyName)
    {
        return car.GetType().GetProperties()
           .Single(pi => pi.Name == propertyName)
           .GetValue(car, null);
    }

    public static string TranslateSpecialCharacters(string sourceString)
    {
        if (!string.IsNullOrEmpty(sourceString))
        {
            sourceString = sourceString.Replace("á", "a")
                .Replace("Á", "A")
                .Replace("à", "a")
                .Replace("À", "A")
                .Replace("â", "a")
                .Replace("Â", "A")
                .Replace("å", "a")
                .Replace("Å", "A")
                .Replace("ã", "a")
                .Replace("Ã", "A")
                .Replace("ä", "a")
                .Replace("Ä", "A")
                .Replace("æ", "ae")
                .Replace("Æ", "AE")
                .Replace("ç", "c")
                .Replace("Ç", "C")
                .Replace("é", "e")
                .Replace("É", "E")
                .Replace("è", "e")
                .Replace("È", "E")
                .Replace("ê", "e")
                .Replace("Ê", "E")
                .Replace("ë", "e")
                .Replace("Ë", "E")
                .Replace("í", "i")
                .Replace("Í", "I")
                .Replace("ì", "i")
                .Replace("Ì", "I")
                .Replace("î", "i")
                .Replace("Î", "I")
                .Replace("ï", "i")
                .Replace("Ï", "I")
                .Replace("ñ", "n")
                .Replace("Ñ", "N")
                .Replace("ó", "o")
                .Replace("Ó", "O")
                .Replace("ò", "o")
                .Replace("Ò", "O")
                .Replace("ô", "o")
                .Replace("Ô", "O")
                .Replace("ø", "o")
                .Replace("Ø", "O")
                .Replace("õ", "o")
                .Replace("Õ", "O")
                .Replace("ö", "o")
                .Replace("Ö", "O")
                .Replace("ß", "ss")
                .Replace("ú", "u")
                .Replace("Ú", "U")
                .Replace("ù", "u")
                .Replace("Ù", "U")
                .Replace("û", "u")
                .Replace("Û", "U")
                .Replace("ü", "u")
                .Replace("Ü", "U")
                .Replace("ÿ", "y");
        }
        return sourceString;
    }

    public static void setInstanceProperty(object instance, string propertyName, object value)
    {
        Type type = instance.GetType();
        PropertyInfo propertyInfo = type.GetProperty(propertyName);

        propertyInfo.SetValue(instance, value, null);

        return;
    }

    public static string GetGenericTranslation(string source)
    {
        var translation = source;
        SEBContext db = new SEBContext();
        var language = System.Web.HttpContext.Current.Session["chosenLanguage"].ToString();
        var t = db.GenericTranslations.FirstOrDefault(g => g.Source == source && g.CountryCode == language);
        if (t != null) translation = t.Translation;
        return translation;

    }

    public static string GetErrorTranslation(string source)
    {
        try
        {
            var errorCode = int.Parse(source.Substring(0,5));
            SEBContext db = new SEBContext();
            var language = System.Web.HttpContext.Current.Session["chosenLanguage"].ToString();
            var t = db.ErrorTranslations.FirstOrDefault(g => g.ErrorCode == errorCode);
            if (t != null)
            {
                switch (language)
                {
                    case "NO":
                        return string.IsNullOrEmpty(t.NO) ? t.GB : t.NO;
                        break;
                    case "SE":
                        return string.IsNullOrEmpty(t.SE) ? t.GB : t.SE;
                        break;
                    case "DK":
                        return string.IsNullOrEmpty(t.DK) ? t.GB : t.DK;
                        break;
                    case "FI":
                        return string.IsNullOrEmpty(t.FI) ? t.GB : t.FI;
                        break;
                    default:
                        return t.GB;
                        break;
                }
            }
            return string.Empty;
        }
        catch (Exception ex)
        {
            return string.Empty;
        }

    }

    public static SEB.Maestro.DCReservation MapReservation(SEB.Service.DCReservation DCreservation)
    {
        var reservation = new SEB.Maestro.DCReservation();
        reservation.ID = DCreservation.ID;
        reservation.DriverGivenName = DCreservation.DriverGivenName;
        reservation.DriverSurname = DCreservation.DriverSurname;
        reservation.DriverEmail = DCreservation.DriverEmail;
        reservation.DriverPhone = DCreservation.DriverPhone;
        reservation.DriverStreet = DCreservation.DriverStreet;
        reservation.DriverPostName = DCreservation.DriverPostName;
        reservation.DriverPostCode = DCreservation.DriverPostCode;
        reservation.DriverCountry = DCreservation.DriverCountry;
        reservation.DriverBirthDate = DCreservation.DriverBirthDate;
        reservation.DriversLicence = DCreservation.DriversLicence;
        reservation.ArrivalFlightCode = DCreservation.ArrivalFlightCode;
        reservation.ArrivalFlightNumber = DCreservation.ArrivalFlightNumber;
        reservation.ArrivalFlightAirline = DCreservation.ArrivalFlightAirline;
        reservation.DriversLicence = DCreservation.DriversLicence;
        reservation.DriverBirthDate = DCreservation.DriverBirthDate;
        reservation.DriverPhone = DCreservation.DriverPhone;
        reservation.PaymentCID = DCreservation.PaymentCID;
        reservation.PaymentCEX = DCreservation.PaymentCEX;
        reservation.PaymentMOP = DCreservation.PaymentMOP;
           

                //Voucher
                reservation.PaymentCID = DCreservation.PaymentCID;
                reservation.PaymentMOP = DCreservation.PaymentMOP;
                reservation.IATANumber = DCreservation.IATANumber;
                reservation.PaymentVoucherElectronic = DCreservation.PaymentVoucherElectronic;
                reservation.PaymentVoucherCode = DCreservation.PaymentVoucherCode;
        
                reservation.PickupLocationCountry = DCreservation.PickupLocationCountry ;
                reservation.PickupLocationCountryName = DCreservation.PickupLocationCountryName ;
                reservation.PickupDateTime = DCreservation.PickupDateTime ;
            
                reservation.ReturnLocationCountry = DCreservation.ReturnLocationCountry ;
                reservation.ReturnLocationCountryName = DCreservation.ReturnLocationCountryName ;
                reservation.ReturnDateTime = DCreservation.ReturnDateTime ;
            

        reservation.PickupLocation = DCreservation.PickupLocation ;
                    reservation.PickupLocationName = DCreservation.PickupLocationName;
        reservation.ReturnLocation = DCreservation.ReturnLocation;
                    reservation.ReturnLocationName = DCreservation.ReturnLocationName;
        reservation.PickupLocationCountry = DCreservation.PickupLocationCountry;
            reservation.PickupLocationCountryName = DCreservation.PickupLocationCountryName ;
                reservation.ReturnLocationCountry = DCreservation.ReturnLocationCountry;
                reservation.ReturnLocationCountryName = DCreservation.ReturnLocationCountryName;
                    reservation.PickupLocationName = DCreservation.PickupLocationName;
            reservation.ReturnLocationName = DCreservation.ReturnLocationName;

            reservation.CountryCode = DCreservation.CountryCode;
        reservation.DeliveryAddress1 = DCreservation.DeliveryAddress1;
                reservation.DeliveryAddress2 = DCreservation.DeliveryAddress2;
                reservation.DeliveryPostCode = DCreservation.DeliveryPostCode;
                reservation.DeliveryInstructions1 = DCreservation.DeliveryInstructions1;

                reservation.CollectionAddress1 = DCreservation.CollectionAddress1;
                reservation.CollectionAddress2 = DCreservation.CollectionAddress2;
                reservation.CollectionPostCode = DCreservation.CollectionPostCode;
                reservation.CollectionInstructions1 = DCreservation.CollectionInstructions1;

    reservation.PickupDateTime = DCreservation.PickupDateTime;
    reservation.CountryCode = DCreservation.CountryCode;
            reservation.DriverCountry = DCreservation.DriverCountry;
            reservation.AWDNumber = DCreservation.AWDNumber;

    reservation.VehicleMakeModelName = DCreservation.VehicleMakeModelName;
        reservation.VehicleGroupValue = DCreservation.VehicleGroupValue;



        reservation.VehicleSize = (SEB.Maestro.DCVehicleSize)(int) DCreservation.VehicleSize ;
        reservation.VehicleCategory = (SEB.Maestro.DCVehicleCategory)(int)DCreservation.VehicleCategory; // DCreservation.VehicleCategory;
            reservation.RateQualifier = DCreservation.RateQualifier;
            reservation.Amount = DCreservation.Amount;
            reservation.RateCategory = (SEB.Maestro.DCRateCategory)(int)DCreservation.RateCategory; // DCreservation.RateCategory;
            reservation.CurrencyCode = DCreservation.CurrencyCode;
            reservation.VehicleGroupType = DCreservation.VehicleGroupType;


        return reservation;
    }

    public static bool SendMail(string strSubject, string strBody,string mailto, string strCC, string strBcc)
    {
        try
        {
            string MailFrom = string.Empty;
            string MailTo = string.Empty;
            string MailServerName = ConfigurationManager.AppSettings.Get("MailServer");

            string MailPassword = ConfigurationManager.AppSettings.Get("Mailpassword");
            string MailUserName = ConfigurationManager.AppSettings.Get("MailuserName");
            string MailHost = ConfigurationManager.AppSettings.Get("Mailhost");
            int MailPort = Convert.ToInt32(ConfigurationManager.AppSettings.Get("MailPort"));

            MailFrom = ConfigurationManager.AppSettings.Get("MailUserName");
            MailTo = mailto;


            MailMessage email = new MailMessage();
            MailAddress mFromAddress = new MailAddress(MailFrom);
            MailAddress mToAddress = new MailAddress(MailTo);

            email.From = mFromAddress;
            email.To.Add(mToAddress);

            email.Subject = strSubject;
            email.IsBodyHtml = true;
            email.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure;
            email.Body = strBody;
            MailUserName = ConfigurationManager.AppSettings.Get("MailuserName");
            MailPassword = ConfigurationManager.AppSettings.Get("Mailpassword");
            SmtpClient ObjMail = new SmtpClient();

            if (System.Configuration.ConfigurationManager.AppSettings["BCCALL"].ToLower() == "yes")
            {
                email.Bcc.Add(new MailAddress(System.Configuration.ConfigurationManager.AppSettings["BCCALL_EmailAddress"]));
            }

            ObjMail.EnableSsl = false;
            //NetworkCredential credential = new NetworkCredential(MailUserName, MailPassword);

            //ObjMail.Credentials = credential;
            ObjMail.Host = MailHost;
            ObjMail.Port = MailPort;
            if (System.Configuration.ConfigurationManager.AppSettings["SendEmail"].ToLower() == "yes")
            {
                ObjMail.Send(email);
            }
            return true;

        }

        catch (Exception Ex)
        {
            logger.Error("Error sending email", Ex);
            return false;
        }
    }

        public static string GetHostUrl()
        {
            var u = System.Web.HttpContext.Current.Request.Url;
            var url = u.Scheme + "://" + u.Authority;

            if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["VirtualDirectory"])) url += ConfigurationManager.AppSettings["VirtualDirectory"];

            return url;
        }


        public static bool SendErrorMail(string strSubject, string strBody)
        {
            try
            {
                string MailFrom = string.Empty;
                string MailServerName = ConfigurationManager.AppSettings.Get("MailServer");
                string MailPassword = ConfigurationManager.AppSettings.Get("Mailpassword");
                string MailUserName = ConfigurationManager.AppSettings.Get("MailuserName");
                string MailHost = ConfigurationManager.AppSettings.Get("Mailhost");
                int MailPort = Convert.ToInt32(ConfigurationManager.AppSettings.Get("MailPort"));

                MailFrom = ConfigurationManager.AppSettings.Get("MailUserName");

                MailMessage email = new MailMessage();
                MailAddress mFromAddress = new MailAddress(MailFrom);

                email.From = mFromAddress;

                foreach (string to in ConfigurationManager.AppSettings["ErrorEmailRecipients"].Split(';'))
                {
                    email.To.Add(new MailAddress(to));
                }

                email.Subject = strSubject;
                email.IsBodyHtml = true;
                email.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure;
                email.Body = strBody;
                MailUserName = ConfigurationManager.AppSettings.Get("MailuserName");
                MailPassword = ConfigurationManager.AppSettings.Get("Mailpassword");
                SmtpClient ObjMail = new SmtpClient();

                ObjMail.EnableSsl = false;


                //ObjMail.Credentials = credential;
                ObjMail.Host = MailHost;
                ObjMail.Port = MailPort;
                if (System.Configuration.ConfigurationManager.AppSettings["SendEmail"].ToLower() == "yes")
                {
                    ObjMail.Send(email);
                }
                return true;

            }
            catch (Exception Ex)
            {
                return false;
            }
        }

    }

    