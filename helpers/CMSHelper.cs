﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;

namespace SEB.helpers
{
    public static class CMSHelper
    {
        private static SqlConnection connection = new SqlConnection();
        private static SqlCommand command = new SqlCommand();
        private static SqlDataAdapter adapter = new SqlDataAdapter();
        private static DataSet data = new DataSet();

        public static Dictionary<string,string> GetCMSItemParameters(int itemid)
        {
            connection.ConnectionString = System.Configuration.ConfigurationManager.AppSettings["CMSDatabase"];
            command.Connection = connection;
            command.CommandType = CommandType.Text;
            adapter.SelectCommand = command;
            var ret = new Dictionary<string, string>();

            data.Clear();
            command.CommandText = "select * from urlparameters where itemid = " + itemid;
            adapter.Fill(data, "params");
            foreach (DataRow paramRow in data.Tables["params"].Rows)
            {
                ret.Add(paramRow["ParameterName"].ToString(), paramRow["ParameterValue"].ToString());
            }

            return ret;
        }

        public static string GetCMSItem(int position)
        {
            connection.ConnectionString = System.Configuration.ConfigurationManager.AppSettings["CMSDatabase"];
            command.Connection = connection;
            command.CommandType = CommandType.Text;
            adapter.SelectCommand = command;

            var returnString = string.Empty;
            var CMSSiteId = System.Configuration.ConfigurationManager.AppSettings["CMSSiteId"].ToString();
            data.Clear();
            command.CommandText = "SELECT   Items.TopMessage, Items.BottomMessage, Items.id FROM Items where items.SiteId = " + CMSSiteId + " and items.Position = " + position;
            command.CommandText += " and (select COUNT(*) from SelectionCriteria where ItemId = items.id and CriteriaName = 'countrycode' and CriteriaValue = '" + HttpContext.Current.Session["chosenLanguage"] + "') > 0";
            command.CommandText += " and (select COUNT(*) from SelectionCriteria where ItemId = items.id and CriteriaName = 'startdate' and CONVERT(datetime, CriteriaValue, 103) <= GETDATE()) > 0";
            command.CommandText += " and (select COUNT(*) from SelectionCriteria where ItemId = items.id and CriteriaName = 'enddate' and CONVERT(datetime, CriteriaValue, 103) >= GETDATE()) > 0";
            connection.Open();
            adapter.Fill(data);
            connection.Close();

            if (data.Tables[0].Rows.Count > 0)
            {
                var row = data.Tables[0].Rows[0];

                //command.CommandText = "select * from urlparameters where itemid = " + row["id"];
                //adapter.Fill(data, "params");
                var pmts = "item=" + row["id"];
                //foreach (DataRow paramRow in data.Tables["params"].Rows)
                //{
                //    pmts += "&" + paramRow["ParameterName"].ToString() + "=" + paramRow["ParameterValue"].ToString();
                //}

                if (position == 1)
                {
                    returnString += "<a href='" + CommonHelper.GetHostUrl() + "/home/AvisPreferred?" + pmts + "'><div class='dummy' style='background: url(" + CommonHelper.GetHostUrl() + "/booking/getfile/" + row["id"] + ") no-repeat'>";
                    returnString += row["TopMessage"].ToString() + row["BottomMessage"].ToString() + "</div></a>";
                }
                else
                {
                    returnString += "<div class='dummy' style='background: url(" + CommonHelper.GetHostUrl() + "/booking/getfile/" + row["id"] + ") no-repeat'>";
                    returnString += row["TopMessage"].ToString() + row["BottomMessage"].ToString() + "</div>";
                }
            }

            return returnString;
        }
    }
}