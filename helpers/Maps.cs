﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AutoMapper;

namespace SEB.helpers
{
    public class Maps
    {
        public Maps()
        {
            Mapper.CreateMap<SEB.Maestro.ReservationCreateResponse, SEB.Service.ReservationCreateResponse>();
            Mapper.CreateMap<SEB.Maestro.ReservationCreateResponse, SEB.Service.ReservationCreateResponse>();
            Mapper.CreateMap<SEB.Maestro.DCReservation, SEB.Service.DCReservation>();
            Mapper.CreateMap<SEB.Maestro.DCCharge, SEB.Service.DCCharge>();
            Mapper.CreateMap<SEB.Maestro.Error, SEB.Service.Error>();
            Mapper.CreateMap<SEB.Maestro.DCReservation.ErrorItem, SEB.Service.DCReservation.ErrorItem>();

            Mapper.CreateMap<SEB.Maestro.ReservationCreateRequest, SEB.Service.ReservationCreateRequest>();

            Mapper.CreateMap<SEB.Maestro.DCChargeCalculation, SEB.Service.DCChargeCalculation>();

            Mapper.CreateMap<SEB.Maestro.DCChargeTax, SEB.Service.DCChargeTax>();
            Mapper.CreateMap<SEB.Maestro.DCChargePurpose, SEB.Service.DCChargePurpose>();
            Mapper.CreateMap<SEB.Maestro.DCCoverageType, SEB.Service.DCCoverageType>();

            Mapper.CreateMap<SEB.Maestro.ReservationModifyRequest, SEB.Service.ReservationModifyRequest>();
            Mapper.CreateMap<SEB.Maestro.ReservationModifyResponse, SEB.Service.ReservationModifyResponse>();

            Mapper.CreateMap<SEB.Maestro.DCExtra, SEB.Service.DCExtra>();
            Mapper.CreateMap<SEB.Service.DCExtra, SEB.Maestro.DCExtra>();
        }
    }
}