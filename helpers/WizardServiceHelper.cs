﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Globalization;
using System.ComponentModel.DataAnnotations;
using SEB.WizardWebService;
using SEB.Service;
using System.Xml.Serialization;
using System.IO;

namespace SEB.helpers.WizardHelpers
{
    public enum ReservationStatus
    {
        Available,

        OnRequest,

        StopSell
    }

    public class CreateReservationResponse
    {
        public ReservationStatus CarStatus { get; set; }

        public string AvailableCarGroups { get; set; }

        public string ReservationNumber { get; set; }
    }

    public class UpdateReservationResponse
    {
        public bool Success { get; set; }

        public string ErrorMessage { get; set; }
    }

    public class WizardReservation
    {
        [Required]
        [StringLength(6, MinimumLength = 6)]
        public string WizardNumber { get; set; }

        [Required]
        [StringLength(3, MinimumLength = 3)]
        public string MneumonicCode { get; set; }

        public DateTime CheckoutTime { get; set; }

        [Required]
        public char CarGroup { get; set; }

        public DateTime CheckinTime { get; set; }

        [Required]
        [StringLength(29)]
        public string CustomerName { get; set; }

        public DateTime CustomerDateOfBirth { get; set; }

        [Required]
        [StringLength(15)]
        public string DamageNumber { get; set; }

        [Required]
        [StringLength(7)]
        public string CarRegistrationNumber { get; set; }

        [Required]
        [StringLength(4)]
        public string DbsNumber { get; set; }

        [Required]
        [StringLength(29)]
        public string AddressOne { get; set; }

        [Required]
        [StringLength(29)]
        public string AddressTwo { get; set; }

        [Required]
        [StringLength(29)]
        public string AddressThree { get; set; }

        public long PhoneNumber { get; set; }

        [Required]
        [StringLength(14)]
        public string RentalLocationShortName { get; set; }

        // [Required]
        [StringLength(2, MinimumLength = 2)]
        public string CreditCardType { get; set; }

        // [Required]
        [StringLength(16, MinimumLength = 16)]
        public string CreditCardNumber { get; set; }

        // [Required]
        [StringLength(2, MinimumLength = 2)]
        public string CreditCardExipryMonth { get; set; }

        // [Required]
        [StringLength(2, MinimumLength = 2)]
        public string CreditCardExpiryYear { get; set; }

        public string WorkshopName { get; set; }

        public string Remarks { get; set; }

        public string GetWizardCreditCardInput()
        {
            return string.Format("{0}{1}", this.CreditCardType, this.CreditCardNumber);
        }

        public string GetWizardCreditCardExpiryDate()
        {
            return string.Format("{0}/{1}", this.CreditCardExipryMonth, this.CreditCardExpiryYear);
        }
    }

    public class OpenOrUpdateRentalRequest
    {
        [Required]
        public DateTime CheckInDate { get; set; }

        [Required]
        public DateTime DateOfBirth { get; set; }

        [Required]
        [StringLength(9, MinimumLength = 9)]
        public string RaNumber { get; set; }
    }


    public interface IWizardService
    {
        CreateReservationResponse CreateReservation(WizardReservation wizardReservation);
        CreateReservationResponse CreateReservationWithCard(WizardReservation wizardReservation);

        UpdateReservationResponse UpdateReservation(WizardReservation wizardReservation, string wizardReservationNumber);
        UpdateReservationResponse UpdateReservationWithCard(WizardReservation wizardReservation, string wizardReservationNumber);

        bool DeleteReservation(string wizardReservationNumber);

        bool OpenOrUpdateRental(OpenOrUpdateRentalRequest rentalRequest);

        bool DoCreditCardUpdate(string ra, string cardNumber, string expiry);
    }

    public static class WizardExtensions
    {
        public static string ToWizardDate(this DateTime dateTime)
        {
            return dateTime.ToString("ddMMMyy", CultureInfo.GetCultureInfo("en-US")).ToUpperInvariant();
        }

        public static string ToWizardDateTime(this DateTime dateTime)
        {
            return string.Format("{0}/{1}", dateTime.ToWizardDate(), dateTime.ToString("HHmm")).ToUpperInvariant();
        }

        public static string RemoveSpecialCharacters(this string str)
        {
            return RemoveSpecialCharacters(str, true);
        }

        public static string RemoveSpecialCharacters(this string str, bool removeSpaceChar)
        {
            str = str.Replace('å', 'a').Replace('ø', 'o').Replace("æ", "ae").Replace('Å', 'A').Replace('Ø', 'O').Replace("Æ", "AE");
            if (removeSpaceChar)
            {
                return str.Replace(" ", "").ToUpperInvariant();
            }

            return str.ToUpperInvariant();
        }

        public static string FormatResNumber(string resNo)
        {
            var pt1 = resNo.Substring(0, 4);
            var pt2 = resNo.Substring(4, 4);
            var pt3 = resNo.Substring(8, 2);
            var pt4 = resNo.Substring(10, 1);
            return pt1 + "-" + pt2 + "-" + pt3 + "-" + pt4;
        }
    }

    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    public class WizardService : IWizardService
    {

        public bool DoCreditCardUpdate(string ra, string cardNumber, string expiry)
        {
            var success = false;
            var db = new SEBContext();
            try
            {
                var sw_reservation = db.Reservations.FirstOrDefault(r => r.Reservation_DCID == ra);
                DCReservation reservation = new DCReservation();
                if (sw_reservation != null)
                {
                    var serializer = new XmlSerializer(reservation.GetType());
                    var deserialized = serializer.Deserialize(new StringReader(sw_reservation.Reservation_FormState));
                    reservation = (DCReservation)deserialized;
                }

                SEB.WizardWebService.WizardClient client = new WizardClient();

                var request = new X502Request()
                {
                    WizardNumber = reservation.WizardNumber,
                    CountryCode = "NO", //reservation.DriverCountry,
                    CheckoutStation = reservation.PickupLocation,
                    CheckOutTime = helpers.WizardHelpers.WizardExtensions.ToWizardDateTime(reservation.PickupDateTime),// "28oct13/0700",
                    CarGroup = reservation.VehicleGroupType,
                    CinLocation = reservation.ReturnLocation,
                    CheckinTime = helpers.WizardHelpers.WizardExtensions.ToWizardDateTime(reservation.ReturnDateTime), //"31OCT13/1500",
                    CustomerName = "r/" + helpers.WizardHelpers.WizardExtensions.FormatResNumber(ra), // "r/0475-5467-NO-3", // "r/4994-9588-NO-3", // reservation number
                    Pay = "NA", // reservation.PaymentMOP, // "CH",                
                    TransactionType = X502ProcessOption.UpdateWithCreditCard,
                    CreditCardNumber = cardNumber, //"CX4711100000000000",
                    CreditExplry = expiry // "12/14"
                };

                var result = new IntegroResult();

                try
                {
                    result = client.X502Process(request);
                    if (result.Message.Contains("RES OK")) success = true;
                }
                catch (Exception ex)
                {
                    var s = ex.Message;
                }
            }
            catch { }
            return success;
        }
        private static void ValidateWizardRequest(object wizardRequest)
        {
            Validator.ValidateObject(wizardRequest, new ValidationContext(wizardRequest, null, null), true);
        }

        public CreateReservationResponse CreateReservation(WizardReservation wizardReservation)
        {
            if (wizardReservation == null)
            {
                throw new ArgumentNullException("wizardReservation");
            }

            ValidateWizardRequest(wizardReservation);

            // Alaways remove special chracters å æ
            var client = new WizardClient();
            var request = new X502Request()
            {
                WizardNumber = wizardReservation.WizardNumber.RemoveSpecialCharacters(), // "TJ996Z",        // Forsikringsselskap drop down - Check if Wizard number is attached to Insurance company. Erro handling
                CountryCode = "NO",             // Always NO
                CheckoutStation = wizardReservation.MneumonicCode.RemoveSpecialCharacters(), // "K6R",        // MneminID - Avis utleiestasjon    
                CheckOutTime = wizardReservation.CheckoutTime.ToWizardDateTime(), // "25MAR13/0930",  // Fra dato - check if the date is passed or not
                CarGroup = wizardReservation.CarGroup.ToString(CultureInfo.InvariantCulture), // "K",                 // Bilgruppe ønsket av kunde
                CinLocation = wizardReservation.MneumonicCode.RemoveSpecialCharacters(), // "K6R",            // // MneminID - Avis utleiestasjon
                CheckinTime = wizardReservation.CheckinTime.ToWizardDateTime(), // "27MAR13/0930",   // Til dato
                CustomerName = wizardReservation.CustomerName.RemoveSpecialCharacters(false), // "ELLINGSEN,DAVID", // Etternavn, fornavn - with no spaces
                DateOfBirth = wizardReservation.CustomerDateOfBirth.ToWizardDate(), //"10MAR65",          // Fødselsdato
                DamageNumber = wizardReservation.DamageNumber.RemoveSpecialCharacters(), //"12345",          // Skadenummer 
                CarRegNumber = "T" + wizardReservation.CarRegistrationNumber.RemoveSpecialCharacters(), //"TDN95092",       //Registreringsnr./kunde
                DbsNumber = wizardReservation.DbsNumber.RemoveSpecialCharacters(), //"0032",             // DBS Number
                AddressOne = wizardReservation.AddressOne.RemoveSpecialCharacters(false), //"SNARUMSVEIN",     //Gateadresse
                AddressTwo = wizardReservation.AddressTwo.RemoveSpecialCharacters(false), //"41420073",        //Postnr./Poststed
                AddressThree = wizardReservation.AddressThree.RemoveSpecialCharacters(false) + ",NO", //"3370 VIKERSUND,NO", // always atached ,NO with adress 3
                PhoneNumber = wizardReservation.PhoneNumber.ToString(CultureInfo.InvariantCulture), //"41420073",           // Telefon numeric only
                RemarkComments = "DEL " + wizardReservation.Remarks.RemoveSpecialCharacters() + "/ROD",    // Short name from location table - always put DEL infront and /ROD at the end
                Via = "DL",                 // Always DL
                Pay = "CH",                 // Always CH
                TransactionType = X502ProcessOption.Create
            };

            IntegroResult result = null;
            try
            {
                result = client.X502Process(request);
            }
            catch (Exception ex)
            {
                throw new InvalidOperationException("Wizard error trying to create a new reservation, verify all input values are valid.", ex);
            }

            if (result.Message.Substring(116, 6) == "RES OK")
            {
                var reservationNumber = result.Message.Substring(204, 14);
                return new CreateReservationResponse { CarStatus = ReservationStatus.Available, ReservationNumber = reservationNumber };
            }

            //  CMD:                 502 RESERVATIONS/RATES  DOC          H/C               MSG cr  PAC        WIZ TK020A          ERROR - SEE HIGHLIGHTED FIELDS               STA OS2        CTR                                                             
            if (result.Message.Substring(116, 5) == "ERROR")
            {
                throw new InvalidOperationException("A wizard error occurred.");
            }

            //  CMD:                 502 RESERVATIONS/RATES  DOC          H/C           RES MSG cr  PAC        WIZ TJ996Z          REQUEST RES COMPLETED                        STA OS2        CTR                 WIZARD OF AVIS NUMBER TJ996Z        "
            if (result.Message.Substring(116, 21) == "REQUEST RES COMPLETED")
            {
                return new CreateReservationResponse { CarStatus = ReservationStatus.OnRequest };
            }

            if (result.Message.Substring(116, 2) == "A/")
            {
                var carGroupStatus = new Dictionary<string, string>();
                try
                {
                    var carGroupDefinitions = result.Message.Substring(115, 32) + " " + result.Message.Substring(195, 32);
                    var carGroups = carGroupDefinitions.Split(new[] { " " }, StringSplitOptions.RemoveEmptyEntries);
                    foreach (var carGroup in carGroups)
                    {
                        // Put all car groups in a dictionary
                        if (carGroupStatus.ContainsKey(carGroup.Substring(0, 1)))
                        {
                            carGroupStatus.Remove(carGroup.Substring(0, 1));
                        }

                        carGroupStatus.Add(carGroup.Substring(0, 1), carGroup.Substring(2, 1));
                    }
                }
                catch (Exception ex)
                {
                    throw new InvalidOperationException("Unexpected wizard response, could not extract valid car groups from response string.", ex);
                }


                if (carGroupStatus[wizardReservation.CarGroup.ToString(CultureInfo.InvariantCulture)] == "S")
                {
                    return new CreateReservationResponse { CarStatus = ReservationStatus.StopSell, AvailableCarGroups = GetAvailableCarGroups(carGroupStatus) };
                }

                throw new InvalidOperationException("Unexpected wizard response, could not determine car group status.");
            }
            // CMD:                 502 RESERVATIONS/RATES  DOC          H/C               MSG cr  PAC        WIZ TJ996Z          REQUEST RES COMPLETED                        STA OS2        CTR                 WIZARD OF AVIS NUMBER TJ996Z         
            // CMD:                 502 RESERVATIONS/RATES  DOC          H/C               MSG cr  PAC        WIZ TJ996Z          REQUEST RES COMPLETED                        STA OS2        CTR                 WIZARD OF AVIS NUMBER TJ996Z         
            /*
            if (carGroupStatus[wizardReservation.CarGroup.ToString(CultureInfo.InvariantCulture)] == "R")
            {
                return new CreateReservationResponse { CarStatus = ReservationStatus.OnRequest, AvailableCarGroups = GetAvailableCarGroups(carGroupStatus) };
            }
            */

            throw new InvalidOperationException("Invalid Wizard Response: " + result.Message);

            // iF CAR IS OK / AVAILABLE
            // CMD:                 502 RESERVATIONS/RATES  DOC          H/C               MSG cr  PAC        WIZ TJ996Z          RES OK                                       STA OS2        CTR                 RES NO. 2918-9928-NO-5     


            //iF CAR IS ON STOP SELL
            // CMD:                 502 RESERVATIONS/RATES  DOC          H/C               MSG cr  PAC        WIZ TJ996Z          A/S B/A C/A D/S E/S F/S G/A H/A              STA K6R        CTR                 I/S J/S K/R L/N M/A N/A O/R P/N             


            //IF CAR IS ON REQUEST
            //CMD:                 502 RESERVATIONS/RATES  DOC          H/C               MSG cr  PAC        WIZ TJ996Z          A/S B/A C/A D/S E/S F/S G/A H/A              STA K6R        CTR                 I/S J/S K/R L/N M/A N/A O/R P/N              
        }

        public CreateReservationResponse CreateReservationWithCard(WizardReservation wizardReservation)
        {
            if (wizardReservation == null)
            {
                throw new ArgumentNullException("wizardReservation");
            }

            ValidateWizardRequest(wizardReservation);

            // Alaways remove special chracters å æ
            var client = new WizardClient();
            var request = new X502Request()
            {
                WizardNumber = wizardReservation.WizardNumber.RemoveSpecialCharacters(), // "TJ996Z",        // Forsikringsselskap drop down - Check if Wizard number is attached to Insurance company. Erro handling
                CountryCode = "NO",             // Always NO
                CheckoutStation = wizardReservation.MneumonicCode.RemoveSpecialCharacters(), // "K6R",        // MneminID - Avis utleiestasjon    
                CheckOutTime = wizardReservation.CheckoutTime.ToWizardDateTime(), // "25MAR13/0930",  // Fra dato - check if the date is passed or not
                CarGroup = wizardReservation.CarGroup.ToString(CultureInfo.InvariantCulture), // "K",                 // Bilgruppe ønsket av kunde
                CinLocation = wizardReservation.MneumonicCode.RemoveSpecialCharacters(), // "K6R",            // // MneminID - Avis utleiestasjon
                CheckinTime = wizardReservation.CheckinTime.ToWizardDateTime(), // "27MAR13/0930",   // Til dato
                CustomerName = wizardReservation.CustomerName.RemoveSpecialCharacters(), // "ELLINGSEN,DAVID", // Etternavn, fornavn - with no spaces
                DateOfBirth = wizardReservation.CustomerDateOfBirth.ToWizardDate(), //"10MAR65",          // Fødselsdato
                DamageNumber = wizardReservation.DamageNumber.RemoveSpecialCharacters(), //"12345",          // Skadenummer 
                CarRegNumber = "T" + wizardReservation.CarRegistrationNumber.RemoveSpecialCharacters(), //"TDN95092",       //Registreringsnr./kunde
                DbsNumber = wizardReservation.DbsNumber.RemoveSpecialCharacters(), //"0032",             // DBS Number
                AddressOne = wizardReservation.AddressOne.RemoveSpecialCharacters(false), //"SNARUMSVEIN",     //Gateadresse
                AddressTwo = wizardReservation.AddressTwo.RemoveSpecialCharacters(false), //"41420073",        //Postnr./Poststed
                AddressThree = wizardReservation.AddressThree.RemoveSpecialCharacters(false) + ",NO", //"3370 VIKERSUND,NO", // always atached ,NO with adress 3
                PhoneNumber = wizardReservation.PhoneNumber.ToString(CultureInfo.InvariantCulture), //"41420073",           // Telefon numeric only
                RemarkComments = "DEL " + wizardReservation.Remarks.RemoveSpecialCharacters() + "/ROD",    // Short name from location table - always put DEL infront and /ROD at the end
                Via = "DL",                 // Always DL
                Pay = "CH",                 // Always CH
                CreditCardNumber = wizardReservation.GetWizardCreditCardInput(),
                CreditExplry = wizardReservation.GetWizardCreditCardExpiryDate(),
                TransactionType = X502ProcessOption.Create
            };

            IntegroResult result = null;
            try
            {
                result = client.X502ProcessWitCard(request);
            }
            catch (Exception ex)
            {
                throw new InvalidOperationException("Wizard error trying to create a new reservation, verify all input values are valid.", ex);
            }

            if (result.Message.Substring(116, 6) == "RES OK")
            {
                var reservationNumber = result.Message.Substring(204, 14);
                return new CreateReservationResponse { CarStatus = ReservationStatus.Available, ReservationNumber = reservationNumber };
            }

            //  CMD:                 502 RESERVATIONS/RATES  DOC          H/C               MSG cr  PAC        WIZ TK020A          ERROR - SEE HIGHLIGHTED FIELDS               STA OS2        CTR                                                             
            if (result.Message.Substring(116, 5) == "ERROR")
            {
                throw new InvalidOperationException("A wizard error occurred.");
            }

            //  CMD:                 502 RESERVATIONS/RATES  DOC          H/C           RES MSG cr  PAC        WIZ TJ996Z          REQUEST RES COMPLETED                        STA OS2        CTR                 WIZARD OF AVIS NUMBER TJ996Z        "
            if (result.Message.Substring(116, 21) == "REQUEST RES COMPLETED")
            {
                return new CreateReservationResponse { CarStatus = ReservationStatus.OnRequest };
            }

            if (result.Message.Substring(116, 2) == "A/")
            {
                var carGroupStatus = new Dictionary<string, string>();
                try
                {
                    var carGroupDefinitions = result.Message.Substring(115, 32) + " " + result.Message.Substring(195, 32);
                    var carGroups = carGroupDefinitions.Split(new[] { " " }, StringSplitOptions.RemoveEmptyEntries);
                    foreach (var carGroup in carGroups)
                    {
                        // Put all car groups in a dictionary
                        if (carGroupStatus.ContainsKey(carGroup.Substring(0, 1)))
                        {
                            carGroupStatus.Remove(carGroup.Substring(0, 1));
                        }

                        carGroupStatus.Add(carGroup.Substring(0, 1), carGroup.Substring(2, 1));
                    }
                }
                catch (Exception ex)
                {
                    throw new InvalidOperationException("Unexpected wizard response, could not extract valid car groups from response string.", ex);
                }


                if (carGroupStatus[wizardReservation.CarGroup.ToString(CultureInfo.InvariantCulture)] == "S")
                {
                    return new CreateReservationResponse { CarStatus = ReservationStatus.StopSell, AvailableCarGroups = GetAvailableCarGroups(carGroupStatus) };
                }

                throw new InvalidOperationException("Unexpected wizard response, could not determine car group status.");
            }
            // CMD:                 502 RESERVATIONS/RATES  DOC          H/C               MSG cr  PAC        WIZ TJ996Z          REQUEST RES COMPLETED                        STA OS2        CTR                 WIZARD OF AVIS NUMBER TJ996Z         
            // CMD:                 502 RESERVATIONS/RATES  DOC          H/C               MSG cr  PAC        WIZ TJ996Z          REQUEST RES COMPLETED                        STA OS2        CTR                 WIZARD OF AVIS NUMBER TJ996Z         
            /*
            if (carGroupStatus[wizardReservation.CarGroup.ToString(CultureInfo.InvariantCulture)] == "R")
            {
                return new CreateReservationResponse { CarStatus = ReservationStatus.OnRequest, AvailableCarGroups = GetAvailableCarGroups(carGroupStatus) };
            }
            */

            throw new InvalidOperationException("Invalid Wizard Response: " + result.Message);

            // iF CAR IS OK / AVAILABLE
            // CMD:                 502 RESERVATIONS/RATES  DOC          H/C               MSG cr  PAC        WIZ TJ996Z          RES OK                                       STA OS2        CTR                 RES NO. 2918-9928-NO-5     


            //iF CAR IS ON STOP SELL
            // CMD:                 502 RESERVATIONS/RATES  DOC          H/C               MSG cr  PAC        WIZ TJ996Z          A/S B/A C/A D/S E/S F/S G/A H/A              STA K6R        CTR                 I/S J/S K/R L/N M/A N/A O/R P/N             


            //IF CAR IS ON REQUEST
            //CMD:                 502 RESERVATIONS/RATES  DOC          H/C               MSG cr  PAC        WIZ TJ996Z          A/S B/A C/A D/S E/S F/S G/A H/A              STA K6R        CTR                 I/S J/S K/R L/N M/A N/A O/R P/N              
        }

        private static string GetAvailableCarGroups(IDictionary<string, string> carGroupStatuses)
        {
            var validCarGroups = new List<string>();
            foreach (var carGroup in carGroupStatuses)
            {
                if (carGroup.Value == "A")
                {
                    validCarGroups.Add(carGroup.Key);
                }
            }

            return string.Join(",", validCarGroups.ToArray());
        }

        public UpdateReservationResponse UpdateReservation(WizardReservation wizardReservation, string wizardReservationNumber)
        {
            if (wizardReservation == null)
            {
                throw new ArgumentNullException("wizardReservation");
            }

            if (string.IsNullOrWhiteSpace(wizardReservationNumber))
            {
                throw new ArgumentNullException("wizardReservationNumber");
            }

            ValidateWizardRequest(wizardReservation);

            var client = new WizardClient();
            var request = new X502Request()
            {
                WizardNumber = wizardReservation.WizardNumber.RemoveSpecialCharacters(), // "TJ996Z",        // Forsikringsselskap drop down - Check if Wizard number is attached to Insurance company. Erro handling
                CountryCode = "NO",             // Always NO
                CheckoutStation = wizardReservation.MneumonicCode.RemoveSpecialCharacters(), // "K6R",        // MneminID - Avis utleiestasjon    
                CheckOutTime = wizardReservation.CheckoutTime.ToWizardDateTime(), // "25MAR13/0930",  // Fra dato - check if the date is passed or not
                CarGroup = wizardReservation.CarGroup.ToString(CultureInfo.InvariantCulture), // "K",                 // Bilgruppe ønsket av kunde
                CinLocation = wizardReservation.MneumonicCode.RemoveSpecialCharacters(), // "K6R",            // // MneminID - Avis utleiestasjon
                CheckinTime = wizardReservation.CheckinTime.ToWizardDateTime(), // "27MAR13/0930",   // Til dato
                CustomerName = "r/" + wizardReservationNumber.RemoveSpecialCharacters(), // RESERVATION NUMBER
                DateOfBirth = wizardReservation.CustomerDateOfBirth.ToWizardDate(), //"10MAR65",          // Fødselsdato
                DamageNumber = wizardReservation.DamageNumber.RemoveSpecialCharacters(), //"12345",          // Skadenummer 
                CarRegNumber = "T" + wizardReservation.CarRegistrationNumber.RemoveSpecialCharacters(), //"TDN95092",       //Registreringsnr./kunde
                DbsNumber = wizardReservation.DbsNumber.RemoveSpecialCharacters(), //"0032",             // DBS Number
                AddressOne = wizardReservation.AddressOne.RemoveSpecialCharacters(false), //"SNARUMSVEIN",     //Gateadresse
                AddressTwo = wizardReservation.AddressTwo.RemoveSpecialCharacters(false), //"41420073",        //Postnr./Poststed
                AddressThree = wizardReservation.AddressThree.RemoveSpecialCharacters(false) + ",NO", //"3370 VIKERSUND,NO", // always atached ,NO with adress 3
                PhoneNumber = wizardReservation.PhoneNumber.ToString(CultureInfo.InvariantCulture), //"41420073",           // Telefon numeric only
                RemarkComments = "DEL " + wizardReservation.Remarks.RemoveSpecialCharacters() + "/ROD",    // Short name from location table - always put DEL infront and /ROD at the end
                Via = "DL",                 // Always DL
                Pay = "CH",                 // Always CH
                TransactionType = X502ProcessOption.Update
            };

            try
            {
                var result = client.X502Process(request);
                if (result.Message.Substring(116, 6) == "RES OK")
                {
                    return new UpdateReservationResponse { Success = true };
                }

                var errorMessage = result.Message.Substring(115, 45);
                return new UpdateReservationResponse { Success = false, ErrorMessage = errorMessage };

                // SUCCESS
                // CMD:                 502 RESERVATIONS/RATES  DOC          H/C               MSG mr  PAC        WIZ TJ996Z          RES OK                                       STA OS2        CTR                 RES NO. 2919-0166-NO-5  

                // ERROR
                // CMD:                 502 RESERVATIONS/RATES  DOC          H/C               MSG mr  PAC        WIZ TJ996Z          RATE NOT VALID FOR STN,DATE,CAR GRP          STA OS2        CTR    
            }
            catch (Exception ex)
            {
                throw new InvalidOperationException("Wizard error trying to update reservation '" + wizardReservationNumber + "', verify all input values are valid.", ex);
            }

        }

        public UpdateReservationResponse UpdateReservationWithCard(WizardReservation wizardReservation, string wizardReservationNumber)
        {
            if (wizardReservation == null)
            {
                throw new ArgumentNullException("wizardReservation");
            }

            if (string.IsNullOrWhiteSpace(wizardReservationNumber))
            {
                throw new ArgumentNullException("wizardReservationNumber");
            }

            ValidateWizardRequest(wizardReservation);

            var client = new WizardClient();
            var request = new X502Request()
            {
                WizardNumber = wizardReservation.WizardNumber.RemoveSpecialCharacters(), // "TJ996Z",        // Forsikringsselskap drop down - Check if Wizard number is attached to Insurance company. Erro handling
                CountryCode = "NO",             // Always NO
                CheckoutStation = wizardReservation.MneumonicCode.RemoveSpecialCharacters(), // "K6R",        // MneminID - Avis utleiestasjon    
                CheckOutTime = wizardReservation.CheckoutTime.ToWizardDate(), // "25MAR13/0930",  // Fra dato - check if the date is passed or not
                CarGroup = wizardReservation.CarGroup.ToString(CultureInfo.InvariantCulture), // "K",                 // Bilgruppe ønsket av kunde
                CinLocation = wizardReservation.MneumonicCode.RemoveSpecialCharacters(), // "K6R",            // // MneminID - Avis utleiestasjon
                CheckinTime = wizardReservation.CheckinTime.ToWizardDate(), // "27MAR13/0930",   // Til dato
                CustomerName = "r/" + wizardReservationNumber.RemoveSpecialCharacters(), // RESERVATION NUMBER
                DateOfBirth = wizardReservation.CustomerDateOfBirth.ToWizardDate(), //"10MAR65",          // Fødselsdato
                DamageNumber = wizardReservation.DamageNumber.RemoveSpecialCharacters(), //"12345",          // Skadenummer 
                CarRegNumber = "T" + wizardReservation.CarRegistrationNumber.RemoveSpecialCharacters(), //"TDN95092",       //Registreringsnr./kunde
                DbsNumber = wizardReservation.DbsNumber.RemoveSpecialCharacters(), //"0032",             // DBS Number
                AddressOne = wizardReservation.AddressOne.RemoveSpecialCharacters(false), //"SNARUMSVEIN",     //Gateadresse
                AddressTwo = wizardReservation.AddressTwo.RemoveSpecialCharacters(false), //"41420073",        //Postnr./Poststed
                AddressThree = wizardReservation.AddressThree.RemoveSpecialCharacters(false) + ",NO", //"3370 VIKERSUND,NO", // always atached ,NO with adress 3
                PhoneNumber = wizardReservation.PhoneNumber.ToString(CultureInfo.InvariantCulture), //"41420073",           // Telefon numeric only
                RemarkComments = "DEL " + wizardReservation.Remarks.RemoveSpecialCharacters() + "/ROD",    // Short name from location table - always put DEL infront and /ROD at the end
                Via = "DL",                 // Always DL
                Pay = "CH",                 // Always CH
                CreditCardNumber = wizardReservation.GetWizardCreditCardInput(),
                CreditExplry = wizardReservation.GetWizardCreditCardExpiryDate(),
                TransactionType = X502ProcessOption.UpdateWithCreditCard
            };

            try
            {
                var result = client.X502ProcessWitCard(request);
                if (result.Message.Substring(35, 6) == "RES OK")
                {
                    return new UpdateReservationResponse { Success = true };
                }

                var errorMessage = result.Message;
                return new UpdateReservationResponse { Success = false, ErrorMessage = errorMessage };

                // SUCCESS
                // CMD:                 502 RESERVATIONS/RATES  DOC          H/C               MSG mr  PAC        WIZ TJ996Z          RES OK                                       STA OS2        CTR                 RES NO. 2919-0166-NO-5  

                // ERROR
                // CMD:                 502 RESERVATIONS/RATES  DOC          H/C               MSG mr  PAC        WIZ TJ996Z          RATE NOT VALID FOR STN,DATE,CAR GRP          STA OS2        CTR    
            }
            catch (Exception ex)
            {
                throw new InvalidOperationException("Wizard error trying to update reservation '" + wizardReservationNumber + "', verify all input values are valid.", ex);
            }

        }

        public bool DeleteReservation(string wizardReservationNumber)
        {
            if (string.IsNullOrWhiteSpace(wizardReservationNumber))
            {
                throw new ArgumentNullException("wizardReservationNumber");
            }

            var client = new WizardClient();
            var request = new X502Request()
            {
                CountryCode = "NO",
                CustomerName = "r/" + wizardReservationNumber, // always prefix with r/res#
                TransactionType = X502ProcessOption.Delete
            };

            try
            {

                var result = client.X502Process(request);
                if (result.Message.Substring(196, 9) == "CANCEL OK")
                {
                    return true;
                }

                return false;
                // CMD:                 502 RESERVATIONS/RATES  DOC          H/C               MSG xl  PAC        WIZ TJ996Z          AWD1 N005501  AWD2  000000                   STA OS2        CTR                 CANCEL OK  
            }
            catch (Exception ex)
            {
                throw new InvalidOperationException("Wizard error trying to cancel reservation '" + wizardReservationNumber + "'.", ex);
            }
        }

        public bool OpenOrUpdateRental(OpenOrUpdateRentalRequest rentalRequest)
        {
            if (rentalRequest == null)
            {
                throw new ArgumentNullException("rentalRequest");
            }

            ValidateWizardRequest(rentalRequest);
            var raNumber = int.Parse(rentalRequest.RaNumber);

            var client = new WizardClient();
            var request = new X203Request()
            {
                AgentNumber = "66666",
                CheckOutDateTime = "",
                CheckInDateTime = rentalRequest.CheckInDate.ToWizardDateTime(), // "27MAR13/1200",
                DateOfBirth = rentalRequest.DateOfBirth.ToWizardDate(), // "25FEB69",
                CountryCode = "NO",
                RaNumber = raNumber.ToString(CultureInfo.InvariantCulture) // "681638510"
            };

            try
            {
                // MODIFY COMPLETED - ENTER MXC IN ACT
                var result = client.X203Process(request);
                if (result.Message == "MODIFY COMPLETED - ENTER MXC IN ACT")
                {
                    return true;
                }

                return false;
            }
            catch (Exception ex)
            {
                throw new InvalidOperationException("Wizard error trying to open or update a rental'" + rentalRequest.RaNumber + "'.", ex);
            }
        }
    }
}