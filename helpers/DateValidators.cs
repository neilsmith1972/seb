﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SEB.helpers
{
    public sealed class Over18Attribute : ValidationAttribute
    {
        public override bool IsValid(object value)
        {
            if (isDate(value.ToString()))
            {
                var dob = (DateTime) value;
                return (dob <= DateTime.Now.AddYears(-18));
            }
            return false;
        }

        public bool isDate(string value)
        {
            try
            {
                var dob = DateTime.Parse(value);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }

    
}