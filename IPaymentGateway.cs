﻿//---------------------------------------------------------------------
// <copyright file="IPaymentGateway.cs" company="Adam Elsworth">
//      Copyright (c) Adam Elsworth. All Rights Reserved.
//      Information Contained Herein is Proprietary and Confidential.
// </copyright>
//---------------------------------------------------------------------
namespace SolutionTemplate.ExternalServices
{
    #region "Using Directive(s)"

    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    using SolutionTemplate.ExternalServices.Dto;

    #endregion

    /// <summary>
    /// 
    /// </summary>
    public interface IPaymentGateway
    {            
        #region "Constant Fields"

        #endregion

        #region "Fields"

        #endregion

        #region "Delegates"

        #endregion

        #region "Events"

        #endregion

        #region "Enums"

        #endregion

        #region "Interfaces"

        #endregion

        #region "Properties"

        #endregion

        #region "Indexers"

        #endregion

        #region "Methods"

        PaymentResponseDto CapturePayment(CapturePaymentDto paymentToCapture);

        PaymentResponseDto RefundPayment(RefundPaymentDto paymentToCapture);

        PaymentResponseDto CancelPayment(CancelPaymentDto paymentToCapture);

        /// <summary>
        /// Release funds reserved by an authorization, but do not cancel the authorization itself.
        /// </summary>
        /// <param name="paymentToCapture"></param>
        /// <returns></returns>
        PaymentResponseDto ReleaseHeldFunds(CancelPaymentDto paymentToCapture);

        Dictionary<string, object> GetFormParametersForPostingPaymentAuthorization(AuthorizationDto paymentToAuthorize, string carRequestId);


        #endregion

        #region "Structs"

        #endregion

        #region "Classes"

        #endregion
    }
}
