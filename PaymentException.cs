﻿namespace MvcPayment
{
    using System;

    public class PaymentException : Exception
    {
        public string PaymentProviderReasonCode { get; set; }

        public string PaymentProviderMessage { get; set; }

        public PaymentException(string message, string paymentProviderReasonCode, string paymentProviderMessage)
            : base(string.Format("{0}. Payment Provider Reason Code: {1}. Payment Provider Reason Message: {2}.", message, paymentProviderReasonCode, paymentProviderMessage))
        {
            this.PaymentProviderReasonCode = paymentProviderReasonCode;
            this.PaymentProviderMessage = paymentProviderMessage;
        }
    }
}