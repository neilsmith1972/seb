using System;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using SEB.Entities;
using SEB.Mapping;

namespace SEB
{
	public class SEBContext : DbContext
	{
		static SEBContext()
		{ 
			Database.SetInitializer<SEBContext>(null);
		}

		public DbSet<Reservation> Reservations { get; set; }
		public DbSet<ReservationExtra> ReservationExtras { get; set; }
		public DbSet<User> Users { get; set; }
		public DbSet<UserStatus> UserStatus { get; set; }
        public DbSet<AwdNumber> AwdNumbers { get; set; }
        public DbSet<Payment> Payments { get; set; }
        public DbSet<Country> Countries { get; set; }
        public DbSet<BudgetLocationMapping> BudgetLocationMapping { get; set; }
        public DbSet<Airline> Airlines {get; set;}
        public DbSet<Site> Sites { get; set; }
        public DbSet<VoucherCode> VoucherCodes { get; set; }
        public DbSet<GenericTranslation> GenericTranslations { get; set; }
        public DbSet<ErrorTranslation> ErrorTranslations { get; set; }
        public DbSet<EnquiryRecipient> EnquiryRecipient { get; set; }


        protected override void OnModelCreating(DbModelBuilder modelBuilder)
		{
            modelBuilder.Conventions.Remove<IncludeMetadataConvention>();
			modelBuilder.Configurations.Add(new ReservationMap());
			modelBuilder.Configurations.Add(new ReservationExtraMap());
			modelBuilder.Configurations.Add(new UserMap());
			modelBuilder.Configurations.Add(new UserStatusMap());
            modelBuilder.Configurations.Add(new AwdNumberMap());
            modelBuilder.Configurations.Add(new PaymentMap());
            modelBuilder.Configurations.Add(new CountryMap());
            modelBuilder.Configurations.Add(new BudgetLocationMappingMap());
            modelBuilder.Configurations.Add(new AirlineMap());
            modelBuilder.Configurations.Add(new SiteMap());
            modelBuilder.Configurations.Add(new VoucherCodeMap());
            modelBuilder.Configurations.Add(new GenericTranslationMap());
            modelBuilder.Configurations.Add(new EnquiryRecipientMap());
        }
	}
}

