﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Configuration;
using Microsoft.Practices.Unity;

[assembly: log4net.Config.XmlConfigurator(ConfigFile="log4net.config", Watch = true)]

namespace SEB
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801

    public class MvcApplication : System.Web.HttpApplication
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }

        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            //routes.MapRoute("EuroCard", // Route name
            //    "{Booking}/{BookingDetails}/{EuroCard}", // URL with parameters
            //    new { controller = "Booking", action = "BookingDetails", id = "EuroCard" } // Parameter defaults
            //);

            routes.MapRoute(
                "Default", // Route name
                "{controller}/{action}/{id}", // URL with parameters
                new { controller = "Booking", action = "BookingDetails", id = UrlParameter.Optional } // Parameter defaults
            );

        }

        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();

            RegisterGlobalFilters(GlobalFilters.Filters);
            RegisterRoutes(RouteTable.Routes);


        }

        //////public static bool OnTrustedNetwork(out string ip)
        //////{
        //////    string[] ips = System.Configuration.ConfigurationManager.AppSettings["SWECOIPAddresses"].Split(';');
        //////    //string ip_1 = Request.UserHostAddress;
        //////    //string eu_ip = Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
        //////    ip = HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];
        //////    if (ips.Contains(ip))
        //////    {
        //////        HttpContext.Current.Session["SwecoNetwork"] = "yes";
        //////        return true;
        //////    }
        //////    else
        //////    {
        //////        HttpContext.Current.Session["SwecoNetwork"] = null;
        //////        return false;
        //////    }

        //////}

        //public MvcApplication()
        //{
        //    BeginRequest += new EventHandler(MvcApplication_BeginRequest);
        //}

        //void MvcApplication_BeginRequest(object sender, EventArgs e)
        //{
        //    if (HttpContext.Current.Session["culture"] == null) HttpContext.Current.Session["culture"] = "sv-SE";
        //    System.Threading.Thread.CurrentThread.CurrentCulture= new CultureInfo(HttpContext.Current.Session["culture"].ToString());
        //    System.Threading.Thread.CurrentThread.CurrentUICulture =
        //        System.Threading.Thread.CurrentThread.CurrentCulture;
        //} 

    }
}