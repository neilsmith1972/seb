﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SEB.InvoiceService;

namespace SEB.Models
{
    public class HomeIndexViewModel
    {
        public string RaNumber { get; set; }
        public DtoDocumentSearchResult SearchResult { get; set; }
    }
}